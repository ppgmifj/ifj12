/**
 * @file template.c
 * @brief Tento subor sluzi ako sablona.

 * Obsahlejsi popis ako brief,
 * tento subor sluzi len ako sablona pre rychle copy and paste.
 */
/*
 ********************************************************************
 * File:         template.c
 * Date:         13.10. 2012
 * Author(s):    Gabriel Brandersky
 * Project:      Interpret IFJ12
 * Team:         Petr Jirout,         xjirou07
 *               Petr Huf,            xhufpe00
 *               Matej Vanatko,       xvanat01
 *               Gabriel Brandersky,  xbrand04
 ********************************************************************
 */

#include <stdio.h>
#include <stdlib.h>

#define BULGAR_CONSTANT 21

/**
 * Vyctovy typ pre chybove kody programu
 */
enum ecodes
{
    E_OK = 0,             ///< Bez chyby
    E_LEXEM,              ///< lexikalna chyba - chybna struktura lexemu
    E_SYNTAX,             ///< syntakticka chyba - chybna struktura programu

    /* semanticke chyby */
    E_VAR_SEM,            ///< Nedefinovana premena
    E_FUNC_SEM,           ///< Nedefinovana funkcia
    E_OTHER_SEM,          ///< Ostatne semanticke chyby

    /* Chyby za behu */
    E_DIV_ZERO = 10,      ///< Delenie nulov
    E_UNCOMPATIBLE_TYPE,  ///< Nekompatiblne typy
    E_CASTING,            ///< Chyba pretypovani - funkce numeric
    E_OTHER_RUN,          ///< Ostatne behove chyby

    /* Interna chyba - neovplyvnena vstupnym suborom */
    E_INTER = 99,         ///< malloc, fopen, atd.
};


/**
 * Vyctovy typ pre terminaly
 */
typedef enum terminals
{
    T_ID,             ///< identikator

    /* klucove slova */
    T_EOL,            ///< '\n'
    T_IF,             ///< if
    T_ELSE,           ///< else
    T_END,            ///< end
    T_FUNC,           ///< function
    T_WHILE,          ///< while

    /* zatvorky */
    T_L_BRACKET,      ///< (
    T_R_BRACKET,      ///< )
    T_L_SQ_BRACKET,   ///< [
    T_R_SQ_BRACKET,   ///< ]

    /* ine */
    T_COMMA,          ///< ,
    T_ASSIGN,         ///< =
    T_END_OF_INPUT,   ///< $

    T_COUNT,          ///< spocitame si pocet terminalov
    T_UNKNOWN         ///< neznamy token
} T_TERM;

char *TOKEN_NAMES[] = {
    /* matika */
    [T_PLUS] = "+",
    [T_MINUS] = "-",
    [T_MUL] = "*",
    [T_DIV] = "/",
    [T_EXP] = "**",
    [T_EQ] = "==",
    [T_NOT_EQ] = "!=",
    [T_LOWER] = "<",
    [T_GREATER] = ">",
    [T_L_EQ] = "<=",
    [T_G_EQ] = ">=",
    [T_L_BRACKET] = "(",
    [T_R_BRACKET] = ")",
    
    /* klicova slova */
    [T_EOL] = "EOL",
    [T_FUNC] = "function",
    [T_ELSE] = "else",
    [T_IF] = "if",
    [T_NIL] = "nil",
    [T_RETURN] = "return",
    [T_TRUE] = "true",
    [T_FALSE] = "false",
    [T_WHILE] = "while",
    [T_LOOP] = "loop",
    [T_FOR] = "for",
    [T_IN] = "in",
    [T_END] = "end",
    
    [T_L_SQ_BRACKET] = "[",
    [T_R_SQ_BRACKET] = "]",
    
    /* ine */
    [T_COMMA] = ",",
    [T_ASSIGN] = "=",
    [T_COLON] = ":",
    
    /* typy */
    [T_NUM] = "numeric",
    [T_STR] = "retezec",
    [T_ID] = "id",
    [T_EOF] = "EOF",
    [40] = "lexikalna chyba",
    [41] = "interna chyba"
};    


/**
 * Debugovacia funkcia na vypisanie tokenu
 */
void printToken(int token)
{
    if (token == -3)
        token = 40;
    else if (token == -2)
        token = 41;
    else if (token < 0 || token > T_EOF)
        token = T_EOF; 
    
    printf("%s", TOKEN_NAMES[token]);
}


/**
* Funkce vracejici token - pouzivat vycet vsude tam, kde to dava smysl
* @param myToken ukazatel na strukturu tokenu
* @return typ tokenu (T_TERM)
*/
T_TERM GetNextToken(TToken *myToken)
{
    ...
}

/**
 * Vyctovy typ pro rozmerove dimenze
 */
typedef enum {
    dimOne,   ///< 1D, jedna dimenze
    dimTwo,   ///< 2D, dve dimenze
    dimThree, ///< 3D, tri dimenze
    docKovar  ///< Meda
} dimension_t;

typedef short int newtype_t //(suffix "_t" pri tvorbe typu")

/**
 * Struktura pro uchovani obsahu PPM souboru.
 */
typedef struct ppm {
    unsigned long xsize; ///< velikost x rozmeru
    unsigned long ysize; ///< velikost y rozmeru
    char data[]; ///< vektor pro uchovani samotnych dat
} ppm_t;


/**
 * Kratky, jednoradkovy popis funkce.
 *
 * Obsahlejsi, podrobnejsi popis cinnosti funkce.
 * Klidne na vice radku.
 *
 * @bug Obcas vraci spatne vysledky.
 * @warning Pri zadani cisla 0 se zachveje lampicka.
 * @param firstNumber prvni operand (int)
 * @param secondNumber druhy operand (int)
 * @return prumer zadanych cisel (double)
 */
double Average(int firstNumber, int secondNumber)
{

}

/**
 * Kratky, jednoradkovy popis dalsi funkce.
 *
 * Obsahlejsi, podrobnejsi popis cinnosti funkce.
 * Klidne na vice radku. Tato funkce
 *
 * @return stupen serioznosti (vyctovy typ ecodes)
 */
int SeriousFunction()
{

}

int main()
{
    double toughVariable;

    return EXIT_SUCCESS;
}
