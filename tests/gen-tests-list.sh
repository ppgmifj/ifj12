/*
 * Tento skript vypise vsechny nase testy vcetne prvniho radku, kde je uvedeno
 * o jaky test se jedna a jestli to nahodou neni test nad rozsirenimi
 */
 
for i in $( ls -l | grep ^d | awk '{print $9}'| grep -E '[0-9]+'); do
	echo -n "Sekce "
	echo -n $i
	echo -n "   "
	echo -n "Pocet testu: "
	echo $( ls -l ./$i | wc -l)
	for j in $(ls $i); do
		echo -n -e "\t"
		echo -n $j
		echo -n "  "
		echo -n -e "\t"
		echo $(head -n 1 $i/$j)
	done
	echo " "
	echo " "
done

