#!/bin/bash
export LC_ALL=C
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!!!!!!!!!!!!!!!!!! NA TENTO SCRIPT NESAHAT !!!!!!!!!!!!!!!!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#               TOTO JE NUTNE VYPLNIT DLE VASEHO PROJEKTU
# Vystup programu make, ktery vytvori spustitelny soubor s timto nazvem
PROGRAM="ifj12"

# Slozka, kde skript najde umisteni vaseho projektu a provede nad nim Make
PROJECT="../project"

# Slozka kde muzeme ocekavat podslozky pojmenovane exit kody a v nich testy k otestovani
TESTY="$(pwd)"

# Nazev jazyka, jehoz je vas projekt podmnozinou
LANG="FALCON"

# Spustitelny prikaz pro provedeni testu pomoci jazyka, jehoz je vas projekt podmnozinou
LANG_S="falcon -p "$TESTY"/podpora"

# Prikaz pro prelozeni projektu
MAKE="make"

# Prikaz pro clean celeho prelozeneho projektu
MAKE_CLEAN="make clean"

# Prefix pro parametr -n 
NOTEST="notest"

# Pokud je stdout pripojen na terminal, tak vypisujem barevne
if [ -t 1 ]; then
    GREEN_E=32
    RED_E=31
    YELLOW="\033[33m" 
    BLUE=36
else
    GREEN_E=''
    RED_E=''
    YELLOW=''
    BLUE=''     
fi

print_help() {
    echo "Usage: "$0" [-hsvVtpPfSnk] [cisla exit kodu v pripade -o  ||  cislo exit kodu v pripade -k]
        
    Tento skript vyzaduje nasledujici adresarovou strukturu. Pokud ji nebudete akceptovat, nerucim za mozne nasledky :-)

    ROOT_SLOZKA -> \$PROJECT\\\$PROGRAM (obsah promenne \$PROJECT obsahuje nazev vystupu programu make -> obsah promenne \$PROGRAM) 
                -> SLOZKA_S_TESTY\0\* -> testy s exit code 0 (nesmi byt v dalsich podslozkach !!)
                -> SLOZKA_S_TESTY\1\* -> testy s exit code 1
                -> SLOZKA_S_TESTY\2\* -> testy s exit code 2 apod....
                -> SLOZKA_S_TESTY\run_tests.sh  -> tento skript s pravem spousteni 
                   !!! (pri pouziti absolutni cesty v promennych \$PROJECT a \$TESTY je mozno presunout i jinam) !!!

        !!!!! PRED PRVNIM OSTRYM SPUSTENIM SI PROSIM NASTAVTE PROMENNE NAD TOUTO NAPOVEDOU DLE VASEHO PROJEKTU !!!!!


    -h  tato napoveda (nelze kombinovat s zadnym dalsim prepinacem :-) )

    -s  zobrazi testovaci vystupy pro kazdy test zvlast (nikoliv jen sumarizace za kazdy exit code)

    -S  zobrazi pouze summary kazdeho testovaneho exit kodu

    -v  valgrind - zakladni vypisy u jednotlivych testu (nelze pouzit s -V, -S a pri spusteni bez parametru -s)

    -V  valgrind - podrobne vypisy u jednotlivych testu (nelze pouzit s -v, -S a pri spusteni bez parametru -s)

    -t  vypis time u jednotlivych testu (nelze pouzit s -S a pri spusteni bez parametru -s)

    -p  spousti testy po jednom, nikoliv vsechny najednou (nelze pouzit pri spusteni s -S a bez -s)

    -P  spousti jednotlive casti najednou -> nepta se, zda-li ma pokracovat, pokud otestuje jeden exit kod

    -f  zobrazi pouze testy, ktere udelaly fail (nelze pouzit pri spusteni s -S)

    -n  ignoruje testy s prefixem "notest" (napr. "notest-0-test-36.fal") -> vyzaduje spravne nastaveni promenne \$NOTEST v hlavicce skriptu

    -k  testuje od exit kodu zadaneho cislem jako dalsi parametr (cislo oddeleno mezerou od prepinace) [ EXIT KOD JE NUTNE ZADAT VZDY AZ JAKO POSLEDNI PARAMETR 
]

    -o  testuje pouze zadane exit kody jako dalsi parametry programu (jednotlive kody oddeleny mezerou) [ JEDNOTLIVE EXIT KODY JE NUTNE ZADAT VZDY AZ JAKO 
POSLEDNI ]

    "
}
#------------------------------------------------------
# ZPRACOVANI PARAMETRU
vFlag=false; VFlag=false; tFlag=false; pFlag=false; sFlag=false; PFlag=false;
fFlag=false; SFlag=false; nFlag=false; kFlag=false; oFlag=false; uFlag=false;
while getopts ':vVtpPsfSnko' opt; do
        case "$opt" in
                v) vFlag=true;;
                V) VFlag=true;;
                t) tFlag=true;;
                p) pFlag=true;;
                P) PFlag=true;;
                s) sFlag=true;;
                f) fFlag=true;;
                S) SFlag=true;;
                n) nFlag=true;;
                k) kFlag=true;;
                o) oFlag=true;;
                ?) uFlag=true;;
        esac
done
# Zbavime se prepinacu, ktere mame uz zpracovany a vytahneme si zbyle operandy
((OPTIND--))
shift $OPTIND
# Tisk napovedy v pripade chyby
if [ $uFlag = "true" ]; then
        clear
        print_help
        echo " "
        exit 2 
fi 
if ([ $# -gt 0 ] && [ $kFlag = "false" ] && [ $oFlag = "false" ]) || ([ $vFlag = "true" ] && [ $VFlag = "true" ]); then 
        print_help
        echo -e "\t\033[${RED_E}m NELZE SOUCASNE ZADAT -v a -V \033[0m"
        echo " "
        exit 2
fi
if [ $SFlag = "true" ] && ([ $sFlag = "true" ] || [ $vFlag = "true" ] || [ $VFlag = "true" ] || [ $fFlag = "true" ] || [ $pFlag = "true" ] || [ $tFlag = "true" 
]); then 
        print_help
        echo -e "\t\033[${RED_E}m S parametrem -S muzete zadat soucasne jen parametry -P -n -k \033[0m"
        echo " "
        exit 2
fi
if [ $oFlag = "true" ]&& [ $kFlag = "true" ]; then
        print_help
        echo -e "\t\033[${RED_E}m NELZE SOUCASNE POUZIT PARAMETRY -k a -o \033[0m"
        echo " "
        exit 2  
fi

if [ $kFlag = "true" ] && [ $# -ne 1 ]; then
        print_help
        echo -e "\t\033[${RED_E}m S parametrem -k musite zadat i cislo exit kodu, od ktereho se ma testovat \033[0m"
        echo " "
        exit 2
fi

if [ $kFlag = "true" ]; then    
        i_zad=$@
        if ! [[ "$i_zad" =~ ^[0-9]+$ ]] ; then
                print_help
                echo -e "\t\033[${RED_E}m S parametrem -k muzete zadat POUZE A JEN cislo exit kodu \033[0m"
                echo " "
                exit 2
        fi
else
        i_zad=0
fi
if [ $oFlag = "true" ]; then
        if [ $# -eq 0 ]; then
                print_help
                echo -e "\t\033[${RED_E}m Parametr -o musi mit alespon jeden ciselny argument \033[0m"
                echo " "
                exit 2  
        fi

        i_only_p=("$@")
        i_only_p=("$(echo ${i_only_p[*]}  | tr " " "\n" | sort -u -g)")
        for a in $i_only_p; do
                i_only+=($a) 
                if ! [[ "$a" =~ ^[0-9]+$ ]] ; then
                        print_help
                        echo -e "\t\033[${RED_E}m S parametrem -o muzete zadat POUZE A JEN cisla exit kodu \033[0m"
                        echo " "
                        exit 2
                fi              
        done
fi
if [ $sFlag = "false" ] && [ $pFlag = "true" ]; then
        clear
        print_help
        echo " "
        exit 2
fi


#-------------------------------------------------------
#-------------------------------------------------------
# Hlavni testovaci funkce pro cely projekt
zobraz_test (){
        echo "###################################################################################"
        echo -n $COUNT
        COUNT=`expr $COUNT + 1`
        echo -e -n "\t${YELLOW} NOVY TEST - \033"
        echo -e -n "${YELLOW} "$(head -n 1 "$k")" \033[0m"
        echo -e "\t\033[${BLUE}m`tput bold`$k`tput sgr0`\033[0m"
        echo "###################################################################################"
}

test_it() {

clear

# Provedeme opetovny preklad
cd "$PROJECT"
if [ $? -gt 0 ]; then
        echo "Nepodarilo se vstoupit do slozky s projektem. Podivejte se do napovedy a do zahlavi tohoto skriptu"
        exit 99
fi 

$MAKE_CLEAN > "/dev/null" 2> "/dev/null"
if [ $? -gt 0 ]; then
        echo "Nepodarilo se prelozit projekt"
        exit 99
fi 
$MAKE > "/dev/null"
if [ $? -gt 0 ]; then
        echo "Nepodarilo se prelozit projekt"
        exit 99
fi 

# A jdeme do testu a vytvorime si tam pomocne slozky
cd "$TESTY"
mkdir "exit-$LANG" 2> "/dev/null" >"/dev/null"
mkdir "exit-project" 2> "/dev/null" >"/dev/null"
mkdir "out-$LANG" 2> "/dev/null" >"/dev/null"
mkdir "out-project" 2> "/dev/null" >"/dev/null"
rm -fR "exit-$LANG"/* "exit-project"/* "out-$LANG"/* "out-project"/*
EXIT_F=$(pwd)/"exit-$LANG"
EXIT_P=$(pwd)/"exit-project"
OUT_F=$(pwd)/"out-$LANG"
OUT_P=$(pwd)/"out-project"
EXEC=0
INDEX=0
clear
for i in $( ls -l | grep ^d | awk '{print $9}'| grep -E '[0-9]+'); do           
    if [ -d "$i" ] && [ $i -ge $i_zad ] && (([ $oFlag = "true" ] && [ $i -eq ${i_only[$INDEX]} ]) || [ $oFlag = "false" ]); then
        mkdir "$OUT_P/$i" "$OUT_F/$i" "$EXIT_P/$i" "$EXIT_F/$i"
        rm -fR "$OUT_P/$i/*" "$OUT_F/$i/*" "$EXIT_P/$i/*" "$EXIT_F/$i/*"        
        PASSED_T=0
        FAILED_T=0
        PASSED_O=0
        FAILED_O=0
        ALL_T=0
        TIME_FALCON_S=0
        TIME_FALCON_M=0
        TIME_PROJECT_S=0
        TIME_PROJECT_M=0
        COUNT=1
        EXEC=`expr $EXEC + 1`
        if [ $sFlag = "false"  ]; then
                echo -e "\033[${BLUE}mTestuji cast s exit kody cislo - $i \033[0m"                      
        fi
        if [ $nFlag = "false" ]; then
                DIR=$( find "$i/" -type f | grep .fal | sort -d -f )
        else
                DIR=$( find "$i/" -type f | grep .fal | grep -E `echo $NOTEST` -v | sort -d -f )
        fi
        for k in $( find "$i/" -type f | grep .fal | sort -d -f ); do
                ZOBRAZ=0
                FAILED=0

                if [ $pFlag = "true"  ]; then
                        clear
                fi
                if [ $sFlag = "true" ] && [ $fFlag = "false" ]; then
                        zobraz_test
                fi
                ALL_T=`expr $ALL_T + 1`
                $LANG_S $k  >"$OUT_F/$k.out" 2>/dev/null
                echo $? > "$EXIT_F/$k.exit"

                "$PROJECT"/"$PROGRAM" $k  >"$OUT_P/$k.out" 2>/dev/null
                echo $? > "$EXIT_P/$k.exit"
        
                if [ $(cat $EXIT_P/$k.exit) -gt 100 ]; then
                        FAILED_O=`expr $FAILED_O + 1`
                        FAILED_T=`expr $FAILED_T + 1`
                        if [ $SFlag = "false" ] && [ $sFlag = "false" ]; then
                                echo -e -n "\033[${RED_E}m WRONG EXIT-CODE \033[0m"
                                echo -n "in test"
                                echo -e -n "\033[${RED_E}m $(echo "$k" | sed -r -e 's/([[:digit:]]*\/)([[:digit:]]*)/\2/') \033[0m"
                                echo -n "your program returned"
                                echo -e -n "\033[${RED_E}m $(cat $EXIT_P/$k.exit) => PROBABLY SEGFAULT \033[0m" 
                                echo -e -n "\t\033[${RED_E}m WRONG OUTPUT \033[0m"
                                echo -n "in test"
                                echo -e "\033[${RED_E}m $(echo "$k" | sed -r -e 's/([[:digit:]]*\/)([[:digit:]]*)/\2/') \033[0m"
                        fi
                        if [ $sFlag = "true" ]; then
                                if [ $fFlag = "true" ]; then
                                        zobraz_test
                                        FAILED=1                
                                fi
                                echo -e "\033[${RED_E}m !!! SEGMENTATION FAULT !!! \033[0m" 
                        fi      
                else                            
                        if [ $(cat $EXIT_P/$k.exit) -ne $i ]; then
                                if [ $fFlag = "true" ]; then
                                        ZOBRAZ=1
                                        zobraz_test
                                        FAILED=1
                                fi
                                if [ $SFlag = "false" ] && [ $sFlag = "false" ]; then
                                        echo -e -n "\033[${RED_E}m WRONG EXIT-CODE \033[0m"
                                        echo -n "in test"
                                        echo -e -n "\033[${RED_E}m $(echo "$k" | sed -r -e 's/([[:digit:]]*\/)([[:digit:]]*)/\2/') \033[0m"
                                        echo -n "your program returned"
                                        echo -e -n "\033[${RED_E}m $(cat $EXIT_P/$k.exit) \033[0m" 
                                        if [ $i -gt 0 ]; then
                                                echo " "
                                        fi                                      
                                fi
                                if [ $sFlag = "true" ]; then
                                        ZOBRAZ=1
                                        echo -e -n "\033[${RED_E}m WRONG EXIT-CODE \033[0m"
                                        echo -n "expected ($LANG has)"
                                        echo -e -n "\033[${RED_E}m $(cat $EXIT_F/$k.exit) \033[0m"
                                        echo -n "and is ($PROGRAM has)"
                                        echo -e -n "\033[${RED_E}m $(cat $EXIT_P/$k.exit) \033[0m"
                                        echo -n "and should be"
                                        echo -e -n "\033[${RED_E}m $i \033[0m"                                  
                                fi
                                FAILED_T=`expr $FAILED_T + 1`           
                        else
                                if [ $sFlag = "true" ] && [ $fFlag = "false" ]; then
                                        ZOBRAZ=1
                                        echo -e -n "\033[${GREEN_E}m EXIT-CODE OK \033[0m"
                                fi
                                PASSED_T=`expr $PASSED_T + 1`           
                        fi
                        if [ $i -eq 0 ]; then
                                if [ -e "$OUT_P"/$k.out ]; then
                                        diff "$OUT_P"/$k.out "$OUT_F"/$k.out >/dev/null 2>/dev/null
                                        if [ $? -ne 0 ]; then
                                                FAILED_O=`expr $FAILED_O + 1`
                                                if [ $SFlag = "false" ] && [ $sFlag = "false" ]; then
                                                        if [ $FAILED -eq 1 ]; then
                                                                echo -e "\t"
                                                        fi
                                                        echo -e -n "\033[${RED_E}m WRONG OUTPUT \033[0m"
                                                        echo -n "in test"
                                                        echo -e "\033[${RED_E}m $(echo "$k" | sed -r -e 's/([[:digit:]]*\/)([[:digit:]]*)/\2/') \033[0m"
                                                        if [ $FAILED -eq 1 ]; then
                                                                echo " "
                                                        fi
                                                fi
                                                FAILED=1                
                                                if [ $sFlag = "true" ]; then
                                                        if [ $ZOBRAZ -eq 0 ]; then
                                                                zobraz_test
                                                        fi
                                                        echo -e -n "\t\033[${RED_E}m WRONG OUTPUT - $LANG HAS DIFFERENT \033[0m"
                                                        if [ $pFlag = "true" ] && [ $fFlag = "false" ]; then    
                                                                echo " "
                                                                echo " "
                                                                echo -e "\t\033[${RED_E}mPro zobrazeni vystupu programu diff stisknete A nebo a\033[0m"
                                                                read z
                                                                if [ "$z" == "a" ]; then
                                                                        echo " "
                                                                        echo "Prvni radek je obvykle vas projekt, druhy referencni vysledek"
                                                                        diff "$OUT_P"/$k.out "$OUT_F"/$k.out 2>/dev/null
                                                                fi
                                                        fi
                                                        echo " "
                                                fi
                                        else
                                                if [ $SFlag = "false" ] && [ $sFlag = "false" ] && [ $FAILED -eq 1 ]; then
                                                        echo " "        
                                                fi
                                                PASSED_O=`expr $PASSED_O + 1`           
                                                if [ $fFlag = "false" ] && [ $sFlag = "true" ]; then
                                                        echo -e -n "\033[${GREEN_E}m OUTPUT OK \033[0m"
                                                        echo " "
                                                fi              
                                        fi                      
                                fi
                        fi      
                fi              
                if [ $vFlag = "true" ] && ([ $fFlag = "false" ] || [ $FAILED -eq 1 ]); then
                        valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --log-file=valgrind.txt "$PROJECT"/"$PROGRAM" $k  >/dev/null 2>/dev/null
                        echo " "
                        echo "     ---------------------------------------------------------------VALGRIND OUT"
                                cat valgrind.txt | sed 's/^.*[=]/     |/g' | grep "in use at exit"
                                cat valgrind.txt | sed 's/^.*[=]/     |/g' | grep "total heap usage"
                                VAL_TMP=$(cat valgrind.txt | sed 's/^.*[=]/     |/g' | grep "ERROR SUMMARY" | grep "0 errors from 0 contexts")
                                if [ -z $VAL_TMP ]; then
                                        echo -e  "\033[${RED_E}m$(cat valgrind.txt | sed 's/^.*[=]/     |/g' | grep "ERROR SUMMARY")\033[0m"
                                else
                                        echo -e  "\033[${GREEN_E}m$(cat valgrind.txt | sed 's/^.*[=]/     |/g' | grep "ERROR SUMMARY")\033[0m"
                                fi
                        echo "     ---------------------------------------------------------------VALGRIND OUT"
                        echo " "
                fi
                if [ $VFlag = "true" ] && ([ $fFlag = "false" ] || [ $FAILED -eq 1 ]); then
                        valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --log-file=valgrind.txt "$PROJECT"/"$PROGRAM" $k  >/dev/null 2>/dev/null
                        echo " "
                        echo "     ---------------------------------------------------------------VALGRIND TRACE OUT"
                                cat valgrind.txt | sed 's/^.*[=]/     |/g' | ./tail2 -n +6
                        echo "     ---------------------------------------------------------------VALGRIND TRACE OUT"
                        echo " "
                fi

                TIME="$(sh -c "time $LANG_S $k &> /dev/null" 2>&1)"
                TIME_FALCON_S=`echo "$TIME_FALCON_S + $(echo $TIME | awk '{print $2}' | sed -r -e 's/(^[[:digit:]]+)m([[:digit:]]+\.[[:digit:]]+)s/\2/')" | bc 
-l`
                TIME_FALCON_M=`echo "$TIME_FALCON_M + $(echo $TIME | awk '{print $2}' | sed -r -e 's/(^[[:digit:]]+)m([[:digit:]]+\.[[:digit:]]+)s/\1/')" | bc 
-l`

                TIME_P="$(sh -c "time "$PROJECT"/"$PROGRAM" $k &> /dev/null" 2>&1)"
                TIME_PROJECT_S=`echo "$TIME_PROJECT_S + $(echo $TIME_P | awk '{print $2}' | sed -r -e 's/(^[[:digit:]]+)m([[:digit:]]+\.[[:digit:]]+)s/\2/')" | 
bc -l`
                TIME_PROJECT_M=`echo "$TIME_PROJECT_M + $(echo $TIME_P | awk '{print $2}' | sed -r -e 's/(^[[:digit:]]+)m([[:digit:]]+\.[[:digit:]]+)s/\1/')" | 
bc -l`
                if [ $tFlag = "true" ] && ([ $fFlag = "false" ] || [ $FAILED -eq 1 ]); then
                        echo "     ---------------------------------------------------------------TIME"
                        echo -e "\t $LANG"
                                echo -n -e "\t\tReal: "
                                echo "$(echo $TIME | awk '{print $2}')"
                                echo -n -e "\t\tUser: "                         
                                echo $(echo $TIME | awk '{print $4}')                   
                                echo -n -e "\t\tSyst: "                         
                                echo $(echo $TIME | awk '{print $6}')                   
                        echo -e "\t $PROGRAM"
                                echo -n -e "\t\tReal: "
                                echo $(echo $TIME_P | awk '{print $2}')                         
                                echo -n -e "\t\tUser: "                         
                                echo $(echo $TIME_P | awk '{print $4}')                         
                                echo -n -e "\t\tSyst: "                         
                                echo $(echo $TIME_P | awk '{print $6}')                         
                        echo "     ---------------------------------------------------------------TIME"
                fi
                if [ $sFlag = "true" ] && ([ $fFlag = "false" ] || [ $FAILED -eq 1 ]); then
                        echo " "
                        echo " "
                fi
                if [ $pFlag = "true" ] && ([ $fFlag = "false" ] || [ $FAILED -eq 1 ]); then
                        echo "Chcete pokracovat dalsim testem ? (A/N) - default A"
                        read y
                        if [ "$y" = "N" -o "$y" = "n" ]; then
                                $MAKE_CLEAN > "/dev/null" 2> "/dev/null"
                                rm -f "$i"/*.fam valgrind.txt
                                rm -fR "exit-$LANG"/$i/* "exit-project"/$i/* "out-$LANG"/$i/* "out-project"/$i/*
                                rmdir "exit-$LANG"/$i "exit-project"/$i "out-$LANG"/$i "out-project"/$i
                                rmdir "exit-$LANG" 2> "/dev/null" >"/dev/null"
                                rmdir "exit-project" 2> "/dev/null" >"/dev/null"
                                rmdir "out-$LANG" 2> "/dev/null" >"/dev/null"
                                rmdir "out-project" 2> "/dev/null" >"/dev/null"
                                exit 1
                        fi
                fi
        done
        echo "#######################################################################"
        echo -e "\t${YELLOW}CAST CISLO $i VYHODNOCENA\033[0m"
        echo " "
        echo -e -n "\tCelkovy cas real $LANG: "
        echo -e -n "\033[${GREEN_E}m$TIME_FALCON_M\033[0m"
        echo -e -n "\033[${GREEN_E}mm\033[0m"
        echo -e "\033[${GREEN_E}m$TIME_FALCON_S\033[0m"
        echo " "
        echo -e -n "\tCelkovy cas real $PROGRAM: "
        echo -e -n "\033[${GREEN_E}m$TIME_PROJECT_M\033[0m"
        echo -e -n "\033[${GREEN_E}mm\033[0m"
        echo -e "\033[${GREEN_E}m$TIME_PROJECT_S\033[0m"
        echo " "
        echo " "        
        echo -e "Bylo provedeno celkem\033[${GREEN_E}m $ALL_T\033[0m testu - Uspesnost exit kodu \
        \033[${GREEN_E}m `echo "($PASSED_T / $ALL_T * 100)" | bc -l | sed -r -e \
        's/([[:digit:]]+\.[[:digit:]]{0,2})([[:digit:]]*)/\1/'`%\033[0m"
        echo " "        
        echo -e -n "\t\033[${GREEN_E}mPASSED EXIT-CODE - \033[0m"
        echo -n $PASSED_T
        echo -n -e "\t\t"
        echo -e -n "\033[${RED_E}mFAILED EXIT-CODE - \033[0m"
        echo $FAILED_T
        if [ $i -eq 0 ]; then
                echo " "        
                echo -e "Bylo provedeno celkem\033[${GREEN_E}m $ALL_T\033[0m testu - Uspesnost output diffu\
                \033[${GREEN_E}m `echo "($PASSED_O / $ALL_T * 100)" | bc -l | sed -r -e \
                's/([[:digit:]]+\.[[:digit:]]{0,2})([[:digit:]]*)/\1/'`%\033[0m"
                echo " "        
                echo -e -n "\t\033[${GREEN_E}mPASSED OUTPUT - \033[0m"
                echo -n $PASSED_O
                echo -n -e "\t\t"
                echo -e -n "\033[${RED_E}mFAILED OUTPUT - \033[0m"
                echo $FAILED_O
        fi      
        echo "#######################################################################"
        echo " "
        echo " "
        if [ $PFlag = "false" ]; then
                echo "Chcete pokracovat v dalsich castech ? (A/N) - default A"
                read x
                if [ "$x" = "N" -o "$x" = "n" ]; then
                        $MAKE_CLEAN > "/dev/null" 2> "/dev/null" 
                        rm -f "$i"/*.fam valgrind.txt
                        rm -fR "exit-$LANG"/$i/* "exit-project"/$i/* "out-$LANG"/$i/* "out-project"/$i/*
                        rmdir "exit-$LANG"/$i "exit-project"/$i "out-$LANG"/$i "out-project"/$i
                        rmdir "exit-$LANG" 2> "/dev/null" >"/dev/null"
                        rmdir "exit-project" 2> "/dev/null" >"/dev/null"
                        rmdir "out-$LANG" 2> "/dev/null" >"/dev/null"
                        rmdir "out-project" 2> "/dev/null" >"/dev/null"
                        exit 1
                fi
        fi
        rm -f "$i"/*.fam valgrind.txt
        rm -fR "exit-$LANG"/$i/* "exit-project"/$i/* "out-$LANG"/$i/* "out-project"/$i/*
        rmdir "exit-$LANG"/$i "exit-project"/$i "out-$LANG"/$i "out-project"/$i
        INDEX=`expr $INDEX + 1`
    fi
done
$MAKE_CLEAN > "/dev/null" 2> "/dev/null"
rmdir "exit-$LANG" 2> "/dev/null" >"/dev/null"
rmdir "exit-project" 2> "/dev/null" >"/dev/null"
rmdir "out-$LANG" 2> "/dev/null" >"/dev/null"
rmdir "out-project" 2> "/dev/null" >"/dev/null"
}

test_it 2>/dev/null

if [ $EXEC -eq 0 ]; then
        echo " "
        echo " "
        echo -e "\033[${RED_E}m !!! NEBYLY PROVEDENY ZADNE TESTY -> ZREJME SPATNE ZADANE CISLO \
        PARAMETRU 'n' NEBO SLOZKA S TESTY NEOBSAHUJE PODSLOZKY POJMENOVANE EXIT KODY !!! \033[0m"
        echo " "
        echo " "
else
        echo -e "\t\t\t\033[${GREEN_E}m !!! HOTOVO !!! \033[0m"
        echo " "
        echo " "
fi
