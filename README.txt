Týmový projekt do předmětu IFJ -- Formální jazyky a překladače.

Tým č. 71, varianta: a) 4) I).

Další info viz https://www.fit.vutbr.cz/study/courses/IFJ/public/project/
               http://www.bum3rang.cz/ifj

Dokumentace: http://www.bum3rang.cz/ifj/dokumentace

Git statistiky: http://www.bum3rang.cz/ifj/git-statistiky


Složení týmu:
    Petr Jirout
    Gabriel Branderský
    Petr Huf
    Matěj Vaňátko

Rozdělení práce (základní):

    Lexikální analyzátor (scanner) - Matěj Vaňátko
    Syntaktický a sémantický analyzátor - Petr Huf, Gabriel Branderský
    Interpret - Petr Jirout
