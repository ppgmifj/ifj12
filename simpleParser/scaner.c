#include <stdio.h>
#include "scaner.h"


// promenna pro ulozeni vstupniho souboru
FILE *source;

void SetSourceFile(FILE *f)
{
    source = f;
}
/**
 * Simulacia lexilalneho analyzatoru
 * @return 
 */
int GetNextToken()
{
    int token = fgetc(source);
    switch (token) {
        case 'a': token = A_TER;
        break;
        
        case 'b': token = B_TER;
        break;
        
        case EOF: token = END_OF_INPUT;
        break;
        
        default: token = N_KNOWN_TER;  // TOKEN nebol rozpoznany
    }
    
    return  token;
}

