/* Terminaly */
enum terminals
{
    A_TER,
    B_TER,
    END_OF_INPUT,
    COUNT_TER,  // pocet terminalov
    N_KNOWN_TER
};

//hlavicka funkce simulujici lexikalni analyzator
void SetSourceFile(FILE *f);
int GetNextToken();