/**
 * @file main.c
 * @brief Syntakticky analyzator pre jednoduchy jazyk.
 */
/*
 * *******************************************************************
 * Date:         14.10. 2012
 * Author(s):    Gabriel Brandersky
 * Project:      Interpret IFJ12
 * Team:         Petr Jirout,         xjirou07
 *               Petr Huf,            xhufpe00
 *               Matej Vanatko,       xvanat01
 *               Gabriel Brandersky,  xbrand04
 * Description:
 * Program overi ci vstupny subor je syntakticky spravny
 * podla jazyka:
 * L = {a^n . b^n : n >= 1}
 * Vstupny subor je zadany ako parameter programu, ukazka:
 * ./parser inputFile
 * 
 * LL gramatika daneho jazyka
 * N = {S, P}      neterminaly
 * T = {a, b}      terminaly
 * P = {           pravidla
 *      S->aPb, 
 *      P->aPb
 *      P->eps.
 *     }  
 ********************************************************************
 */

#include <stdio.h>

// konstanta EXIT_SUCCESS
#include <stdlib.h>

// konstanty true / false
#include <stdbool.h>

#include "scaner.h"

/**
 * Vyctovy typ pre chybove kody programu 
 */
enum ecodes
{
    EOK = 0,             ///< Bez chyby
    ELEXEM,              ///< lexikalna chyba - chybna struktura lexemu
    ESYNTAX,             ///< syntakticka chyba - chybna struktura programu
    
    /* semanticke chyby */
    EVAR_SEM,            ///< Nedefinovana premena 
    EFUNC_SEM,           ///< Nedefinovana funkcia 
    EOTHER_SEM,          ///< Ostatne semanticke chyby
    
    /* Chyby za behu */
    EDIV_ZERO = 10,      ///< Delenie nulov
    EN_COMPATIBLE_TYPE,  ///< Nekompatiblne typy
    ECASTING,            ///< Chyba pretypovani - funkce numeric
    EOTHER_RUN,          ///< Ostatne behove chyby
    
    /* Interna chyba - neovplyvnena vstupnym suborom */
    EINTER = 99,         ///< malloc, fopen, atd.
};

/* Skutocny nazov tokenov */
// vyuziva debuggovacia funkcia printToken
const char *T_NAMES[] = 
{
    [A_TER] = "a",
    [B_TER] = "b",
    [N_KNOWN_TER] = "#",
    [END_OF_INPUT] = "$"
};


/**
 * Debugovacia funkcia na vypisanie tokenu
 */
void printToken(int token)
{
    if (token < 0 || token >= COUNT_TER)
    { token = N_KNOWN_TER; }
    
    printf("%s", T_NAMES[token]);
} 

/**
 * P -> aPb
 */
int PRule(int *token)
{
    int correct = false;
    
    if (*token == A_TER) {
        *token = GetNextToken();
        
        // simulujeme pravidlo 2. P -> aPb
        correct = PRule(token); 
        
        if (*token != B_TER)
            return false;
        
        *token = GetNextToken();
    }
    else if (*token == B_TER) {
        // simulujeme pravidlo 3. P -> eps
        correct = true;
    }
    else 
        return false;
        
    return true;
}

/**
 * S -> aPb
 */
int SRule()
{
    // predpokladame chybu
    int correct = false;
    
    // ziskame PRVY token
    int token = GetNextToken();
    
    //  aplikujeme pravidlo - zaciname odstaranenim 'a' terminalu
    if (token == A_TER)
        token = GetNextToken();
    else
        return false;
    
    correct = PRule(&token);
    
    //  tentoraz odstaranenime 'b' terminal
    if (token == B_TER)
        token = GetNextToken();
    else
        return false;
    
    // NOTE tu moze byt kontrola end of input
    
    return correct;
}

int main(int argc, char** argv)
{
    // TODO je to urcite interna chyba?
    if (argc != 2)
        return EINTER; // nebol zadany vstupny subor
    
    FILE *file;
    file = fopen(argv[1],"r");
    if (file == NULL)
        return EINTER; // nepodarilo sa otvorit subor

    SetSourceFile(file);

    // TODO ako kontrolovat lexikalnu chybu
    if (SRule())
        printf("Spravny kod!\n");
    else
        printf("Nespravny kod!\n");
     
    return EXIT_SUCCESS;
}