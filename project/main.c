/**
 * @file main.c
 * @brief Interpret IFJ12
 */
/*
 * *******************************************************************
 * Date:         25. 10. 2012
 * Author(s):    Gabriel Brandersky
 * Project:      Interpret IFJ12
 * Team:         Petr Jirout,         xjirou07
 *               Petr Huf,            xhufpe00
 *               Matej Vanatko,       xvanat01
 *               Gabriel Brandersky,  xbrand04
 * Description:
 * Interpret jazyka IFJ12, ktory je podmnozinou jazyka Falcon
 * Ako prvy a jediny parameter ma meno suboru so zdrojovym kodom, ktory
 * sa ma interpretovat
 * *******************************************************************
 */

#define OPTIMIZE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "global_types.h"
#include "scanner.h"
#include "ial.h"
#include "parser.h"
#include "interpreter.h"
#include "byte_code.h"


int main(int argc, char* argv[])
{
    int byteCode=0;
    int retCode=0;

    if (argc == 2)
        byteCode = 0;
    else if (argc == 3) {
      // Kontrola parametru
      if (strcmp(argv[2],"-g") == 0)
        byteCode = 1;
      else if (strcmp(argv[2],"-b") == 0)
        byteCode = 2;
    }
    else
      return E_INTER;

    // Pokud provadime klasickou interpretaci
    if (byteCode == 0) {
      // otvorime subor do globalnej premenej z lex. analyzatoru
      FileGlobal = fopen(argv[1],"r");
      if (FileGlobal == NULL)
        return E_INTER; // nepodarilo sa otvorit subor

      // vykona syntakticku kontrolu
      retCode = Parse();
#ifdef DEBUG_PARSER
      printf("Syntakticka analyza: %d\n", retCode);
#endif

      if (retCode == E_OK)
      {
          retCode = Interpreter(conTable, instListGlobal);
      }
    }
    else if (byteCode == 1) {        // Generujeme byte-code
        // otvorime subor do globalnej premenej z lex. analyzatoru
        FileGlobal = fopen(argv[1],"r");
        if (FileGlobal == NULL)
            return E_INTER; // nepodarilo sa otvorit subor

        // vykona syntakticku kontrolu
        retCode = Parse();

        if (retCode == E_OK)
        {
            retCode = GenerateByteCode();
        }
    }
    else {                        // Interpretujeme byte-code
      // otvorime subor do globalnej premenej z lex. analyzatoru
      FileGlobal = fopen(argv[1],"r");
      if (FileGlobal == NULL)
        return E_INTER; // nepodarilo sa otvorit subor

      retCode = DoByteCode();
    }


#ifdef DEBUG_PARSER
    if (retCode != E_OK)
       fprintf(stderr,"CHYBA!!! asi riadok c. %d\n ",TokenInfoGlobal.line);
#endif



#ifdef DEBUG_INTERPRETER
    char* errs[100];
    errs[0]  = "E_OK";
    errs[1]  = "E_LEXEM";
    errs[2]  = "E_SYNTAX";
    errs[3]  = "E_VAR_SEM";
    errs[4]  = "E_FUNC_SEM";
    errs[5]  = "E_OTHER_SEM";
    errs[10] = "E_DIV_ZERO";
    errs[11] = "E_INCOMPATIBLE_TYPE";
    errs[12] = "E_CASTING";
    errs[13] = "E_OTHER_RUN";
    errs[99] = "E_INTER";

    if (retCode != E_OK)
        fprintf(stderr,"\n#########################\nPPGM_IFJ12 hlasi chybu: %s\n##########################\n",errs[retCode]);
#endif

    // dealokacia tabulky konstant, zoznamu instrukci
    ConTableDestroy(conTable);
    InstListDestroy(instListGlobal);
    //dealokace retezce pro token

    if (byteCode != 2)
      free(TokenInfoGlobal.string);

    fclose(FileGlobal);
#ifdef DEBUG_INTERPRETER
    if (retCode == E_OK)
        fprintf(stderr,"\n###################__Everything_went_well!__###################\n");
#endif

    return retCode;
}
