/**
 * @file builin.c
 * @brief Implementace vestavenych funkci.
 */
/*
 * *******************************************************************
 * Date:         15.11. 2012
 * Author(s):    Petr Huf
 * Project:      Interpret IFJ12
 * Team:         Petr Jirout,         xjirou07
 *               Petr Huf,            xhufpe00
 *               Matej Vanatko,       xvanat01
 *               Gabriel Brandersky,  xbrand04
 * Description:
 * Implementace vestavenych funkci interpretu
 ********************************************************************
 */
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "interpreter.h"
#include "str.h"
#include "global_types.h"
#include "ial.h"
#include "builtin.h"
#include "scanner.h"

#define BuiltDEBUG 0

#if BuiltDEBUG

#define INPUT 0
#define NUMERIC 0
#define PRINT 0
#define TYPEOF 0
#define LENGTH 0
#define FIND 1
#define SORT 0

int main()
{

  int error;
  putchar('\n');
  printf("---------------------------------------------\n");
#if INPUT
  valueStackItem_t item;

  error=Input(&item);
  printf("%s",item.value.string);
#endif

#if NUMERIC
  valueStackItem_t item,dest;


  item.type=ST_STRING;
  item.value.string="   212  3";
  error=Numeric(&item,&dest);
  printf("Prevedene cislo: %lf",dest.value.number);
#endif

#if PRINT
  valueStackItem_t item;

  item.type=ST_LOGIC;
  item.value.number=5;

  error=Print(&item);
#endif

#if TYPEOF
  valueStackItem_t item,dest;

  item.type=ST_FUNC;
  item.value.number=5;


  error=TypeOf(&item,&dest);
  printf("Vraceno: %lf",dest.value.number);
#endif

#if LENGTH
  valueStackItem_t item,dest;
  error=0;

  item.type=ST_STRING;
  item.value.string="ahojda";


  Len(&item,&dest);
  printf("Delka: %lf",dest.value.number);
#endif

#if FIND
  valueStackItem_t item1,item2,dest;

  item1.type=ST_STRING;
  item1.value.string="nazdarek jak se mas?";

  item2.type=ST_STRING;
  item2.value.string=NULL;


  error=Find(&item1,&item2,&dest);
  printf("Pozice: %lf",dest.value.number);
#endif

#if SORT
  valueStackItem_t item,dest;

  item.type=ST_STRING;
  item.value.string="231";


  error=Sort(&item,&dest);
  printf("Serazeno: %s",dest.value.string);
#endif


  printf("\n---------------------------------------------");
  printf("\n\nKONEC: %d\n\n",error);

  return 0;
}

#endif

// Funkce cte retezec ze vstupu a vraci ho formou parametru
ecodes_e Input(valueStackItem_t* dest)
{
  string read;

  if (strInit(&read)) // inicializace retezce kam budeme ukladat
    return E_INTER;

  int c;
  while ((c=getchar()) != '\n')
    if (strAddChar(&read,c))
      return E_INTER;

  dest->type=ST_STRING;
  dest->value.string=read.str;

  return E_OK;
}

// Funkce prevadi cislo na retezec
ecodes_e Numeric(valueStackItem_t* item, valueStackItem_t* dest)
{
  int type=item->type;

  // Neplatne typy
  if (type == ST_DEFAULT || type == ST_LOGIC || type == ST_NIL || type == ST_FUNC)
    return E_CASTING;

  // Pro typ cislo, vratime to stejne cislo
  if (type == ST_NUMBER) {
    dest->value.number=item->value.number;
    dest->type = ST_NUMBER;
    return E_OK;
  }

  // Kontrola,zda cislo vyhovuje definici ciselneho literalu
  char *cislo=item->value.string;
  int i, lastI;

  i = type =  0;

  // Odsekne bile znaky na zacatku retezce
  while(isspace(cislo[i]))
    ++i;

  lastI = i;

  // Cteme celou cast cisla
  while (isdigit(cislo[i]))
    ++i;

  // Pokud cela cast cisla je prazdna
  if (lastI == i)
    return E_CASTING;

  if (cislo[i] == '.')
    type = 0;                 // Cela a desetinna cast nebo cela, desetinna a exponencionalni cast
  else if (cislo[i] == 'e')
    type = 1;                // Cela a exponencionalni cast
  else
    return E_CASTING;

  // Pokud cteme desetinnou cast
  if (type == 0) {
    ++i;  // Posunume se na dalsi znak
    lastI = i;

    // Cteme desetinnou cast nebo exponencialni cast
    while (isdigit(cislo[i]))
      ++i;

    // Pokud nebyla nactena zadna cislice
    if (lastI == i)
      return E_CASTING;
  }



  // Pokud musime cist exponent
  if (type == 1 || (type == 0 && cislo[i] == 'e')) {
    if (cislo[i] == 'e') {
       // Cteme jeste exponent
      ++i;
      if (!(cislo[i] == '+' || cislo[i] == '-' || isdigit(cislo[i])))  // Test na nepovinne zmanenko
        return E_CASTING;
      if (!isdigit(cislo[i]))     // Pokud jsme neprecetli cislo, presuneme se na dalsi znak
        ++i;
      lastI = i;
      while (isdigit(cislo[i]))
         ++i;
      if (lastI == i)
        return E_CASTING;
    }
    else
      return E_CASTING;
  }

  // Musime prevest string na cislo
  if ((sscanf(item->value.string,"%lf",&dest->value.number)) != 1)
    return E_CASTING;

  dest->type = ST_NUMBER;

  return E_OK;
}

// Funkce tiskne term na standardni vystup
ecodes_e Print(valueStackItem_t* item, valueStackItem_t* dest)
{
  int type=item->type;
  if (type == ST_DEFAULT || type == ST_FUNC)
    return E_OTHER_RUN;

  if (type == ST_STRING) {
    int c,i=0;
    while ((c=item->value.string[i++]) != '\0')
      if (c != '\\')
	putchar(c);
      else {                          // Tisk escape sekvenci
	c=item->value.string[i++];
	if (c=='n')
	  putchar('\n');
	else if (c=='t')
	  putchar('\t');
	else if (c=='\\')
	  putchar('\\');
	else if (c=='"')
	  putchar('"');
	else if (c=='x') {
	  int chars[2];
          int number;
          for (int k=0;k<=1;++k) {
            chars[k]=item->value.string[i++];
            if (isdigit(chars[k]))
              chars[k] -= '0';
            else if (chars[k]>='A' && chars[k]<='F')
              chars[k] = chars[k] - 'A'+10;
            else if (chars[k]>='a' && chars[k]<='f')
              chars[k] = chars[k] - 'a'+10;
            else
              return E_OTHER_RUN;
           }
           number=16*chars[0]+chars[1];
           putchar(number);
          }
          else
            return E_OTHER_RUN;
      }

  }
  else if (type == ST_NIL)
    fputs("Nil",stdout);
  else if (type == ST_NUMBER)
    printf("%g",item->value.number);
  else if (type == ST_LOGIC) {
    if (item->value.number > 0)
      fputs("true",stdout);
    else
      fputs("false",stdout);
  }

  dest->type = ST_NIL;

  return E_OK;
}

// Funkce v parametru vraci typ predaneho id
ecodes_e TypeOf(valueStackItem_t* item, valueStackItem_t* dest)
{
  int varType=item->type;
  int type;

  if (varType == ST_DEFAULT)
    return E_OTHER_RUN;

  if (varType == ST_STRING)
    type = 8;
  else if (varType == ST_NIL)
    type = 0;
  else if (varType == ST_LOGIC)
    type = 1;
  else if (varType == ST_NUMBER)
    type = 3;
  else
    type = 6;   // varType == ST_FUNC

  dest->type = ST_NUMBER;
  dest->value.number = type;

  return E_OK;
}

// Funcke vraci delku retezce jinak 0
void Len(valueStackItem_t* item, valueStackItem_t* dest)
{

  dest->type = ST_NUMBER;

  if (item->type != ST_STRING)
    dest->value.number = 0;
  else
    dest->value.number = strlen(item->value.string);

}

// Funkce hledat podretezec (item2) v retezci (item1) a vraci jeho pozici
ecodes_e Find(valueStackItem_t* item1,valueStackItem_t* item2, valueStackItem_t* dest)
{
  if (item1->type != ST_STRING || item2->type != ST_STRING ||
      item1->value.string == NULL || item2->value.string ==NULL)
    return E_UNCOMPATIBLE_TYPE;

  dest->type = ST_NUMBER;
  dest->value.number = KMPSearch(item1->value.string, item2->value.string);

  return E_OK;
}

// Funkce seradi retezec predany v polozce item a vraci ho serazeny v parametru
ecodes_e Sort(valueStackItem_t* item, valueStackItem_t* dest)
{
  if (item->type != ST_STRING)
    return E_UNCOMPATIBLE_TYPE;

  int length=strlen(item->value.string);


  dest->value.string = malloc(length + 1);
  if (dest->value.string == NULL)
    return E_INTER;

  dest->type = ST_STRING;

  strcpy(dest->value.string,item->value.string);

  // Prazdny retezecradit nebudeme
  if (length != 0)
    MergeSort(dest->value.string,0,length-1);

  return E_OK;
}
