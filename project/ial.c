/**
* @file ial.c
* @brief Merge Sort, KMP, binarni strom, tabulka symbolu, tabulka konstant.

*  Implementace Merge Sortu a Knuth-Moris-Prattova algoritmu pro hledani
*  podretezce.
*/
/*
*********************************************************************
* File:         ial.c
* Date:         14. 10. 2012
* Author:       Petr Jirout
* Project:      Interpret IFJ12
* Team:         Petr Jirout,        xjirou07
*               Petr Huf,           xfugpe00
*               Matej Vanatko,      xvanat01
*               Gabriel Brandersky  xbrand04
*********************************************************************
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>

#include "ial.h"
#include "global_types.h"

//porovnavaci funkce
static inline int BSTNodeStrCmp(const symTable_t* table, const btNode_t* first, const char* string);
static inline int BSTNodeCmp(const symTable_t* table, const btNode_t* first, const btNode_t* second);
//rekurzivni uvolneni binarniho stromu
static void BSTDestroy(btNode_t* root);
//vlozeni prvku do tabulky retezcu
static unsigned int ConTableInsert(conTable_t* conTable, ST_TYPE itemInType, union stValue itemInValue);

/**
* Vlozi novy prvek do binarniho stromu.
*
* Pokud jiz prvek se stejnym jmenem existuje, aktualizuje jeho hodnotu a vraci jeho adresu.
*
* @warning Vytvari nevyvazeny binarni strom.
* @param table Ukazatel na tabulku, do ktere chci prvek vlozit. (symTable_t*)
* @param data Data, ktera vlozim do noveho prvku, slouzi soucasne jako vyhledavaci klic. (int)
* @return Ukazatel na vlozeny prvek. Vraci NULL pokud se nezdarila alokace pro novy prvek. (btNode_t*)
*/
btNode_t* SymTableInsert(symTable_t* table, conTable_t* conTable, stData_t itemIn)
{
    if (table == NULL || table->root == NULL) //nevalidni koren stromu
        return NULL;

    btNode_t* root = table->root; //lokalni kopie, nesmim menit puvodni hodnotu

    size_t itemNameLength = strlen(itemIn.tempName); //delka nazvu identifikatoru

    if (table->chainIndex == 0) //cisty strom, po inicializaci, bez prvku
    {
        root->data.nameIndex = table->chainIndex;
        (void) strcpy(table->nameChain + table->chainIndex, itemIn.tempName);
        table->chainIndex += itemNameLength + 1;
        root->data.type = itemIn.type;
        root->data.stackIndex = table->stackIndexCounter++; //priradim novy index do zasobniku

        //Ulozim hodnotu
        if (itemIn.type != ST_DEFAULT)
        {
            root->data.loaded = 0;//konstanta jeste nebyla nactena do zasobniku hodnot
            root->data.conTableIndex = ConTableInsert(conTable, itemIn.type, itemIn.value);
            if (root->data.conTableIndex == UINT_MAX)
                return NULL;
        }

        return root;
    }

    btNode_t* newItem = malloc(sizeof(*newItem));
    if (newItem == NULL)
        return NULL;


    if ((itemNameLength + 1 + table->chainIndex) >= table->chainLengthLimit) //malo mista v retezu jmen
    {
        char* temp = realloc(table->nameChain, table->chainLengthLimit + SYM_TABLE_CHAIN_SIZE);
        if (temp == NULL)
        {
            free(newItem);
            return NULL;
        }

        table->nameChain = temp;
        table->chainLengthLimit += SYM_TABLE_CHAIN_SIZE; //zvetsim limit pro retez jmen
    }

    //nakopiruji identifikator do retezu jmen
    (void) strcpy((table->nameChain)+(table->chainIndex), itemIn.tempName);

    newItem->data.nameIndex = table->chainIndex; //ulozim si index do retezu jmen
    table->chainIndex += itemNameLength + 1;     //zvetsim celkovy index

    newItem->left = NULL;
    newItem->right = NULL;
    newItem->data.type = itemIn.type; //nahraji obsah do nove vytvoreneho prvku


    if (root->left == NULL && root->right == NULL ) //mam pouze jeden prvek ve stromu
    {
        int check;
        if ((check = BSTNodeCmp(table, newItem, root)) > 0) //novy prvek pichnu na odpovidajici nohu
        {
            root->right = newItem;
            newItem->parent = root;
            newItem->data.stackIndex = table->stackIndexCounter++; //priradim novy index do zasobniku

            //pokud znam typ, ulozim hodnotu do tabulky konstant
            if (itemIn.type != ST_DEFAULT)
            {
                newItem->data.loaded = 0; //konstanta jeste nebyla nactena do zasobniku hodnot
                newItem->data.conTableIndex = ConTableInsert(conTable, itemIn.type, itemIn.value);
                if (newItem->data.conTableIndex == UINT_MAX)
                    return NULL;
            }

            return newItem;
        }
        else if (check ==  0) //stejny prvek uz ve stromu mam, pouze aktualizuji
        {
            //Ulozim hodnotu
            if (itemIn.type == ST_STRING)
            { //zkopirovat novy retezec na misto stareho, realokovat
                if (strlen(conTable->items[root->data.conTableIndex].value.string) < strlen(itemIn.value.string))
                {
                    char* temp = realloc(conTable->items[root->data.conTableIndex].value.string, strlen(itemIn.value.string)+1);
                    if (temp == NULL)
                       return NULL;

                    conTable->items[root->data.conTableIndex].value.string = temp;
                }

                strcpy(conTable->items[root->data.conTableIndex].value.string, itemIn.value.string);
                root->data.value.string = conTable->items[root->data.conTableIndex].value.string;
            }
            else if (itemIn.type == ST_DEFAULT) //neznam typ, nevkladam do tabulky konstant
            {
                ;
            }
            else //ST_NIL, ST_LOGIC, ST_NUMBER, ST_ARRAY, aktualizuji hodnotu
            {
                conTable->items[root->data.conTableIndex].value.number = itemIn.value.number;
                root->data.value.number = itemIn.value.number;
            }

            free(newItem); //uvolnim zbytecnou alokaci
            //odecist hodnotu z retezu jmen
            table->chainIndex -= itemNameLength + 1;
            return root;

        }
        else //check < 0
        {
            root->left = newItem;

            newItem->parent = root;
            newItem->data.stackIndex = table->stackIndexCounter++; //priradim novy index do zasobniku

            //pokud znam typ, ulozim hodnotu do tabulky konstant
            if (itemIn.type != ST_DEFAULT)
            {
                newItem->data.loaded = 0; //konstanta jeste nebyla nactena do zasobniku hodnot
                newItem->data.conTableIndex = ConTableInsert(conTable, itemIn.type, itemIn.value);
                if (newItem->data.conTableIndex == UINT_MAX)
                    return NULL;
            }

            return newItem;
        }
    }


    while (root != NULL) //dokud nevlozim prvek na spravne misto
    {
        if (BSTNodeCmp(table, newItem, root) == 0) //prvek se stejnym nazvem jiz ve stromu existuje
        {  //aktualizuji jeho hodnotu
            #ifdef DEBUG_IAL
            printf("Item with same data has been found -- skipping.\n");
            #endif
            root->data.type = itemIn.type;

            //Ulozim hodnotu
            if (itemIn.type == ST_STRING)
            { //zkopirovat novy retezec na misto stareho, realokovat
                if (strlen(conTable->items[root->data.conTableIndex].value.string) < strlen(itemIn.value.string))
                {
                    char* temp = realloc(conTable->items[root->data.conTableIndex].value.string, strlen(itemIn.value.string)+1);
                    if (temp == NULL)
                       return NULL;

                    conTable->items[root->data.conTableIndex].value.string = temp;
                }

                strcpy(conTable->items[root->data.conTableIndex].value.string, itemIn.value.string);
                root->data.value.string = conTable->items[root->data.conTableIndex].value.string;
            }
            else if (itemIn.type == ST_DEFAULT) //neznam typ, nevkladam do tabulky konstant
            {
                ;
            }
            else //ST_NIL, ST_LOGIC, ST_NUMBER, ST_ARRAY, aktualizuji hodnotu
            {
                conTable->items[root->data.conTableIndex].value.number = itemIn.value.number;
                root->data.value.number = itemIn.value.number;
            }

            free(newItem); //uvolnim zbytecnou alokaci
            //odecist hodnotu z retezu jmen
            table->chainIndex -= itemNameLength + 1;
            return root;
        }

        //aktualni koren nema zadne syny
        if (root->left == NULL && root->right == NULL)
        {
            if (BSTNodeCmp(table, newItem, root) > 0)
                root->right = newItem;
            else
                root->left = newItem;

            newItem->parent = root;
            newItem->data.stackIndex = table->stackIndexCounter++; //priradim novy index do zasobniku

            //pokud znam typ, ulozim hodnotu do tabulky konstant
            if (itemIn.type != ST_DEFAULT)
            {
                newItem->data.loaded = 0; //konstanta jeste nebyla nactena do zasobniku hodnot
                newItem->data.conTableIndex = ConTableInsert(conTable, itemIn.type, itemIn.value);
                if (newItem->data.conTableIndex == UINT_MAX)
                    return NULL;
            }

            return newItem;
        }

        //aktualni koren ma jednoho syna -- leveho
        if (root->right == NULL)
        {
            if (BSTNodeCmp(table, newItem, root) > 0) //novy prvek je vetsi nez akt. koren, vlozim ho na pravou nohu
            {
                root->right = newItem;
                newItem->parent = root;
                newItem->data.stackIndex = table->stackIndexCounter++; //priradim novy index do zasobniku

                //pokud znam typ, ulozim hodnotu do tabulky konstant
                if (itemIn.type != ST_DEFAULT)
                {
                    newItem->data.loaded = 0; //konstanta jeste nebyla nactena do zasobniku hodnot
                    newItem->data.conTableIndex = ConTableInsert(conTable, itemIn.type, itemIn.value);
                    if (newItem->data.conTableIndex == UINT_MAX)
                        return NULL;
                }

                return newItem;
            }
            else //chci pokracovat v leve vetvi, protoze novy prvek je mensi nebo roven akt. korenu
            {
                root = root->left;
                continue;
            }
        }

        //aktualni koren ma jednoho syna -- praveho
        if (root->left == NULL)
        {
            if (BSTNodeCmp(table, newItem, root) > 0) //novy prvek je vetsi nez akt. koren, pokracuji do prave nohy
            {
                root = root->right;
                continue;
            }
            else //novy prvek je mensi nez akt. koren, vkladam do leve nohy
            {
                root->left = newItem;
                newItem->parent = root;
                newItem->data.stackIndex = table->stackIndexCounter++;

                //pokud znam typ, ulozim hodnotu do tabulky konstant
                if (itemIn.type != ST_DEFAULT)
                {
                    newItem->data.loaded = 0; //konstanta jeste nebyla nactena do zasobniku hodnot
                    newItem->data.conTableIndex = ConTableInsert(conTable, itemIn.type, itemIn.value);
                    if (newItem->data.conTableIndex == UINT_MAX)
                        return NULL;
                }

                return newItem;
            }
        }

        //aktualni koren ma dva syny -- musim vybrat, kterym budu pokracovat
        if (BSTNodeCmp(table, newItem, root) > 0)
            root = root->right;
        else
            root = root->left;
    }

    return NULL; //nikdy nenastane
}

/**
* Vyhledava prvek v tabulce symbolu.
*
* Vyhledava na zaklade predaneho klice.
*
* @param root Ukazatel na tabulku, ve ktere se bude vyhledavat. (symTable_t*)
* @param dataIn Klic, dle ktereho se vyhledava. (const int)
* @return Ukazatel na prvek, ktery vyhovuje klici. Pokud takovy prvek neexistuje, vraci NULL.
*/
btNode_t* SymTableSearch(symTable_t* table, const char* name)
{
    if (table == NULL || name == NULL || table->chainIndex == 0)
        return NULL;

    btNode_t* root = table->root;
    while (root != NULL) //dokud neprojdu celym stromem
    {
        if (BSTNodeStrCmp(table, root, name) == 0) //nasel jsem pozadovany prvek
            return root;

        //aktualni koren nema zadne syny -- nenasel jsem pozadovany prvek
        if (root->left == NULL && root->right == NULL)
            return NULL;

        //aktualni koren ma jednoho syna -- leveho -> jdu do nej
        if (root->right == NULL)
        {
            root = root->left;
            continue;
        }

        //aktualni koren ma jednoho syna -- praveho -> jdu do nej
        if (root->left == NULL)
        {
            root = root->right;
            continue;
        }

        //aktualni koren ma dva syny -- musim vybrat, kterym budu pokracovat
        if (BSTNodeStrCmp(table, root, name) > 0)
            root = root->left;
        else
            root = root->right;
    }

    return NULL; //nenalezl jsem prvek
}


/**
* Porovnavaci funkce pro binarni vyhledavaci strom.
*
* Provadi porovnani na zaklade datoveho obsahu uzlu a retezce.
*
* @param first Prvni prvek k porovnani (btNode_t*).
* @param second Retezec k porovnani (const char*).
* @return Vraci cislo <0, pokud je prvni prvek mensi, 0 pokud jsou shodne, >0 kdyz je prvni prvek vetsi. (int)
*/
static inline int BSTNodeStrCmp(const symTable_t* table, const btNode_t* first, const char* string)
{
    if (first == NULL)
        return -1;
    if (string == NULL)
        return 1;

    return (strcmp(table->nameChain + first->data.nameIndex, string));
}

/**
* Porovnavaci funkce pro binarni vyhledavaci strom.
*
* Provadi porovnani na zaklade datoveho obsahu dvou uzlu.
*
* @param first Prvni prvek k porovnani (btNode_t*).
* @param second Druhy prvek k porovnani (btNode_t*).
* @return Vraci cislo <0, pokud je prvni prvek mensi, 0 pokud jsou shodne, >0 kdyz je prvni prvek vetsi. (int)
*/
static inline int BSTNodeCmp(const symTable_t* table, const btNode_t* first, const btNode_t* second)
{
    if (first == NULL)
        return -1;
    if (second == NULL)
        return 1;

    return (strcmp(table->nameChain + first->data.nameIndex, table->nameChain + second->data.nameIndex));
}

/**
* Rekurzivne znici tabulku symbolu a uvolni pamet.
*
* @param table Ukazatel na tabulku, kterou chci zrusit. (symTable_t*)
*/
void SymTableDestroy(symTable_t* table)
{
    if (table == NULL)
        return;

    free(table->nameChain);
    BSTDestroy(table->root);    //Rekurzivne uvolni cely binarni strom
    free(table);
}

/**
* Znici binarni strom a uvolni pamet.
*
* Rekurzivne uvolnuje pamet a zahazuje ukazatele na uvolnenou pamet (nastavi na NULL).
*
* @param root Ukazatel na koren stronu (bt_Node_t*)
*/
static void BSTDestroy(btNode_t* root)
{
    if (root == NULL)
        return;

    BSTDestroy(root->left);
    BSTDestroy(root->right);

    free(root);
    root = NULL;
}

/**
* Inicializuje tabulku symbolu.
*
* Inicializuje tabulku symbolu, vytvori koren binarniho stromu, alokuje misto pro
* retez nazvu a nastavi jeho velikost.
*
* @return Ukazatel na koren nove vytvorene tabulky symbolu, NULL pri neuspechu. (symTable_t*)
*/
symTable_t* SymTableInit(void)
{
    symTable_t* newTable = malloc(sizeof(*newTable));
    if (newTable == NULL)
        return NULL;

    btNode_t* root = malloc(sizeof(*root));
    if (root == NULL)
    {
        free(newTable);
        return NULL;
    }

    newTable->nameChain = malloc(SYM_TABLE_CHAIN_SIZE * sizeof(char));
    if (newTable->nameChain == NULL)
    {
        free(root);
        free(newTable);
        return NULL;
    }

    root->parent = NULL;
    root->left = NULL;
    root->right = NULL;
    root->data.tempName = NULL; //na zadost GB

    newTable->chainLengthLimit = SYM_TABLE_CHAIN_SIZE;
    newTable->root = root;
    newTable->chainIndex = 0;

    newTable->stackIndexCounter = VSTACK_PARAM; //pozice, odkud zacinaji zadavane hodnoty zasobniku

    return newTable;
}

/**
* Vklada konstantu do tabulky konstant, alokuje pro ni pamet, pokud je treba.
*
* @param conTable Ukazatel na tabulku retezcu. (char*)
* @param stringIn Ukazatel na retezec, ktery chci zkopirovat. (char*)
* @return Index do tabulky retezcu, na ktery jsem dany retezec ukladal, UINT_MAX pri chybe (unsigned int)
*/
static unsigned int ConTableInsert(conTable_t* conTable, ST_TYPE itemInType, union stValue itemInValue)
{
    if (conTable->actIndex == UINT_MAX) //plna tabulka
        return UINT_MAX;
    if (conTable->actIndex >= conTable->maxSize) //treba realokovat
    {
        struct conTableItem* temp = realloc(conTable->items, conTable->maxSize + CON_TABLE_SIZE);
        if (temp == NULL)
            return UINT_MAX;

        conTable->items = temp;
        conTable->maxSize += CON_TABLE_SIZE;

        for (unsigned int i = conTable->maxSize - CON_TABLE_SIZE ; i < conTable->maxSize; ++i)
            conTable->items[i].type = ST_DEFAULT;
    }

    conTable->items[conTable->actIndex].type = itemInType;  //priradim typ

    //konstanta je retezec
    if (itemInType == ST_STRING)
    {
        conTable->items[conTable->actIndex].value.string = \
                     malloc(sizeof(*conTable->items[conTable->actIndex].value.string) * (strlen(itemInValue.string) + 1));
        if (conTable->items[conTable->actIndex].value.string == NULL)
            return UINT_MAX;

        strcpy(conTable->items[conTable->actIndex].value.string, itemInValue.string);
    }
    //konstanta je cislo, logicka hodnota nebo pole
    else if (itemInType == ST_NUMBER || itemInType == ST_LOGIC || itemInType == ST_ARRAY)
    {
        conTable->items[conTable->actIndex].value.number = itemInValue.number;
    }
    //konstanta je funkce
    else if (itemInType == ST_FUNC)
    {
        conTable->items[conTable->actIndex].value.instAddr = itemInValue.instAddr;
    }
    else //ST_NIL || ST_DEFAULT => nepotrebuji prirazovat hodnotu
    {
        ;
    }

    return conTable->actIndex++;
}

/**
* Vytvori tabulku konstant.
*
* Vytvori a inicializuje tabulku konstant na hromade.
* @return Ukazatel na nove vytvorenou tabulku konstant, NULL pri chybe.
*/
conTable_t* ConTableInit(void)
{
    conTable_t* newConTable = malloc(sizeof(*newConTable));
    if (newConTable == NULL)
        return NULL;

    newConTable->items = malloc(sizeof(*newConTable->items) * CON_TABLE_SIZE);
    if (newConTable->items == NULL)
    {
        free(newConTable);
        return NULL;
    }

    newConTable->actIndex = 0;
    newConTable->maxSize = CON_TABLE_SIZE;

    for (unsigned int i = 0; i < CON_TABLE_SIZE; ++i)
        newConTable->items[i].type = ST_DEFAULT;

    return newConTable;
}

/**
* Zlikviduje tabulku konstant.
*
* Zlikviduje a uvolni pamet, kterou zabirala tabulka konstant.
*
* @param Ukazatel na tabulku konstant, kterou chci znicit. (conTable_t*)
*/
void ConTableDestroy(conTable_t* table)
{
    if (table == NULL)
        return;

    if (table->actIndex == 0)
    {
        free(table->items);
        free(table);
        return;
    }

    //--table->actIndex; //actIndex ukazuje na prvni VOLNY prvek
    do
    {
        --table->actIndex;

        if (table->items[table->actIndex].type == ST_STRING)
            free(table->items[table->actIndex].value.string);



    } while (table->actIndex != 0);

    free(table->items);
    free(table);
}

/**
* Seradi dane pole znaku metodou Merge Sort.
*
* Radi od nejmensiho po nejvetsi, dle lexikograficke vahy.
* Rekurzivni algoritmus, casova slozitost O(n * log n).
*
* @param data Pole znaku, ktere chci seradit (char*).
* @param begin Index prvku, od ktereho chci radit, vcetne (unsigned int).
* @param end Index prvku, do ktereho chci radit, vcetne (unsigned int).
* @return Chybovy stav (0 = OK, -1 = chyba) (int).
*/
int MergeSort(char *data, unsigned int begin, unsigned int end)
{
    if (data == NULL)
        return -1;
    if (data[0] == '\0')
        return 0;

    if (begin < end) //tabulka obsahuje vice nez jeden prvek
    {
        int newMiddle = (begin + end) / 2;

        MergeSort(data, begin,  newMiddle);
        MergeSort(data, newMiddle + 1, end);
        Merge(data, begin, newMiddle, end);
    }

    return 0;
}

/**
* Spojuje dve serazene tabulky do jedne setridene.
*
* Tabulky musi byt serazene (zde lexikograficky), je potreba
* pomocne pole pro serazene hodnoty.
*
* @param data Pole znaku, nad kterym provadim spojovani (char*).
* @param begin Index prvku, kterym zacina razena cast tabulky (unsigned int).
* @param middle Index prvku, ktery rozdeluje razenou tabulku na dve (unsigned int).
* @param end Index prvku, kterym konci razena cast tabulky (unsigned int).
*/
void Merge(char* data, unsigned int begin, unsigned int middle, unsigned int end)
{
    char temp[end - begin + 1]; //pomocna tabulka pro uchovani serazenych hodnot
    unsigned int tempIndex = 0;

    unsigned int       begin1 = begin; //pocatek a konec prvni tabulky
    const unsigned int end1   = middle;
    unsigned int       begin2 = middle + 1; //pocatek a konec druhe tabulky
    const unsigned int end2   = end;

    for ( ; begin1 <= end1 && begin2 <= end2; ++tempIndex)
    {
        // Porovnam prvky na indexech a vlozim do pom. tabulky mensi hodnotu,
        // prislusny index pak posouvam
        if (data[begin1] < data[begin2])
            temp[tempIndex] = data[begin1++];
        else
            temp[tempIndex] = data[begin2++];
    }

    while (begin1 <= end1) //pokud byla delsi prvni tabulka, pripojim ji za pomocnou
        temp[tempIndex++] = data[begin1++];

    while (begin2 <= end2) //delsi byla druha tabulka, pripojuji ji
        temp[tempIndex++] = data[begin2++];

    (void) memcpy( (void*) (data + begin), (void*) temp, sizeof(temp)); //kopiruji obsah pomocne tabulky do skutecne
}

/**
* Vyhledavani podretezce pomoci Knuth-Moris-Prattova algoritmu.
*
* Vraci index prvniho znaku prvniho vyskytu daneho podretezce.
*
* @warning Doplnit symb. konstanty, zvazit optimalizaci - pokud je hledany podretezec delsi, nez zbytek retezce k prohledani, rovnou vyhodit chybu, odstranit DEBUG vypis.
* @param string Retezec, ve kterem budu vyhledavat (const char*).
* @param subString Podretezec, ktery vyhledavam (const char*).
* @return Index retezce, na kterem zacina dany podretezec, nebo -1 pri nenalezeni (int)
*/
int KMPSearch(const char* string, const char* subString)
{
    if (string == NULL || subString == NULL)
        return -1;

    if (subString[0] == '\0') //prazdny podretezec
        return 0;
    size_t stringLen = strlen(string);
    size_t subStringLen = strlen(subString);
    bool patternBreak = false;
    unsigned int subIndex = 0;

    for (unsigned int i = 0; i < stringLen ; ++i)
    {
        if (string[i] == subString[0] && subStringLen == 1)
            return i;
        else if (patternBreak == false && string[i] == subString[subIndex])
        {
            if (subIndex == (subStringLen - 1))  //nalezena kompletni shoda
            {
                #ifdef DEBUG_IAL
                    printf("\nMatch found, end index: %u\n", i);
                #endif
                return i - (subStringLen - 1);
            }
            else
            {
                ++subIndex;
                continue;
            }
        }

        patternBreak = true;

        if (string[i] == subString[0])
        {
            patternBreak = false;
            subIndex = 1;
        }
    }

    return -1;

}

#ifdef DEBUG_IAL

static void PrintSymTable(symTable_t* table);
static void PrintBT(symTable_t* table, btNode_t* root);

//wrapper na tisk tabulky
static void PrintSymTable(symTable_t* table)
{
    if (table->root != NULL)
        PrintBT(table, table->root);
}

//rekurzivne vytiskne vsechny prvky daneho stromu, od korene, zleva doprava
static void PrintBT(symTable_t* table, btNode_t* root)
{
    if (root == NULL)
        return;

    printf("\nNode with name: \'%s\'\n", table->nameChain + root->data.nameIndex);
    if (root->left == NULL)
        printf("\t--left son:  NULL\n");
    else
        printf("\t--left son:  \'%s\'\n", table->nameChain + root->left->data.nameIndex);

    if (root->right == NULL)
        printf("\t--right son: NULL\n");
    else
        printf("\t--right son: \'%s\'\n", table->nameChain + root->right->data.nameIndex);

    PrintBT(table, root->left);
    PrintBT(table, root->right);
}
static void PrintBT2(symTable_t* table, btNode_t* root);
//wrapper na tisk tabulky
static void PrintST(symTable_t* table)
{
  printf("\n  -----   SYMBOL TABLE   -----\n\n");

    if (table->root != NULL)
        PrintBT2(table, table->root);

  printf("\n  ------------------------------\n\n");
}

char *varType[] = {"ST_DEFAULT","ST_STRING","ST_NIL","ST_LOGIC","ST_NUMBER","UNKNOW"};

//rekurzivne vytiskne vsechny prvky daneho stromu, od korene, zleva doprava
static void PrintBT2(symTable_t* table, btNode_t* root)
{
    if (root == NULL)
        return;
    int type;
    printf("NAME:           %s\n", table->nameChain + root->data.nameIndex);
    type=root->data.type;
    if (type < 0 || type > 4)
      type = 5;
    printf("Type:           %s\n",varType[type]);
    if (type == ST_DEFAULT)
      printf("stackIndex:     %u",root->data.stackIndex);
    else
      printf("conTableIndex:  %u",root->data.conTableIndex);
    putchar('\n');putchar('\n');
    PrintBT2(table, root->left);
    PrintBT2(table, root->right);
}

static void PrintCT(conTable_t *table)
{
  printf("\n  -----   CONSTANT TABLE   -----\n\n");

  for (unsigned int i=0;i<table->actIndex;++i) {
    printf("conTableIndex:    %u\n",i);
    int type=table->items[i].type;
    if (type < 0 || type > 4)
      type = 5;
    printf("Type:             %s\n",varType[type]);
    if (type == ST_STRING)
      printf("Value:            %s\n",table->items[i].value.string);
    else if (type == ST_NUMBER)
      printf("Value:            %lf\n",table->items[i].value.number);
    else if (type == ST_NIL)
      printf("Value:            NIL        .... in conTable not defined\n");
    else if (type == ST_LOGIC) {
      if (table->items[i].value.number > 0)
	printf("Value:            TRUE  (%lf)\n",table->items[i].value.number);
      else
	printf("Value:            FALSE (%lf)\n",table->items[i].value.number);
    }
    putchar('\n');
  }
  printf("\n  --------------------------------\n\n");
}

//main pro testovani
int main(void)
{
    //test MergeSortu
    char testArray[] = "qwertzuiopasdfghjklyxcvbnm";
    printf("Unsorted array: \'%s\'\n", testArray);
    MergeSort(testArray, 0, 25);
    printf("Sorted array:   \'%s\'\n", testArray);

    //test KMPSearch
    char testArray2[] = "PajaIvetaZuzkaLucie";
    //char testArray3[] = "Zuzka";
    char testArray3[] = "Iveta";
    int retVal = KMPSearch(testArray2, testArray3);
    printf("KMPSearch return value: %d\n", retVal);

    retVal = KMPSearch(testArray2, "");
    printf("KMPSearch return value: %d\n", retVal);

    //test Binarniho stromu
    printf("\nBinary tree testing -- inserting.\n");
    printf("----------------------------------------------------------------------------------------------------------------\n");
    //btNode_t* myTable = BSTInit();
    symTable_t* myTable = SymTableInit();
    conTable_t* myConTable = ConTableInit();
    stData_t temp;
    temp.type = ST_STRING;
    temp.value.string = "Sheeni";
    temp.tempName= "a";

    printf("\nInserting item \'a\', no tree is existing\n");
    (void) SymTableInsert(myTable, myConTable, temp);
    PrintSymTable(myTable);

    printf("\nInserting item \'b\' ------------------\n");
    temp.tempName = "b";
    btNode_t* value2 = SymTableInsert(myTable, myConTable, temp);
    PrintSymTable(myTable);

    printf("\nInserting item \'f\' ------------------\n");
    temp.tempName = "f";
    btNode_t* value6 = SymTableInsert(myTable, myConTable, temp);
    PrintSymTable(myTable);

    printf("\nInserting item \'d\' ------------------\n");
    temp.tempName = "d";
    (void) SymTableInsert(myTable, myConTable, temp);
    PrintSymTable(myTable);

    printf("\nInserting item \'c\' -------------------\n");
    temp.tempName = "c";
    (void) SymTableInsert(myTable, myConTable, temp);
    PrintSymTable(myTable);

    printf("\nInserting item \'b\' ------------------\n");
    temp.tempName = "b";
    (void) SymTableInsert(myTable, myConTable, temp);
    //PrintSymTable(myTable);

    printf("\nInserting item \'e\' ------------------\n");
    temp.tempName = "e";
    (void) SymTableInsert(myTable, myConTable, temp);
    PrintSymTable(myTable);

    printf("\nInserting item \'g\' ------------------\n");
    temp.tempName = "g";
    (void) SymTableInsert(myTable, myConTable, temp);
    PrintSymTable(myTable);

    printf("\nBinary tree testing -- searching.\n");
    printf("----------------------------------------------------------------------------------------------------------------\n");
    btNode_t* temp2 = NULL;

    printf("Searching item \'a\' \n");
    temp2 = SymTableSearch(myTable, "a");
    printf("Difference between addresses (should be 0): %ld\n", myTable->root - temp2 );

    printf("Searching item \'b\' \n");
    temp2 = SymTableSearch(myTable, "b");
    printf("Difference between addresses (should be 0): %ld\n", temp2 - value2 );

    printf("Searching item \'f\' \n");
    temp2 = SymTableSearch(myTable, "f");
    printf("Difference between addresses (should be 0): %ld\n", temp2 - value6 );

    printf("Searching item \'PD\'\n");
    temp2 = SymTableSearch(myTable, "PD");
    printf("Nonexistent item (should be NULL): %p\n", (void*)temp2);

    SymTableDestroy(myTable);
    ConTableDestroy(myConTable);

  // NAPLNENA TABULKA SYMBOLU ---------------------------------------
  symTable_t *table,*tableF;
  table=SymTableInit();
  tableF=SymTableInit();
  stData_t data;

  conTable_t* conTable = ConTableInit();

  data.tempName="num1";
  data.type=ST_NUMBER;
  data.value.number=12;
  SymTableInsert(table,conTable,data);

  data.tempName="num1";
  data.type=ST_NUMBER;
  data.value.number=33;
  SymTableInsert(table,conTable,data);

  data.tempName="num2";
  data.type=ST_NUMBER;
  data.value.number=17;
  SymTableInsert(table,conTable,data);

  data.value.number=29;
  data.tempName="num3";
  data.type=ST_NUMBER;
  SymTableInsert(table,conTable,data);

  data.tempName="str4";
  data.type=ST_STRING;
  data.value.string="Sheeni";
  SymTableInsert(table,conTable,data);

  data.tempName="str5";
  data.type=ST_STRING;
  data.value.string="Frank";
  SymTableInsert(table,conTable,data);
  data.value.string="Frank Sinatra";
  SymTableInsert(table,conTable,data);

  data.tempName="nil6";
  data.type=ST_NIL;
  SymTableInsert(table,conTable,data);

  data.tempName="log7";
  data.value.number=-1.0;
  data.type=ST_LOGIC;
  SymTableInsert(table,conTable,data);

  data.tempName="log8";
  data.value.number=1.0;
  data.type=ST_LOGIC;
  SymTableInsert(table,conTable,data);

  data.tempName="func";
  data.type=ST_DEFAULT;
  SymTableInsert(tableF,conTable,data);

  PrintST(table);
  PrintCT(conTable);

  SymTableDestroy(table);
  SymTableDestroy(tableF);
  ConTableDestroy(conTable);

    return 0;
}
#endif
