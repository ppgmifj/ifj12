/*
*********************************************************************
* File:         inst_list.h
* Date:         15. 10. 2012
* Author:       Petr Jirout
* Project:      Interpret IFJ12
* Team:         Petr Jirout,        xjirou07
*               Petr Huf,           xfugpe00
*               Matej Vanatko,      xvanat01
*               Gabriel Brandersky  xbrand04
*********************************************************************
*/

#ifndef INST_LIST_H_INCLUDED
#define INST_LIST_H_INCLUDED


#define INST_ARRAY_LENGTH 512 ///< Inicializacni velikost pole instrukci

/**
* Vycet vsech instrukci.
*/
typedef enum
{
    I_ADD = 0,      ///< Scitani             +
    I_SUB,          ///< Odcitani            -
    I_MUL,          ///< Nasobeni            *
    I_DIV,          ///< Deleni              /
    I_POW,          ///< Mocnina             **
    I_ISEQ,         ///< Rovnost             ==
    I_ISNOTEQ,      ///< Nerovnost           !=
    I_LESS,         ///< Mensi               <
    I_GREATER,      ///< Vetsi               >
    I_LESSOREQ,     ///< Mensi nebo rovno    <=
    I_GREATEROREQ,  ///< Vetsi nebo rovno    >=
    I_MOV,          ///< Presun src1 do dest
    I_LOAD,         ///< Src1 je index do CT a dest je index do VT - nacte konstantu
                    ///< do tabulky hodnot
    I_CALL,         ///< Volanie vestavenej funkcie, dest je hodnota z vyctu
                    ///< BUILT_IN_FUNC
    I_JMPIFFALSE,   ///< Skoc ak je src1 nepravidivy, na label dest
    I_PARAM,        ///< Parametry funkcie, src1 je index do zasobniku hodnot
    I_ENDPARAM,     ///< Konec zadavani parametru funkce pri uzivatelsky def.
    I_ENDCALL,      ///< Znacka pre ukoncenie parametrov a ulozenie navratovej
                    ///< hodnoty do dest
    I_LABEL,        ///< Oznacenie useku kodu, na ktory sa skace
    I_FUNCBEG,      ///< Oznacenie zaciatku definicie funkcie
    I_FUNCEND,      ///< Oznacenie konca definicie funkcie
    I_JMP,          ///< Nepodmieneny skok na adresu v dest
    I_CREATESTACK,  ///< Vytvoreni zasobniku
    I_RETURN,       ///< Navrat z funkce a ulozeni navratove hodnoty
    I_SUBSTRING,    ///< Vyber podretezca, v src1, src2 hodnoty indexov
    I_SUBSTRINGRES, ///< Src1 = vyberany podretezec, dest= vysledny retazec
    I_PARAMCOUNT,   ///< Pocet parametrov pre uzivatelske funkcie
    I_NOP,          ///< Prazdna operace, vznika pri optimalizaci
    I_COPY,         ///< Kopirovani
    I_COPY_INC,     ///< Kopirovani s naslednou inkrementaci src1 (pro FORDO)
    I_COPY_ARR,     ///< Kopirovanie hodnoty pola do pomocnej premenej
    I_MODIFY_ARR,   ///< Uprava prvku pole

} INSTR;

/**
* Struktura popisujici instrukci pracujici s triadresnym kodem
*/
typedef struct inst
{
    INSTR instType;      ///< Typ instrukce
    unsigned int src1;   ///< Adresa (index) prvniho operandu na zasobniku
    unsigned int src2;   ///< Adresa (index) druheho operandu na zasobniku
    unsigned int dest;   ///< Adresa (index) cile na zasobniku
} inst_t;


typedef struct instList
{
    inst_t* instArray;
    unsigned int actIndex;
    unsigned int maxSize;
} instList_t;


instList_t* instListGlobal;    ///< ukazatel na globalni pasku instrukci

instList_t* InstListInit(void);
int InstAdd(instList_t* L, inst_t I);
void InstListDestroy(instList_t* L);


#endif //INST_LIST_H_INCLUDED
