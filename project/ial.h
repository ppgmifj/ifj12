/*
*********************************************************************
* Date:         14. 10. 2012
* Author:       Petr Jirout
* Project:      Interpret IFJ12
* Team:         Petr Jirout,        xjirou07
*               Petr Huf,           xfugpe00
*               Matej Vanatko,      xvanat01
*               Gabriel Brandersky  xbrand04
* Description:
*  Implementace Merge Sortu a Knuth-Moris-Prattova algoritmu pro hledani
*  podretezce.
*/

#ifndef IAL_H_INCLUDED
#define IAL_H_INCLUDED

#include "inst_list.h"

#define SYM_TABLE_CHAIN_SIZE 256  ///< Inicializacni velikost retezu nazvu pro tabulku symbolu, budeme alokovat na jeho nasobky

#define CON_TABLE_SIZE 128 ///< Inicializacni velikost tabulky konstant


/**
* Vycet pro mozne varianty hodnoty v tabulce symbolu.
*/
typedef enum
{
    ST_DEFAULT,     ///< Inicializacni hodnota, nema semanticky vyznam
    ST_STRING,      ///< Retezec
    ST_NIL,         ///< Nil
    ST_LOGIC,       ///< Logicka hodnota (true/false)
    ST_NUMBER,      ///< Cislo
    ST_FUNC,        ///< Funkce
    ST_ARRAY,       ///< Retezec (rozsireni)
} ST_TYPE;

/**
* Unie slouzici pro uchovani hodnot v datove slozce tabulky symbolu.
*/
union stValue
{
    char* string;           ///< Docasny ukazatel na retezec, odkud budu kopirovat
    double number;          ///< Uchovani cisla ci logicke hodnoty
    unsigned int instAddr;   ///< Polozka zasobniku hodnot, kam se bude skakat
};

/**
* Struktura popisuji datovou slozku tabulky symbolu.
*/
typedef struct stData
{
    char* tempName;             ///< Docasny ukazatel pro nazev
    unsigned int nameIndex;     ///< Index do retezu nazvu, na nemz zacina nazev identifikatoru
    ST_TYPE type;               ///< Typ ulozene hodnoty
    union stValue value;        ///< Uchovani hodnoty
    unsigned int conTableIndex; ///< Index do tabulky konstant
    unsigned int stackIndex;    ///< Index do tabulky hodnot na zasobniku dane funkce
    int loaded;                 ///< Status, zda jiz byla promenna nahrana do zasobniku
} stData_t;

/**
* Struktura popisujici uzel binarniho stromu
*/
typedef struct btNode
{
    stData_t data;          ///< Datova slozka
    struct btNode* parent;  ///< Ukazatel na otce
    struct btNode* left;    ///< Ukazatel na leveho syna
    struct btNode* right;   ///< Ukazatel na praveho syna
} btNode_t;


/**
* Struktura popisujici prvek tabulky konstant.
*/
struct conTableItem
{
    union stValue value;    ///< Uchovani hodnoty (char* || double)
    ST_TYPE type;           ///< Rozliseni typu (ST_STRING || ST_DOUBLE)
};

/**
* Struktura popisujici tabulku konstant, implementovanou pomoci pole.
*/
typedef struct conTable
{
    struct conTableItem* items;   ///< Dynamicky alokovane pole hodnot (double || char*)
    unsigned int actIndex;        ///< Aktualni (aktivni) index pole
    unsigned int maxSize;         ///< Velikost pameti alokovane pro pole
} conTable_t;

/**
* Struktura popisujici tabulku symbolu.
*/
typedef struct symTable
{
    btNode_t* root;                 ///< Ukazatel na koren binarniho stromu
    char* nameChain;                ///< Ukazatel na retez identifikatoru
    unsigned int chainLengthLimit;  ///< Delka na kterou je tento retezec alokovany
    unsigned int chainIndex;        ///< Aktualne vyuzita delka
    unsigned int stackIndexCounter; ///< Pocet prirazenych promennych do pole
} symTable_t;


btNode_t* SymTableInsert(symTable_t* table, conTable_t* conTable, stData_t itemIn);
btNode_t* SymTableSearch(symTable_t* table, const char* name);
void SymTableDestroy(symTable_t* table);
void ConTableDestroy(conTable_t* table);
symTable_t* SymTableInit(void);
conTable_t* ConTableInit(void);
int MergeSort(char *data, unsigned int begin, unsigned int end);
void Merge(char* data, unsigned int begin, unsigned int middle, unsigned int end);
int KMPSearch(const char* string, const char* subString);


#endif // IAL_H_INCLUDED
