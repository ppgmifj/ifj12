/**
* @file interpreter.c
* @brief Operace pro interpretaci.

* Implementace operaci potrebnych pro interpretaci a samotny interpret.
*/
/*
*********************************************************************
* File:         interpreter.c
* Date:         12. 11. 2012
* Author:       Petr Jirout
* Project:      Interpret IFJ12
* Team:         Petr Jirout,        xjirou07
*               Petr Huf,           xhufpe00
*               Matej Vanatko,      xvanat01
*               Gabriel Brandersky  xbrand04
*********************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <limits.h>

#include "interpreter.h"
#include "inst_list.h"
#include "builtin.h"
#include "global_types.h"


#ifdef DEBUG_INTERPRETER
//pomocna debug funkce
void PrintVS(valueStack_t* stack);
#endif

//znici a uklidi jednu tabulku hodnot
static void VSDestroy(valueStack_t* stack);
//vykona instrukci
static int ExecuteInstruction(conTable_t* conTable, valueStack_t* stack, inst_t inst);

#define InstType(inst) (instList->instArray[inst].instType)
#define AccessInst(inst) (instList->instArray[inst])
#define AccessSAItem(index,src) (VSArray[(index)]->items[instList->instArray[actInst].src])
#define AccessSAItemDirectly(index,src) (VSArray[index]->items[src])

#ifdef DEBUG_INTERPRETER
#define ErrCheck(func) do {errCode = (func);\
                          if (errCode != E_OK) {PrintVS(VSArray[VSArrayIndex]); VSArrayDestroy(VSArray,VSArrayIndex);return errCode;}}while(0)
#endif

#ifndef DEBUG_INTERPRETER
#define ErrCheck(func) do {errCode = (func);\
                          if (errCode != E_OK) {VSArrayDestroy(VSArray,VSArrayIndex);return errCode;}}while(0)
#endif
/**
* Provede interpretaci instrukcni pasky.
*
* Interpretuje instrukce, nacita hodnoty z tabulky konstant, pracuje nad zasobniky hodnot.
*
* @param conTable Ukazatel na tabulku konstant. (conTable_t*)
* @param instList Ukazatel na instrukcni pasku (const instList_t*)
* @return Chybovy kod E_OK, E_INCOMP_TYPES (nekompatibilni typy), E_INTERN (interni chyba), E_DIV_ZERO (deleni nulou)
*/
int Interpreter(conTable_t* conTable, const instList_t* instList)
{
    if (conTable == NULL || instList == NULL)
        return E_INTER;

    //prazdny instrukcni list
    if (instList->actIndex == 0)
        return E_OK;

    //vytvarim pole zasobniku, nadrazeny zasobnik ma vzdy index o jedna vetsi nez aktualni
    valueStack_t** VSArray = malloc(sizeof(*VSArray) * STACK_ARRAY_SIZE);
    if (VSArray == NULL)
        return E_INTER;

    VSArray[0] = VStackInit(); //inicializuji hlavni zasobnik
    if (VSArray[0] == NULL)
    {
        free(VSArray);
        return E_INTER;
    }
    unsigned int VSArrayIndex = 0;       //index aktualniho zasobniku
    unsigned int VSArrayAllocMul = 1;    //multiplikacni cislo pro realokaci pole zasobniku

    unsigned int paramLoaded = 0;        //pomocna promenna pro pocet nactenych parametru
    unsigned int paramCount = 0;         //pomocna promenna pro pocet parametru funkce (uzivatelem definovane)
    valueStackItem_t* dest = NULL;       //pomocna promenna pro vestavene funkce - adresa, kam chceme ulozit vysledek
    valueStackItem_t* firstParam = NULL; //adresa prvniho parametru, pro funkci Find
    unsigned int strLength = 0;          //pomocna promenna pro delku retezce v I_SUBSTRING
    inst_t temp;                         //pomocna promenna pro volani ExecuteInstruction

    int errCode = E_OK;                  //pomocna promenna pro navratove kody


    //zaciname od zacatku
    unsigned int actInst = 0;
    unsigned int maxInstIndex = instList->actIndex;

    while (actInst < maxInstIndex) //projdu cely instrukcni list
    {
    //tady realokovat nemusim, nebot zdejsi instrukce pracuji s jiz vytvorenymi prvky(indexy)

    //vykonavam prislusnou instrukci
    switch(InstType(actInst))
    {
        //---------------------------------------I_LABEL-----------------------------------------
        //labely ignorujeme
        case I_LABEL:
            break;

        //---------------------------------------I_CREATESTACK----------------------------------------
        //vytvarim novy zasobnik
        //maximalni pocet parametru = (VAL_STACK_SIZE - 2) (nyní 254)
        case I_CREATESTACK:

            //zjistim, zda mam v poli zasobniku jeste misto, pripadne realokuji
            if (VSArrayIndex + 1 >= STACK_ARRAY_SIZE * VSArrayAllocMul)
            {
                valueStack_t** tempArr = realloc(VSArray, sizeof(*tempArr) * ++VSArrayAllocMul * STACK_ARRAY_SIZE);
                if (tempArr == NULL)
                {
                    #ifdef DEBUG_INTERPRETER
                    PrintVS(VSArray[VSArrayIndex]);
                    #endif
                    VSArrayDestroy(VSArray, VSArrayIndex);
                    return E_INTER;
                }
                VSArray = tempArr;
            }

            //stary zasobnik se stane nadrazenym, vytvorim novy
            VSArray[++VSArrayIndex] = VStackInit();
            if (VSArray[VSArrayIndex] == NULL)
            {
                #ifdef DEBUG_INTERPRETER
                PrintVS(VSArray[VSArrayIndex]);
                #endif
                VSArrayDestroy(VSArray, VSArrayIndex-1);
                return E_INTER;
            }

            paramLoaded = actInst; //uschovam si aktualni instrukci

            //ulozim dalsi vykonnou (po I_JMP) instrukci na zasobnik, abych vedel kde po skonceni funkce pokracovat
            while (++actInst < maxInstIndex && InstType(actInst) != I_JMP)
            {
                ;
            }
            AccessSAItemDirectly(VSArrayIndex,VSTACK_INST_PTR).value.instAddr = actInst;
            AccessSAItemDirectly(VSArrayIndex,VSTACK_INST_PTR).type = ST_FUNC;

            //obnovim puvodni instrukci a pokracuji ve vytvareni zasobniku
            actInst = paramLoaded;
            paramLoaded = 0;

            //mezi I_CREATESTACK a I_PARAMCOUNT mohou byt I_LOAD --> vykonam je
            while (++actInst < maxInstIndex && InstType(actInst) != I_PARAMCOUNT)
            {
                if (InstType(actInst) == I_LOAD) //lze optimalizovat, nemelo by tu byt nic jineho nez LOADy
                    ExecuteInstruction(conTable, VSArray[VSArrayIndex], AccessInst(actInst)); //I_LOAD
                else
                {
                    #ifdef DEBUG_INTERPRETER
                    PrintVS(VSArray[VSArrayIndex]);
                    #endif
                    VSArrayDestroy(VSArray, VSArrayIndex);
                    return E_INTER;
                }
            }
            //actInst == I_PARAMCOUT, nactu si pocet parametru
            paramCount = (unsigned int) trunc(AccessSAItem(VSArrayIndex, src1).value.number);

            //nactu parametry do zasobniku
            while (++actInst < maxInstIndex && InstType(actInst) != I_ENDPARAM && paramLoaded < paramCount)
            {
                if (InstType(actInst) == I_LOAD)
                    ErrCheck( ExecuteInstruction(conTable, VSArray[VSArrayIndex-1], AccessInst(actInst)) );
                else //InstType(actInst) == I_PARAM
                {
                    //nacitam retezec
                    if (AccessSAItem(VSArrayIndex-1, src1).type == ST_STRING)
                    {
                        AccessSAItemDirectly(VSArrayIndex, VSTACK_PARAM + paramLoaded).value.string =\
                                      malloc(sizeof(char) * (strlen(AccessSAItem(VSArrayIndex-1,src1).value.string) +1));
                        if (AccessSAItemDirectly(VSArrayIndex, VSTACK_PARAM + paramLoaded).value.string == NULL)
                        {
                            #ifdef DEBUG_INTERPRETER
                            PrintVS(VSArray[VSArrayIndex]);
                            #endif
                            VSArrayDestroy(VSArray, VSArrayIndex);
                            return E_INTER;
                        }

                        strcpy(AccessSAItemDirectly(VSArrayIndex, VSTACK_PARAM + paramLoaded).value.string\
                                ,AccessSAItem(VSArrayIndex-1,src1).value.string);
                        AccessSAItemDirectly(VSArrayIndex, VSTACK_PARAM + paramLoaded).type = ST_STRING;
                    }
                    //nacitam NIL
                    else if (AccessSAItem(VSArrayIndex-1,src1).type == ST_NIL)
                    {
                        AccessSAItemDirectly(VSArrayIndex, VSTACK_PARAM + paramLoaded).type = ST_NIL;
                    }
                    //nacitam pole, musim ho zkopirovat cele
                    else if (AccessSAItem(VSArrayIndex-1,src1).type == ST_ARRAY)
                    {
                        //ulozim si index, kde zacina pole (v nadrazenem zasobniku)
                        unsigned int arrIndex = AccessInst(actInst).src1;
                        //nactu pocet prvku pole
                        strLength = (unsigned int) trunc(AccessSAItem(VSArrayIndex-1,src1).value.number);

                        //nactu hodnotu do noveho zasobniku
                        AccessSAItemDirectly(VSArrayIndex, VSTACK_PARAM + paramLoaded).value.number =\
                                  AccessSAItem(VSArrayIndex-1,src1).value.number;
                        AccessSAItemDirectly(VSArrayIndex, VSTACK_PARAM + paramLoaded).type = ST_ARRAY;

                        ++paramLoaded;

                        //nactu prvky pole
                        for (unsigned int i = 0; i <= strLength; ++i, ++paramLoaded)
                        {
                            //kopiruji typ
                            AccessSAItemDirectly(VSArrayIndex, VSTACK_PARAM + paramLoaded).type =\
                                  AccessSAItemDirectly(VSArrayIndex-1, arrIndex+i+1).type;

                            //kopiruji hodnotu
                            if (AccessSAItemDirectly(VSArrayIndex-1,arrIndex+i+1).type == ST_STRING)
                            {
                                AccessSAItemDirectly(VSArrayIndex, VSTACK_PARAM + paramLoaded).value.string =\
                                malloc(sizeof(char) * (strlen(AccessSAItemDirectly(VSArrayIndex-1, arrIndex+i+1).value.string) +1));
                                if (AccessSAItemDirectly(VSArrayIndex, VSTACK_PARAM + paramLoaded).value.string == NULL)
                                {
                                    VSArrayDestroy(VSArray, VSArrayIndex);
                                    return E_INTER;
                                }

                                strcpy(AccessSAItemDirectly(VSArrayIndex,VSTACK_PARAM + paramLoaded).value.string,
                                       AccessSAItemDirectly(VSArrayIndex-1,arrIndex+i+1).value.string);
                            }

                            else if (AccessSAItemDirectly(VSArrayIndex-1,arrIndex+i+1).type == ST_NIL)
                                ;
                            else //ST_NUMBER, ST_LOGIC
                            {
                                AccessSAItemDirectly(VSArrayIndex, VSTACK_PARAM + paramLoaded).value.number =\
                                    AccessSAItemDirectly(VSArrayIndex-1,arrIndex+i+1).value.number;
                            }
                        }
                        //inc(paramloaded)
                    }
                    //nacitam LOGIC nebo NUMBER
                    else
                    {
                        AccessSAItemDirectly(VSArrayIndex, VSTACK_PARAM + paramLoaded).value.number =\
                                  AccessSAItem(VSArrayIndex-1,src1).value.number;
                        AccessSAItemDirectly(VSArrayIndex, VSTACK_PARAM + paramLoaded).type =\
                                  AccessSAItem(VSArrayIndex-1, src1).type;
                    }

                    ++paramLoaded;
                }
            }

            //doplnim ST_NILy, pokud jsou treba
            for ( ; paramLoaded < paramCount ; ++paramLoaded)
            {
                AccessSAItemDirectly(VSArrayIndex, VSTACK_PARAM + paramLoaded).type = ST_NIL;
            }
            break;

        //---------------------------------------I_FUNBEG----------------------------------------------
        //zacatek definice funkce
        case I_FUNCBEG:

            //posouvam se dokud nenarazim na konec definice funkce
            while ((++actInst < maxInstIndex) && (InstType(actInst) != I_FUNCEND) )
            {
                ;
            }

            break;

        //---------------------------------------I_FUNCEND---------------------------------------------
        //konec funkce, cistim zasobnik, nastavuji aktualni instrukci na hodnotu ulozenou na zasobniku
        case I_FUNCEND:

            actInst = AccessSAItemDirectly(VSArrayIndex,VSTACK_INST_PTR).value.instAddr;
            VSDestroy(VSArray[VSArrayIndex--]);

            break;

        //---------------------------------------I_SUBSTRING-------------------------------------------
        //vyber podretezce, pomoci paru instrukci
        case I_SUBSTRING:
            strLength = 0;

            paramLoaded = (unsigned int) trunc(AccessSAItem(VSArrayIndex,src1).value.number); //pocatecni index

            if (AccessInst(actInst).dest == END_INDEX_VALID)
                paramCount = ((unsigned int) trunc(AccessSAItem(VSArrayIndex,src2).value.number)); //koncovy index
            else
                paramCount = UINT_MAX;


            //provedu vsechny I_LOAD, ktere se nachazeji mezi parem instrukci
            while ((++actInst < maxInstIndex) && (InstType(actInst) != I_SUBSTRINGRES) )
            {
                ErrCheck( ExecuteInstruction(conTable, VSArray[VSArrayIndex], AccessInst(actInst)) );
            }

            //actInst je ted I_SUBSTRINGRES
            strLength = strlen(AccessSAItem(VSArrayIndex,src1).value.string);

            //paramCount nebyl zadan, ctu az do konce retezce
            if (paramCount == UINT_MAX)
                paramCount = strLength;

            //vracim prazdny retezec
            if (paramLoaded == paramCount || paramLoaded > paramCount || paramCount > strLength)
            {
                AccessSAItem(VSArrayIndex,dest).value.string = malloc(sizeof(char) * 1);
                if (AccessSAItem(VSArrayIndex,dest).value.string == NULL)
                {
                    #ifdef DEBUG_INTERPRETER
                    PrintVS(VSArray[VSArrayIndex]);
                    #endif
                    VSArrayDestroy(VSArray, VSArrayIndex);
                    return E_INTER;
                }

                //vkladam prazdny retezec
                AccessSAItem(VSArrayIndex,dest).value.string[0] = '\0';

                AccessSAItem(VSArrayIndex,dest).type = ST_STRING;

                break; //konec I_SUBSTRING
            }

            AccessSAItem(VSArrayIndex,dest).value.string = malloc(sizeof(char) * (paramCount+1));
            if (AccessSAItem(VSArrayIndex,dest).value.string == NULL)
            {
                #ifdef DEBUG_INTERPRETER
                PrintVS(VSArray[VSArrayIndex]);
                #endif
                VSArrayDestroy(VSArray, VSArrayIndex);
                return E_INTER;
            }


            strncpy(AccessSAItem(VSArrayIndex,dest).value.string\
                    ,AccessSAItem(VSArrayIndex,src1).value.string + paramLoaded, paramCount - paramLoaded);

            //ukoncovaci nula
            AccessSAItem(VSArrayIndex,dest).value.string[paramCount-paramLoaded] = '\0';

            AccessSAItem(VSArrayIndex,dest).type = ST_STRING;


            break;

        //---------------------------------------I_CALL-----------------------------------------------
        //volani vestavene funkce
        case I_CALL:

            //hodnoty z vyctu BUILT_IN_FUNC + I_LOAD pro nacteni konstanty

            if (AccessSAItem(VSArrayIndex,dest).type == ST_STRING) //budeme prepisovat retezec
                free(AccessSAItem(VSArrayIndex,dest).value.string);

            paramLoaded = 0;
            dest = &AccessSAItem(VSArrayIndex, dest); //adresa, kam chceme ulozit vysledek
            firstParam = NULL; //adresa prvniho parametru, pro funkci Find

            switch(AccessInst(actInst).src1)
            {
                case F_NUMERIC:
                    while (++actInst < maxInstIndex && InstType(actInst) != I_ENDCALL && paramLoaded < 1)
                    {
                        if (InstType(actInst) == I_LOAD)
                          ErrCheck( ExecuteInstruction(conTable, VSArray[VSArrayIndex], AccessInst(actInst)) );
                        else //InstType(actInst) == I_PARAM
                        {
                            ++paramLoaded;
                            ErrCheck( Numeric(&AccessSAItem(VSArrayIndex,src1), dest) );
                            break;
                        }
                    }

                    break;

                case F_INPUT:

                    ErrCheck( Input(dest) );

                    break;

                case F_TYPEOF:
                    while (++actInst < maxInstIndex && InstType(actInst) != I_ENDCALL && paramLoaded < 1)
                    {
                        if (InstType(actInst) == I_LOAD)
                            ErrCheck( ExecuteInstruction(conTable, VSArray[VSArrayIndex], AccessInst(actInst)) );
                        else //InstType(actInst) == I_PARAM
                        {
                            ++paramLoaded;
                            ErrCheck( TypeOf(&AccessSAItem(VSArrayIndex,src1), dest) );
                            break;
                        }
                    }

                    break;

                case F_LEN:
                    while (++actInst < maxInstIndex && InstType(actInst) != I_ENDCALL && paramLoaded < 1)
                    {
                        if (InstType(actInst) == I_LOAD)
                            ErrCheck( ExecuteInstruction(conTable, VSArray[VSArrayIndex], AccessInst(actInst)) );
                        else //InstType(actInst) == I_PARAM
                        {
                            ++paramLoaded;
                            Len(&AccessSAItem(VSArrayIndex,src1), dest);
                            break;
                        }
                    }

                    break;

                case F_FIND:
                    while (++actInst < maxInstIndex && InstType(actInst) != I_ENDCALL && paramLoaded < 2)
                    {
                        if (InstType(actInst) == I_LOAD)
                            ErrCheck( ExecuteInstruction(conTable, VSArray[VSArrayIndex], AccessInst(actInst)) );
                        else if (paramLoaded == 0) //InstType(actInst) == I_PARAM && nemam nacteny parametr
                        {
                           ++paramLoaded;
                           firstParam = &AccessSAItem(VSArrayIndex,src1);
                        }
                        else //InstType(actInst) == I_PARAM && mam nacteny jeden parametr
                        {
                            if (firstParam->type != ST_STRING || AccessSAItem(VSArrayIndex,src1).type != ST_STRING)
                            {
                                #ifdef DEBUG_INTERPRETER
                                PrintVS(VSArray[VSArrayIndex]);
                                #endif
                                VSArrayDestroy(VSArray, VSArrayIndex);
                                return E_UNCOMPATIBLE_TYPE;
                            }
                            ++paramLoaded;
                            ErrCheck( Find(firstParam, &AccessSAItem(VSArrayIndex,src1), dest) );
                            break;
                        }
                    }

                    break;

                case F_SORT:
                    while (++actInst < maxInstIndex && InstType(actInst) != I_ENDCALL && paramLoaded < 1)
                    {
                        if (InstType(actInst) == I_LOAD)
                            ErrCheck( ExecuteInstruction(conTable, VSArray[VSArrayIndex], AccessInst(actInst)) );
                        else //InstType(actInst) == I_PARAM
                        {
                            if (AccessSAItem(VSArrayIndex,src1).type != ST_STRING)
                            {
                                #ifdef DEBUG_INTERPRETER
                                PrintVS(VSArray[VSArrayIndex]);
                                #endif
                                VSArrayDestroy(VSArray, VSArrayIndex);
                                return E_UNCOMPATIBLE_TYPE;
                            }
                            ++paramLoaded;
                            ErrCheck( Sort(&AccessSAItem(VSArrayIndex,src1), dest) );
                            break;
                        }
                    }

                    break;

                case F_PRINT:
                    while (++actInst < maxInstIndex && InstType(actInst) != I_ENDCALL)
                    {
                        if (InstType(actInst) == I_LOAD)
                            ErrCheck( ExecuteInstruction(conTable, VSArray[VSArrayIndex], AccessInst(actInst)) );
                        else //InstType(actInst) == I_PARAM
                        {
                            ErrCheck( Print(&AccessSAItem(VSArrayIndex,src1), dest) );
                        }
                    }

                    break;

                //nemelo by nikdy nastat, znaci chybu
                default:
                    #ifdef DEBUG_INTERPRETER
                    PrintVS(VSArray[VSArrayIndex]);
                    #endif
                    VSArrayDestroy(VSArray, VSArrayIndex);
                    return E_INTER;
                    break;
            }

            //budu prochazet, dokud nedojdu na I_ENDCALL
            while (InstType(actInst) != I_ENDCALL && (++actInst < maxInstIndex))
            {
                ;
            }

            break;

        //----------------------------------I_RETURN-------------------------------------------------
        //konec funkce, ukladam navratovou hodnotu do nadrazeneho zasobniku
        case I_RETURN:

            //nemam nadrazeny zasobnik, koncim interpretaci (nastavim index na maximalni)
            if (VSArrayIndex == 0)
            {
                #ifdef DEBUG_INTERPRETER
                PrintVS(VSArray[VSArrayIndex]);
                #endif
                VSArrayDestroy(VSArray, VSArrayIndex);
                return E_OK;
            }

            //ukladam retezec
            if (AccessSAItem(VSArrayIndex,src1).type == ST_STRING)
            {
                AccessSAItemDirectly(VSArrayIndex-1,VSTACK_RET).value.string = \
                                malloc(sizeof(*AccessSAItemDirectly(VSArrayIndex-1,VSTACK_RET).value.string) * (strlen( AccessSAItem(VSArrayIndex,src1).value.string ) + 1));
                if (AccessSAItemDirectly(VSArrayIndex-1,VSTACK_RET).value.string == NULL)
                {
                    #ifdef DEBUG_INTERPRETER
                    PrintVS(VSArray[VSArrayIndex]);
                    #endif
                    VSArrayDestroy(VSArray, VSArrayIndex);
                    return E_INTER;
                }

                strcpy(AccessSAItemDirectly(VSArrayIndex-1,VSTACK_RET).value.string, AccessSAItem(VSArrayIndex,src1).value.string);
                AccessSAItemDirectly(VSArrayIndex-1, VSTACK_RET).type = ST_STRING;
            }
            //ukladam NIL
            else if (AccessSAItem(VSArrayIndex,src1).type == ST_NIL)
            {
                AccessSAItemDirectly(VSArrayIndex-1, VSTACK_RET).type = ST_NIL;
            }
            //ukladam LOGIC, NUMBER
            else
            {
                AccessSAItemDirectly(VSArrayIndex-1,VSTACK_RET).value.number = AccessSAItem(VSArrayIndex,src1).value.number;
                AccessSAItemDirectly(VSArrayIndex-1,VSTACK_RET).type = AccessSAItem(VSArrayIndex,src1).type;
            }

            //doskacu na konec funkce
            while (actInst < maxInstIndex && InstType(actInst+1) != I_FUNCEND)
            {
                ++actInst;
            }


            break;

        //----------------------------------I_JMPIFFALSE----------------------------------------------
        //podmineny skok (posun) na prislusny LABEL, pokud je podminka false, skacu
        case I_JMPIFFALSE:

            if ((AccessSAItem(VSArrayIndex,src1).type == ST_LOGIC && AccessSAItem(VSArrayIndex,src1).value.number < 0.0)
                || (AccessSAItem(VSArrayIndex,src1).type == ST_NUMBER && AccessSAItem(VSArrayIndex,src1).value.number == 0.0)
                || (AccessSAItem(VSArrayIndex,src1).type == ST_NIL)
                || (AccessSAItem(VSArrayIndex,src1).type == ST_STRING && AccessSAItem(VSArrayIndex,src1).value.string[0] == '\0'))
            {
                actInst = AccessSAItem(VSArrayIndex,dest).value.instAddr;
            }

            break;

        //----------------------------------I_JMP----------------------------------------------------
        //nepodmineny skok na adresu v dest
        case I_JMP:

            actInst = AccessSAItem(VSArrayIndex,dest).value.instAddr;

            break;


        //----------------------------------I_COPY_ARR----------------------------------------------------
        //vyber elementu z pole, rozsireni
        case I_COPY_ARR:

            if (AccessSAItem(VSArrayIndex,src1).type != ST_NUMBER)
            {
                VSArrayDestroy(VSArray, VSArrayIndex);
                return E_UNCOMPATIBLE_TYPE;
            }

            //pokousime se indexovat neco jineho nez pole
            if (AccessSAItem(VSArrayIndex,src2 -1).type != ST_ARRAY)
            {
                #ifdef DEBUG_INTERPRETER
                PrintVS(VSArray[VSArrayIndex]);
                #endif
                VSArrayDestroy(VSArray, VSArrayIndex);
                return E_OTHER_RUN;
            }

            paramLoaded = (unsigned int) trunc(AccessSAItem(VSArrayIndex,src1).value.number); //index prvku
            //pocet parametru (cislovani od 0)
            paramCount = (unsigned int) trunc(AccessSAItemDirectly(VSArrayIndex,AccessInst(actInst).src2 -1).value.number);

            //indexace mimo pole
            if (paramLoaded > paramCount)
            {
                VSArrayDestroy(VSArray, VSArrayIndex);
                return E_OTHER_RUN;
            }

            //zkopiruji prvek pole do destination
            temp.instType = I_COPY;
            temp.src1 = AccessInst(actInst).src2 + paramLoaded;
            temp.dest = AccessInst(actInst).dest;

            ErrCheck( ExecuteInstruction(conTable, VSArray[VSArrayIndex], temp) );

            break;

        //----------------------------------I_MODIFY_ARR-------------------------------------------------
        //modifikuje dany prvek pole
        case I_MODIFY_ARR:

            if (AccessSAItem(VSArrayIndex,src1).type != ST_NUMBER)
            {
                #ifdef DEBUG_INTERPRETER
                PrintVS(VSArray[VSArrayIndex]);
                #endif
                VSArrayDestroy(VSArray, VSArrayIndex);
                return E_UNCOMPATIBLE_TYPE;
            }

            //pokousime se indexovat neco jineho nez pole
            if (AccessSAItem(VSArrayIndex,src2 -1).type != ST_ARRAY)
            {
                #ifdef DEBUG_INTERPRETER
                PrintVS(VSArray[VSArrayIndex]);
                #endif
                VSArrayDestroy(VSArray, VSArrayIndex);
                return E_OTHER_RUN;
            }


            paramLoaded = (unsigned int) trunc(AccessSAItem(VSArrayIndex,src1).value.number); //index prvku
            //pocet parametru (cislovani od 0)
            paramCount = (unsigned int) trunc(AccessSAItemDirectly(VSArrayIndex,AccessInst(actInst).src2 -1).value.number);

            //indexace mimo pole
            if (paramLoaded > paramCount)
            {
                #ifdef DEBUG_INTERPRETER
                PrintVS(VSArray[VSArrayIndex]);
                #endif
                VSArrayDestroy(VSArray, VSArrayIndex);
                return E_OTHER_RUN;
            }

            //zkopiruji prvek pole do destination
            temp.instType = I_MOV;
            temp.src1 = AccessInst(actInst).dest;
            temp.dest = AccessInst(actInst).src2 + paramLoaded;

            ErrCheck( ExecuteInstruction(conTable, VSArray[VSArrayIndex], temp) );

            break;

        //----------------------------------I_COPY_INC-------------------------------------------------
        //kopirovani s naslednou inkrementaci src1
        case I_COPY_INC:

            temp.instType = I_COPY;
            temp.src1 = AccessInst(actInst).src1;
            temp.dest = AccessInst(actInst).dest;
            ErrCheck( ExecuteInstruction(conTable, VSArray[VSArrayIndex], temp) );

            ++AccessInst(actInst).src1;

            break;

        //----------------------------------DEFAULT - EXECUTE INSTRUCTION------------------------------
        //instrukce pracujici se zasobnikem
        default:
            ErrCheck( ExecuteInstruction(conTable, VSArray[VSArrayIndex], AccessInst(actInst)) );

            break;
    } //switch end

    if (actInst < maxInstIndex)
    {
        //posun na dalsi instrukci
        ++actInst;
    }
    } //while end

    VSArrayDestroy(VSArray, VSArrayIndex);

    return E_OK;
}

//###################################################################################################
//###################################################################################################

#define SrcTypesComp(type1, type2) (stack->items[inst.src1].type == type1 && stack->items[inst.src2].type == type2)
#define NumValFromVT(src) (stack->items[inst.src].value.number)
#define TypeFromVT(src) (stack->items[inst.src].type)
#define StrAddrVT(src) (stack->items[inst.src].value.string)

/**
* Vykona instrukci.
*
* Vykona jednoduche instrukce (aritmeticke, logicke, presuny atp.).
*
* @param conTable Ukazatel na tabulku konstant. (conTable_t*)
* @param stack Ukazatel na aktualni zasobnik. (valueStack_t*)
* @param inst Instrukce k vykonani. (inst_t)
* @return E_OK, E_INTERN (interni chyba), E_INCOMP_TYPES (nekompatibilni typy), E_DIV_ZERO (deleni nulou).
*/
static int ExecuteInstruction(conTable_t* conTable, valueStack_t* stack, inst_t inst)
{
    if (TypeFromVT(dest) == ST_STRING)
        free(StrAddrVT(dest));
    //zkontroluji, ze mam dostatecne veliky zasobnik a pripadne realokuji
    if (inst.dest >= stack->maxSize)
    {
        valueStackItem_t* temp = realloc(stack->items, sizeof(*temp) * (stack->maxSize + VAL_STACK_SIZE));
        if (temp == NULL)
        {
            return E_INTER;
        }

        stack->items = temp;

        //inicializuji vsechny nove prvky, abych potom vedel, ktere jiz byly pouzity a ktere ne
        unsigned int i = stack->maxSize;
        stack->maxSize += VAL_STACK_SIZE;

        for ( ; i <= stack->maxSize ; ++i)
            stack->items[i].type = ST_DEFAULT;
    }

    //provadim prislusnou instrukci
    switch (inst.instType)
    {
        //----------------------------------------I_ADD----------------------------------------------
        case I_ADD:
            if (SrcTypesComp(ST_NUMBER, ST_NUMBER)) //scitani dvou cisel
            {
                stack->items[inst.dest].value.number = stack->items[inst.src1].value.number + stack->items[inst.src2].value.number;
                stack->items[inst.dest].type = ST_NUMBER;
            }
            else if (SrcTypesComp(ST_STRING, ST_STRING)) //konkatenace dvou retezcu
            {
                size_t src1lenSS = strlen(stack->items[inst.src1].value.string);

                if (stack->items[inst.src2].type == ST_STRING)
                {
                    stack->items[inst.dest].value.string = malloc(sizeof(*stack->items[inst.dest].value.string) \
                                                                  * ( src1lenSS + strlen(stack->items[inst.src2].value.string) + 1));
                    if (stack->items[inst.dest].value.string == NULL)
                        return E_INTER;
                    strcpy(stack->items[inst.dest].value.string, stack->items[inst.src1].value.string); //kopiruji prvni cast
                    strcpy(stack->items[inst.dest].value.string + src1lenSS, stack->items[inst.src2].value.string); //druhou cast
                }
                stack->items[inst.dest].type = ST_STRING;
            }
// Alokacni konstanta +15 s jistotou pokryva maximalni velikost retezce, ktery produkuje printf("%g") == max 13
            //konkatenace src1 + cokoliv
            else if (stack->items[inst.src1].type == ST_STRING)
            {
                size_t src1len = strlen(stack->items[inst.src1].value.string);

                stack->items[inst.dest].value.string = malloc(sizeof(*stack->items[inst.dest].value.string) * (src1len + 1 + 15));

                strcpy(stack->items[inst.dest].value.string, stack->items[inst.src1].value.string); //kopiruji prvni cast

                //kopiruji druhou cast (jednu z nich)
                if (stack->items[inst.src2].type == ST_NUMBER)
                    sprintf(stack->items[inst.dest].value.string + src1len, "%g", stack->items[inst.src2].value.number);

                else if (stack->items[inst.src2].type == ST_NIL)
                    sprintf(stack->items[inst.dest].value.string + src1len, "Nil");

                else if (stack->items[inst.src2].type == ST_LOGIC)
                    sprintf(stack->items[inst.dest].value.string + src1len, "%s", (stack->items[inst.src2].value.number > 0.0 ? "true" : "false"));
                else
                    return E_INTER; //nikdy by nemelo nastat

                stack->items[inst.dest].type = ST_STRING;
            }
            //konkatenace cokoliv + src2
            else if (stack->items[inst.src2].type == ST_STRING)
            {
                stack->items[inst.dest].value.string = malloc(sizeof(*stack->items[inst.dest].value.string)\
                                                          * (strlen(stack->items[inst.src2].value.string) + 1 + 15));

                int printed;

                if (stack->items[inst.src1].type == ST_NUMBER)
                    printed = sprintf(stack->items[inst.dest].value.string, "%g", stack->items[inst.src1].value.number);
                else if (stack->items[inst.src1].type == ST_NIL)
                    printed = sprintf(stack->items[inst.dest].value.string, "Nil");
                else if (stack->items[inst.src1].type == ST_LOGIC)
                    printed = sprintf(stack->items[inst.dest].value.string, "%s", (stack->items[inst.src1].value.number > 0.0 ? "true" : "false"));
                else
                    return E_INTER; //nemelo by nastat

                //kopiruji zbytek rerezce
                strcpy(stack->items[inst.dest].value.string + printed, stack->items[inst.src2].value.string);

                stack->items[inst.dest].type = ST_STRING;
            }
            else //chyba
                return E_UNCOMPATIBLE_TYPE;
            break;


        //----------------------------------------I_SUB----------------------------------------------
        case I_SUB:
            if ( ! SrcTypesComp(ST_NUMBER, ST_NUMBER) )
            {
                return E_UNCOMPATIBLE_TYPE;
            }
            stack->items[inst.dest].value.number = stack->items[inst.src1].value.number - stack->items[inst.src2].value.number;
            stack->items[inst.dest].type = ST_NUMBER;
            break;


        //----------------------------------------I_MUL----------------------------------------------
        case I_MUL:
            //nasobeni dvou cisel
            if ( SrcTypesComp(ST_NUMBER, ST_NUMBER) )
            {
                stack->items[inst.dest].value.number = stack->items[inst.src1].value.number * stack->items[inst.src2].value.number;
                stack->items[inst.dest].type = ST_NUMBER;
            }
            //mocnina retezce
            else if ( SrcTypesComp(ST_STRING, ST_NUMBER) )
            {
                int number = (int) trunc(stack->items[inst.src2].value.number);

                if (number == 0) //pri nulte mocnine retezce vznika prazdny retezec
                {
                    StrAddrVT(dest) = malloc(sizeof(*StrAddrVT(dest)) * 1);
                    if (StrAddrVT(dest) == NULL)
                        return E_INTER;

                    StrAddrVT(dest)[0] = '\0';
                    TypeFromVT(dest) = ST_STRING;

                    break;
                }

                size_t src1len = strlen(stack->items[inst.src1].value.string);

                stack->items[inst.dest].value.string = malloc(sizeof(*stack->items[inst.dest].value.string) * (number * src1len +1));
                if (stack->items[inst.dest].value.string == NULL) //neuspesna alokace
                    return E_INTER;

                for (int i = 0; i < number; ++i) //kopiruji retezec i-krat za sebe
                    strcpy(stack->items[inst.dest].value.string + i*src1len, stack->items[inst.src1].value.string);

                stack->items[inst.dest].type = ST_STRING;
            }
            //mocnina retezce
            else if ( SrcTypesComp(ST_NUMBER, ST_STRING) )
            {
                int number = (int) trunc(stack->items[inst.src1].value.number);

                if (number == 0) //pri nulte mocnine retezce vznika prazdny retezec
                {
                    StrAddrVT(dest) = malloc(sizeof(*StrAddrVT(dest)) * 1);
                    if (StrAddrVT(dest) == NULL)
                        return E_INTER;

                    StrAddrVT(dest)[0] = '\0';
                    TypeFromVT(dest) = ST_STRING;

                    break;
                }

                size_t src2len = strlen(stack->items[inst.src2].value.string);

                stack->items[inst.dest].value.string = malloc(sizeof(*stack->items[inst.dest].value.string) * (number * src2len +1));
                if (stack->items[inst.dest].value.string == NULL) //neuspesna alokace
                    return E_INTER;

                for (int i = 0; i < number; ++i)
                    strcpy(stack->items[inst.dest].value.string + i*src2len, stack->items[inst.src2].value.string);

                stack->items[inst.dest].type = ST_STRING;
            }
            else //chyba
            {
                return E_UNCOMPATIBLE_TYPE;
            }

            break;

        //----------------------------------------I_DIV----------------------------------------------
        case I_DIV:
            if ( ! SrcTypesComp(ST_NUMBER, ST_NUMBER) )
                return E_UNCOMPATIBLE_TYPE;
            if (stack->items[inst.src2].value.number == 0.0) //deleni nulou
                return E_DIV_ZERO;
            stack->items[inst.dest].value.number = stack->items[inst.src1].value.number / stack->items[inst.src2].value.number;
            stack->items[inst.dest].type = ST_NUMBER;

            break;

        //----------------------------------------I_POW----------------------------------------------
        case I_POW: // **
            if ( ! SrcTypesComp(ST_NUMBER, ST_NUMBER))
                return E_UNCOMPATIBLE_TYPE;

            stack->items[inst.dest].value.number = pow(stack->items[inst.src1].value.number, stack->items[inst.src2].value.number);
            stack->items[inst.dest].type = ST_NUMBER;

            break;

        //----------------------------------------I_ISEQ----------------------------------------------
        case I_ISEQ: // ==
            //rozdilne typy, vysledek false
            if ( stack->items[inst.src1].type != stack->items[inst.src2].type )
            {
                stack->items[inst.dest].value.number = -1.0;
            }
            //oba operandy jsou cislo
            else if ( SrcTypesComp(ST_NUMBER, ST_NUMBER) )
            {
                stack->items[inst.dest].value.number = stack->items[inst.src1].value.number\
                                                        == stack->items[inst.src2].value.number ? 1.0 : -1.0;
            }
            //oba operandy jsou retezec
            else if ( SrcTypesComp(ST_STRING, ST_STRING) )
            {
                //jsou stejne
                if (strcmp(stack->items[inst.src1].value.string, stack->items[inst.src2].value.string) == 0)
                    stack->items[inst.dest].value.number = 1.0;
                else //nejsou stejne
                    stack->items[inst.dest].value.number = -1.0;
            }
            //oba operandy jsou logicke hodnoty
            else if ( SrcTypesComp(ST_LOGIC, ST_LOGIC) )
            {
                //true, oba operandy jsou TRUE
                if ( stack->items[inst.src1].value.number > 0.0 && stack->items[inst.src2].value.number > 0.0)
                    stack->items[inst.dest].value.number = 1.0;
                //true, oba operandy jsou FALSE
                else if (stack->items[inst.src1].value.number < 0.0 && stack->items[inst.src2].value.number < 0.0)
                    stack->items[inst.dest].value.number = 1.0;
                //false, operandy maji rozdilne hodnoty
                else
                    stack->items[inst.dest].value.number = -1.0;
            }
            //oba operandy jsou NIL => true
            else if ( SrcTypesComp(ST_NIL, ST_NIL) )
            {
                stack->items[inst.dest].value.number = 1.0;
            }
            else
                return E_INTER; //nemelo by nastat

            stack->items[inst.dest].type = ST_LOGIC;
            break;

        //----------------------------------------I_ISNOTEQ----------------------------------------------
        case I_ISNOTEQ: // !=
            //rozdilne typy, vysledek true
            if ( stack->items[inst.src1].type != stack->items[inst.src2].type )
            {
                stack->items[inst.dest].value.number = 1.0;
            }
            //oba operandy jsou cislo
            else if ( SrcTypesComp(ST_NUMBER, ST_NUMBER) )
            {
                if (stack->items[inst.src1].value.number != stack->items[inst.src2].value.number)
                    stack->items[inst.dest].value.number = 1.0;
                else
                    stack->items[inst.dest].value.number = -1.0;
            }
            //oba operandy jsou retezce
            else if ( SrcTypesComp(ST_STRING, ST_STRING) )
            {
                //nejsou stejne => true
                if (strcmp(stack->items[inst.src1].value.string, stack->items[inst.src2].value.string) != 0)
                    stack->items[inst.dest].value.number = 1.0;
                //jsou stejne => false
                else
                    stack->items[inst.dest].value.number = -1.0;
            }
            //oba operandy jsou logicke hodnoty
            else if ( SrcTypesComp(ST_LOGIC, ST_LOGIC) )
            {
                //oba operandy jsou TRUE => false
                if ( stack->items[inst.src1].value.number > 0.0 && stack->items[inst.src2].value.number > 0.0)
                    stack->items[inst.dest].value.number = -1.0;
                //oba operandy jsou FALSE => false
                else if (stack->items[inst.src1].value.number < 0.0 && stack->items[inst.src2].value.number < 0.0)
                    stack->items[inst.dest].value.number = -1.0;
                //operandy maji rozdilne hodnoty => true
                else
                    stack->items[inst.dest].value.number = 1.0;
            }
            //oba operandy jsou NIL => false
            else if ( SrcTypesComp(ST_NIL, ST_NIL) )
            {
                stack->items[inst.dest].value.number = -1.0;
            }
            else
                return E_UNCOMPATIBLE_TYPE;

            stack->items[inst.dest].type = ST_LOGIC;
            break;

        //----------------------------------------I_LESS----------------------------------------------
        case I_LESS: // <
            //oba operandy jsou cislo
            if ( SrcTypesComp(ST_NUMBER, ST_NUMBER) )
            {
                if (stack->items[inst.src1].value.number < stack->items[inst.src2].value.number)
                    stack->items[inst.dest].value.number = 1.0;
                else
                    stack->items[inst.dest].value.number = -1.0;
            }
            //oba operandy jsou retezce
            else if ( SrcTypesComp(ST_STRING, ST_STRING) )
            {
                //nejsou stejne => true
                if (strcmp(stack->items[inst.src1].value.string, stack->items[inst.src2].value.string) < 0)
                    stack->items[inst.dest].value.number = 1.0;
                //jsou stejne => false
                else
                    stack->items[inst.dest].value.number = -1.0;
            }
            else
                return E_UNCOMPATIBLE_TYPE;

            stack->items[inst.dest].type = ST_LOGIC;
            break;

        //----------------------------------------I_GREATER----------------------------------------------
        case I_GREATER: // >
            //oba operandy jsou cislo
            if ( SrcTypesComp(ST_NUMBER, ST_NUMBER) )
            {
                if (stack->items[inst.src1].value.number > stack->items[inst.src2].value.number)
                    stack->items[inst.dest].value.number = 1.0;
                else
                    stack->items[inst.dest].value.number = -1.0;
            }
            //oba operandy jsou retezce
            else if ( SrcTypesComp(ST_STRING, ST_STRING) )
            {
                //nejsou stejne => true
                if (strcmp(stack->items[inst.src1].value.string, stack->items[inst.src2].value.string) > 0)
                    stack->items[inst.dest].value.number = 1.0;
                //jsou stejne => false
                else
                    stack->items[inst.dest].value.number = -1.0;
            }
            else
                return E_UNCOMPATIBLE_TYPE;

            stack->items[inst.dest].type = ST_LOGIC;
            break;

        //----------------------------------------I_LESSOREQ----------------------------------------------
        case I_LESSOREQ: // <=
            //oba operandy jsou cislo
            if ( SrcTypesComp(ST_NUMBER, ST_NUMBER) )
            {
                if (stack->items[inst.src1].value.number <= stack->items[inst.src2].value.number)
                    stack->items[inst.dest].value.number = 1.0;
                else
                    stack->items[inst.dest].value.number = -1.0;
            }
            //oba operandy jsou retezce
            else if ( SrcTypesComp(ST_STRING, ST_STRING) )
            {
                //nejsou stejne => true
                if (strcmp(stack->items[inst.src1].value.string, stack->items[inst.src2].value.string) <= 0)
                    stack->items[inst.dest].value.number = 1.0;
                //jsou stejne => false
                else
                    stack->items[inst.dest].value.number = -1.0;
            }
            else
                return E_UNCOMPATIBLE_TYPE;

            stack->items[inst.dest].type = ST_LOGIC;
            break;

        //----------------------------------------I_GREATEROREQ----------------------------------------------
        case I_GREATEROREQ: // >=
            //oba operandy jsou cislo
            if ( SrcTypesComp(ST_NUMBER, ST_NUMBER) )
            {
                if (stack->items[inst.src1].value.number >= stack->items[inst.src2].value.number)
                    stack->items[inst.dest].value.number = 1.0;
                else
                    stack->items[inst.dest].value.number = -1.0;
            }
            //oba operandy jsou retezce
            else if ( SrcTypesComp(ST_STRING, ST_STRING) )
            {
                //nejsou stejne => true
                if (strcmp(stack->items[inst.src1].value.string, stack->items[inst.src2].value.string) >= 0)
                    stack->items[inst.dest].value.number = 1.0;
                //jsou stejne => false
                else
                    stack->items[inst.dest].value.number = -1.0;
            }
            else
                return E_UNCOMPATIBLE_TYPE;

            stack->items[inst.dest].type = ST_LOGIC;
            break;

        //----------------------------------------I_MOV----------------------------------------------
        case I_MOV:
            //presouvam hodnoty, stare likviduji
            //presouvam retezec
            if (TypeFromVT(src1) == ST_STRING)
            {
                //nemusim kopirovat, staci nacist ukazatele a puvodni hodnotu nastavit na neinicializovanou
                StrAddrVT(dest) = StrAddrVT(src1);
                StrAddrVT(src1) = NULL;
                TypeFromVT(src1) = ST_DEFAULT;
                TypeFromVT(dest) = ST_STRING;
            }
            //presouvam NIL
            else if (TypeFromVT(src1) == ST_NIL)
            {
                TypeFromVT(dest) = ST_NIL;
                TypeFromVT(src1) = ST_DEFAULT;
            }
            //presouvam LOGIC, NUMBER, ARRAY
            else
            {
                NumValFromVT(dest) = NumValFromVT(src1);
                TypeFromVT(dest) = TypeFromVT(src1);
                TypeFromVT(src1) = ST_DEFAULT;
            }

            break;

        //----------------------------------------I_COPY----------------------------------------------
        //kopiruji hodnoty
        case I_COPY:

            //kopiruji retezec
            if (TypeFromVT(src1) == ST_STRING)
            {
                StrAddrVT(dest) = malloc(sizeof(*StrAddrVT(dest)) * (strlen(StrAddrVT(src1)) + 1));
                if (StrAddrVT(dest) == NULL)
                    return E_INTER;

                strcpy(StrAddrVT(dest), StrAddrVT(src1));

                TypeFromVT(dest) = ST_STRING;
            }
            //kopiruji NIL
            else if (TypeFromVT(src1) == ST_NIL)
            {
                TypeFromVT(dest) = ST_NIL;
            }
            //presouvam LOGIC, NUMBER, ARRAY
            else
            {
                NumValFromVT(dest) = NumValFromVT(src1);
                TypeFromVT(dest) = TypeFromVT(src1);
            }

            break;

        //----------------------------------------I_LOAD----------------------------------------------
        case I_LOAD:
            //nacitam retezec
            if (conTable->items[inst.src1].type == ST_STRING)
            {
                StrAddrVT(dest) = malloc(sizeof(*StrAddrVT(dest)) * (strlen(conTable->items[inst.src1].value.string) + 1));
                if (StrAddrVT(dest) == NULL)
                    return E_INTER;

                strcpy(StrAddrVT(dest), conTable->items[inst.src1].value.string);
                TypeFromVT(dest) = ST_STRING;
            }
            //nacitam NIL
            else if (conTable->items[inst.src1].type == ST_NIL)
            {
                TypeFromVT(dest) = ST_NIL;
            }
            //nacitam adresu instrukce, typ ST_FUNC
            else if (conTable->items[inst.src1].type == ST_FUNC)
            {
                TypeFromVT(dest) = ST_FUNC;
                stack->items[inst.dest].value.instAddr = conTable->items[inst.src1].value.instAddr;
            }
            //nacitam LOGIC, NUMBER
            else
            {
                NumValFromVT(dest) = conTable->items[inst.src1].value.number;
                TypeFromVT(dest) = conTable->items[inst.src1].type;
            }
            break;

        default:
            return E_INTER; break;
    } //switch end

    return E_OK;
}

/**
* Vytvori tabulku hodnot.
*
* Vytvori tabulku (zasobnik) hodnot na heapu, se kterym operujeme ve funkcich.
*
* @return Ukazatel na novou tabulku hodnot. (valueStack_t*)
*/
valueStack_t* VStackInit(void)
{
    valueStack_t* stack = malloc(sizeof(*stack));
    if (stack == NULL)
        return NULL;

    stack->items = malloc(sizeof(*stack->items) * VAL_STACK_SIZE);
    if (stack->items == NULL)
    {
        free(stack);
        return NULL;
    }

    //inicializuji vsechny prvky, abych potom vedel, ktere jiz byly pouzity a ktere ne
    for (unsigned int i = 0; i < VAL_STACK_SIZE; ++i)
        stack->items[i].type = ST_DEFAULT;
    stack->maxSize = VAL_STACK_SIZE;

    return stack;
}

/**
* Zlikviduje pole tabulek hodnot.
*
* Zlikviduje a korektne uvolni pamet pole tabulek hodnot.
*
* @param array Ukazatel na pole tabulek hodnot. (valueStack_t**)
* @param maxIndex Index urcujici pocet aktualne vytvorenych tabulek hodnot. (unsigned int)
*/
void VSArrayDestroy(valueStack_t** array, unsigned int maxIndex)
{
    for (unsigned int i = 0; i <= maxIndex; ++i)
        VSDestroy(array[i]);

    free(array);
    array = NULL;
}

//znici a uklidi jednu tabulku hodnot
static void VSDestroy(valueStack_t* stack)
{
    if (stack == NULL)
        return;

    //for (unsigned int i = 0; stack->items[i].type != ST_DEFAULT; ++i)
    for (unsigned int i = 0; i < stack->maxSize; ++i)
    {
        if (stack->items[i].type == ST_STRING)
            free(stack->items[i].value.string);
    }

    free(stack->items);
    free(stack);
    stack = NULL;
}


//----------------------------DEBUG_SEGMENT-------------------------
#ifdef DEBUG_INTERPRETER
/*#include <float.h>
int main(void)
{
    int printed = printf("%g\n", (double)10000000);
    printf("Pocet vytisknutych znaku: %d\n", printed-1);

    return 0;
}*/

void PrintVS(valueStack_t* stack)
{
    if (stack == NULL)
        fprintf(stderr, "PrintVS has received a NULL pointer\n");

    char* types[] = {
        "ST_DEFAULT", "ST_STRING", "ST_NIL", "ST_LOGIC", "ST_NUMBER", "ST_FUNC", "ST_ARRAY", "ST_UNKNOWN",};

    printf("\n------------------ VALUE_STACK_DUMP ------------------\n\n");

    printf("Index   Type           Value\n\n");
    for (unsigned int i = 0; i < stack->maxSize; ++i)
    {
        if (stack->items[i].type != ST_DEFAULT)
        {
            printf("%4u\t",i);
            int type=stack->items[i].type;
            if (type < 0 || type > 6)
                type = 7;
            printf("%-15s",types[type]);
            if (type == ST_STRING)
                printf("%s\n",stack->items[i].value.string);
            else if (type == ST_NUMBER)
                printf("%lf\n",stack->items[i].value.number);
            else if (type == ST_NIL)
                printf("NIL\n");
            else if (type == ST_LOGIC)
            {
                if (stack->items[i].value.number > 0)
                    printf("TRUE  (%lf)\n",stack->items[i].value.number);
                else
                    printf("FALSE (%lf)\n",stack->items[i].value.number);
            }
            else if(type == ST_FUNC)
                printf("(%u) Address\n",stack->items[i].value.instAddr);
            else if (type == ST_ARRAY)
                printf("%lf\n",stack->items[i].value.number);
        }
    }
    printf("\n---------------- VALUE_STACK_DUMP_END ----------------\n");

}
#endif
