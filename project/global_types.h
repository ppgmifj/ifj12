/*
 * *******************************************************************
 * Date:         25. 10. 2012
 * Author(s):    Gabriel Brandersky
 * Project:      Interpret IFJ12
 * Team:         Petr Jirout,         xjirou07
 *               Petr Huf,            xhufpe00
 *               Matej Vanatko,       xvanat01
 *               Gabriel Brandersky,  xbrand04
 * Description:
 * Spolocne casti programu
 * *******************************************************************
 */

#ifndef GLOBAL_TYPES_H_INCLUDED
#define GLOBAL_TYPES_H_INCLUDED

#define END_INDEX_VALID 1
#define END_INDEX_EMPTY 0

#define VSTACK_INST_PTR 0
#define VSTACK_RET 1
#define VSTACK_PARAM 2

// #define DEBUG_PARSER 1
/**
 * Vyctovy typ pre chybove kody programu
 */
typedef enum ecodes
{
    E_OK = 0,             ///< Bez chyby
    E_LEXEM,              ///< lexikalna chyba - chybna struktura lexemu
    E_SYNTAX,             ///< syntakticka chyba - chybna struktura programu

    /* semanticke chyby */
    E_VAR_SEM,            ///< Nedefinovana premena
    E_FUNC_SEM,           ///< Nedefinovana funkcia
    E_OTHER_SEM,          ///< Ostatne semanticke chyby

    /* Chyby za behu */
    E_DIV_ZERO = 10,      ///< Delenie nulov
    E_UNCOMPATIBLE_TYPE,  ///< Nekompatiblne typy
    E_CASTING,            ///< Chyba pretypovani - funkce numeric
    E_OTHER_RUN,          ///< Ostatne behove chyby

    /* Interna chyba - neovplyvnena vstupnym suborom */
    E_INTER = 99,         ///< malloc, fopen, atd.
}ecodes_e;

#endif // GLOBAL_TYPES_H_INCLUDED
