/**
* @file inst_list.c
* @brief Operace nad seznamem instrukci.

*  Implementace operaci nad seznamem instrukci.
*/
/*
*********************************************************************
* File:         inst_list.c
* Date:         15. 10. 2012
* Author:       Petr Jirout
* Project:      Interpret IFJ12
* Team:         Petr Jirout,        xjirou07
*               Petr Huf,           xfugpe00
*               Matej Vanatko,      xvanat01
*               Gabriel Brandersky  xbrand04
*********************************************************************
*/

#include "inst_list.h"
#include "ial.h"

#include <stdio.h>
#include <stdlib.h>


/**
* Inicializuje nove instrukcni pole.
*
* @return Ukazatel na nove instrukcni pole, NULL pri chybe alokace. (inst_t*)
* @warning Inicializovat na nejakou neskodnou hodnotu?
*/
instList_t* InstListInit(void)
{
    instList_t* newList = malloc(sizeof(*newList));
    if (newList == NULL)
        return NULL;

    newList->instArray = malloc(sizeof(*newList->instArray) * INST_ARRAY_LENGTH);
    if (newList->instArray == NULL)
    {
        free(newList);
        return NULL;
    }

    newList->actIndex = 0;
    newList->maxSize = INST_ARRAY_LENGTH;

    return newList;
}

/**
* Prida instrukci na konec instrukcni pasky.
*
* @param L Ukazatel na instrukcni pasku, do ktere chci vkladat.
* @param I Struktura obsahujici data instrukce
* return Pri chybe vraci -1, jinak 0. (int)
*/
int InstAdd(instList_t* L, inst_t I)
{
#ifdef DEBUG_INST
    if (L == NULL)
    {
        fprintf(stderr, "Internal error: InstAdd received a NULL pointer.\n");
        return -1;     //vypsat chybu?
    }
#endif

    //male pole, treba realokovat
    if (L->actIndex + 1 >= L->maxSize)
    {
        inst_t* temp = realloc(L->instArray, L->maxSize + INST_ARRAY_LENGTH);
        if (temp == NULL)
            return -1;

        L->instArray = temp;
        L->maxSize += INST_ARRAY_LENGTH;
    }

    L->instArray[L->actIndex++] = I;

    return 0;
}

/**
* Vycisti instrukcni pasku.
*
* Uvolni pamet a vyNULLuje ukazatel na pasku.
*
* @param L Ukazatel na instrukcni pasku, kterou chci uvolnit. (instList_t*)
*/
void InstListDestroy(instList_t* L)
{
#ifdef DEBUG_INST
    if (L == NULL)
    {
        fprintf(stderr,"Internal error: InstListFree received a NULL pointer.\n");
        return;
    }
#endif

    free(L->instArray);
    free(L);
    L = NULL;
}


#ifdef DEBUG_INST

int main(void)
{
    instList_t* myList = InstListInit();
    inst_t myInst;

    myInst.instType = I_STOP;
    InstAdd(myList, myInst);
    myInst.instType = I_ADD;
    InstAdd(myList, myInst);
    myInst.instType = I_SUB;
    InstAdd(myList, myInst);

    PrintInstList(myList);

    InstListDestroy(myList);

    return 0;
}
#endif
