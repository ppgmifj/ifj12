/*
*********************************************************************
* File:         interpreter.h
* Date:         12. 11. 2012
* Author:       Petr Jirout
* Project:      Interpret IFJ12
* Team:         Petr Jirout,        xjirou07
*               Petr Huf,           xfugpe00
*               Matej Vanatko,      xvanat01
*               Gabriel Brandersky  xbrand04
*
* Description:  Interpretacni cast IFJ12.
*********************************************************************
*/

#ifndef INTERPRETER_H_INCLUDED
#define INTERPRETER_H_INCLUDED

#include "inst_list.h"
#include "ial.h"


#define STACK_ARRAY_SIZE 10     ///< Inicializacni velikost zasobniku, urcuje pocet zanoreni pred realokaci

/**
* Struktura popisujici obsah prvku (polozky) tabulky hodnot.
*/
union valueStackValue
{
    unsigned int index;     ///< Index do tabulky retezcu (type == ST_STRING)
    double number;          ///< Hodnota (type == ST_NUMBER)
    char* string;           ///< Ulozeni retezce
    unsigned int instAddr;   ///< Adresa instrukce (pro jmp)
};

/**
* Struktura popisujici prvek (polozku) tabulky hodnot.
*/
typedef struct valueStackItem
{
    union valueStackValue value;  ///< Index do tabulky retezcu nebo hodnota
    ST_TYPE type;                 ///< Typ ulozene hodnoty
} valueStackItem_t;

#define VAL_STACK_SIZE 256 ///< Inicializacni velikost tabulky hodnot

/**
* Struktura popisuju tabulku hodnot, tvorenou pomoci pole
*/
typedef struct valueStack
{
    valueStackItem_t* items;    ///< Dynamicky alokovane pole
    unsigned int maxSize;       ///< Velikost pameti alokovane pro pole
} valueStack_t;

int Interpreter(conTable_t* conTable, const instList_t* instList);
valueStack_t* VStackInit(void);
void VSArrayDestroy(valueStack_t** array, unsigned int maxIndex);

#endif // INTERPRETER_H_INCLUDED
