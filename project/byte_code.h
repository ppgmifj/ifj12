/*
 * *******************************************************************
 * Date:         24.11. 2012
 * Author(s):    Petr Huf
 * Project:      Interpret IFJ12
 * Team:         Petr Jirout,         xjirou07
 *               Petr Huf,            xhufpe00
 *               Matej Vanatko,       xvanat01
 *               Gabriel Brandersky,  xbrand04
 * Description:
 * Implementace byte codu
 ********************************************************************
 */

#ifndef ___BYTECODE___
#define ___BYTECODE___


ecodes_e GenerateByteCode(void);
ecodes_e DoByteCode();

#endif
