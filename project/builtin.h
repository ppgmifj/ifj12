/*
 * *******************************************************************
 * Date:         15.11. 2012
 * Author(s):    Petr Huf
 * Project:      Interpret IFJ12
 * Team:         Petr Jirout,         xjirou07
 *               Petr Huf,            xhufpe00
 *               Matej Vanatko,       xvanat01
 *               Gabriel Brandersky,  xbrand04
 * Description:
 * Implementace vestavenych funkci interpretu
 ********************************************************************
 */

#ifndef ___BUILTIN___
#define ___BUILTIN___

#include "global_types.h"
#include "interpreter.h"

/**
* Vycet specifikujici vestavene funkce poskytovane interpretem.*/
typedef enum
{
    F_PRINT,
    F_NUMERIC,
    F_INPUT,
    F_TYPEOF,
    F_LEN,
    F_FIND,
    F_SORT,
} BUILT_IN_FUNC;


ecodes_e Input(valueStackItem_t* dest);
ecodes_e Numeric(valueStackItem_t* item, valueStackItem_t* dest);
ecodes_e Print(valueStackItem_t* item, valueStackItem_t* dest);
ecodes_e TypeOf(valueStackItem_t* item, valueStackItem_t* dest);
void     Len(valueStackItem_t* item, valueStackItem_t* dest);
ecodes_e Find(valueStackItem_t* item1, valueStackItem_t* item2, valueStackItem_t* dest);
ecodes_e Sort(valueStackItem_t* item, valueStackItem_t* dest);

#endif
