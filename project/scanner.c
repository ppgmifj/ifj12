/**
 * @file scanner.c
 * @brief Tento soubor obsahuje lexikalni analyzator.
 * !! POUZE ZATIM BETA VERZE, CHYBI JESTE PORADNE OTESTOVAT !!
 * Modifikace:
 *  23.10.2012  -> doplneno rozpoznavani cisel
 *              -> podpora escape sekvece lomitko " v retezci
 *              -> podpora i retezce a rezervovanych slov
 * SNAD FUNKCNI, BUDU LADIT A DELAT AUTOMATICKE TESTY (BUDOU SE HODIT)
 * Obsahlejsi popis ako brief,
 * V tomto souboru je napsana v jazyce C implementace konecneho automatu
 * lexikalniho analyzatoru IFJ tymu 71
 */
/*
 ********************************************************************
 * File:         scanner.c
 * Date:         17.10. 2012
 * Author(s):    Matej Vanatko
 * Project:      Interpret IFJ12
 * Team:         Petr Jirout,         xjirou07
 *               Petr Huf,            xhufpe00
 *               Matej Vanatko,       xvanat01
 *               Gabriel Brandersky,  xbrand04
 ********************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "global_types.h"
#include "scanner.h"


/**
 * Kratky, jednoradkovy popis funkce.
 *
 * Obsahlejsi, podrobnejsi popis cinnosti funkce.
 * Klidne na vice radku.
 *
 * @bug Obcas vraci spatne vysledky.
 * @warning Pri zadani cisla 0 se zachveje lampicka.
 * @param firstNumber prvni operand (int)
 * @param secondNumber druhy operand (int)
 * @return prumer zadanych cisel (double)
 */

char *KWordsTable[] = {
    "function","else","if","nil","return","true","false","while","loop","for",
    "in","end"
};

char *RWordsTable[] = {
     "as","def", "directive","export","from","import","launch","load","macro"
};

// Velikost o kterou zvetsujeme misto pro nacteny string
#define BUFFER_INIT 200
#define EOL '\n'
typedef enum ecodess
{
     LEX_OK = 0,
     LEX_INTER = 99,
}AddCharERR;

enum FSM_states
{
    ST_START = 0,
    ST_NUMBER,
    ST_KEYORVAR,
    ST_VARIABLE,
    ST_STRING,
    ST_COMMENT,
    ST_COMMENT_BLOCK,
    ST_COMMENT_OR_DIV,
    ST_PUNCT,
};

static AddCharERR add_char(int position, char c)
{
    static int buffer_size = 0;
    if(position == buffer_size)
    {
        char *tmp = TokenInfoGlobal.string;
        buffer_size += BUFFER_INIT;
        TokenInfoGlobal.string = realloc (TokenInfoGlobal.string, buffer_size*sizeof(char));
        if(TokenInfoGlobal.string == NULL)
        {
            TokenInfoGlobal.string = tmp;
            return LEX_INTER;
        }

    }
    TokenInfoGlobal.string[position] = c;
    return LEX_OK;
}

// Funkce zjistujici, jestli mam ID nebo KEYWORD
static int key_word(char *word)
{
    int k = 0;
    for(int i = T_FUNC; i <= T_END; ++i)
    {
        if(strcmp(word, KWordsTable[k]) == 0)
            return i;
        else
            ++k;
    }

    k=0;
    for(int i = 0; i <= 8; ++i)
    {
        if(strcmp(word, RWordsTable[k]) == 0)
            return -2;
        else
            ++k;
    }
    return -1;
}

// Funkce kontroluje escape sekvence v retezci
// 0   uspech
// -1  neuspech
int CheckEscapeSequences(const char *str)
{
  int i=0;
  while (str[i] != '\0')
  {
    // Je tam escape sekvence
    if (str[i] == '\\')
    {
      ++i;
      if (str[i] == '\0')
        return -1;

      if (str[i] == 'n' || str[i] == 't' || str[i] == '\\' || str[i] == '"')
      {
        ++i;
        continue;
      }

      // Je to escape sekvence \x4F
      if (str[i] == 'x')
      {
        ++i;
        if (str[i] == '\0')
          return -1;
        if (isdigit(str[i]) || (tolower(str[i]) >= 'a' && tolower(str[i]) <= 'f'))
        {
          ++i;
          if (str[i] == '\0')
          return -1;
          if (isdigit(str[i]) || (tolower(str[i]) >= 'a' && tolower(str[i]) <= 'f'))
          {
            ++i;
            continue;
          }
        }
      }

      return -1;     // Nenalezena pozadovana escape sekvence
    }
    else
      ++i;
  }

  return 0;
}

// Funcke nacte nasledujici token
static TTokenType GetNextTokenTrue()
{
    static int type = T_START;
    if (type > T_START)
        return (TokenInfoGlobal.type=type);
    else
        type = T_START;
    int save = 0;
    static int line = 1;
    int state = ST_START;
    int c = 0;
    int i = 0;
    int check = 0;
    int check2 = 0;
    if(add_char(0,'\0') != LEX_OK)
      return LEX_INTER;
    TokenInfoGlobal.len = 0;

    while(type == T_START)
    {
        TokenInfoGlobal.line = line;
        if(save == 0)
            c = fgetc(FileGlobal);
        else
        {
            c = save;
            save = 0;
        }
        switch (state)
        {

            // pocatecni stav
            case ST_START:
                if(c == EOL)
		{
                    ++line;
		    TokenInfoGlobal.line=line-1;
		    return (TokenInfoGlobal.type=T_EOL);
		}
                else if(isdigit(c))
                {
                    save = c;
                    state = ST_NUMBER;
                }
                else if(isalpha(c))
                {
                    save = c;
                    state = ST_KEYORVAR;
                }
                else if(c == '_')
                    state = ST_VARIABLE;
                else if(isspace(c))
                {}
                else if(c == '"')
                    state = ST_STRING;
                else if(c == '/')
                {
                    state = ST_COMMENT_OR_DIV;
                }
                else if(c == EOF)
                    type = T_EOF;
                else if(ispunct(c))
                {
                    save = c;
                    state = ST_PUNCT;
                }
                break;

            case ST_COMMENT_OR_DIV:
                if(c == '*')
                    state = ST_COMMENT_BLOCK;
                else if (c == '/')
                    state = ST_COMMENT;
                else
                {
                    type = T_DIV;
		    if ( c == EOF)
                      ungetc(c, FileGlobal);
		    else if ((ungetc(c, FileGlobal)) == EOF)
                      return (TokenInfoGlobal.type=T_INTER_ERR);
                }
                break;

            case ST_COMMENT:
                if(c == EOL)
                {
                    state = ST_START;
                    ++line;
                    type = T_EOL;
                }
                if(c == EOF)
                    return (TokenInfoGlobal.type=T_EOF);
                break;

            case ST_COMMENT_BLOCK:
                if(c == '*' && (c = fgetc(FileGlobal)) == '/')
                    state = ST_START;
                if(c == EOL)
                    ++line;
		if (c==EOF)
		  type = T_LEX_ERR;
                break;

            case ST_PUNCT:
                if(c == '=')
                {
                    c = fgetc(FileGlobal);
                    if (c == '=')
                        return (TokenInfoGlobal.type=T_EQ);
                    else
                    {
                      if ( c == EOF)
                        ungetc(c, FileGlobal);
		      else if ((ungetc(c, FileGlobal)) == EOF)
                        return (TokenInfoGlobal.type=T_INTER_ERR);
                      return (TokenInfoGlobal.type=T_ASSIGN);
                    }
                }
                else if(c == '+')
                    return (TokenInfoGlobal.type=T_PLUS);
                else if(c == '-')
                    return (TokenInfoGlobal.type=T_MINUS);
                else if(c == '*')
                {
                    if((c = fgetc(FileGlobal)) == '*')
                        return (TokenInfoGlobal.type=T_EXP);
		    else if (c == '/')
			return (TokenInfoGlobal.type=T_LEX_ERR);
                    else
                    {
                        if ( c == EOF)
                          ungetc(c, FileGlobal);
		       else if ((ungetc(c, FileGlobal)) == EOF)
                          return (TokenInfoGlobal.type=T_INTER_ERR);
                        return (TokenInfoGlobal.type=T_MUL);
                    }
                }
                else if(c == '(')
                    return (TokenInfoGlobal.type=T_L_BRACKET);
                else if(c == ')')
                    return (TokenInfoGlobal.type=T_R_BRACKET);
                else if(c == '!')
                {
                    if((c = fgetc(FileGlobal)) == '=')
                        return (TokenInfoGlobal.type=T_NOT_EQ);
                    else
                        return (TokenInfoGlobal.type=T_LEX_ERR);
                }
                else if(c == '<')
                {
                    c = fgetc(FileGlobal);
                    if(c == '=')
                        return (TokenInfoGlobal.type=T_L_EQ);
                    else
                    {
                        if ( c == EOF)
                          ungetc(c, FileGlobal);
		        else if ((ungetc(c, FileGlobal)) == EOF)
                           return (TokenInfoGlobal.type=T_INTER_ERR);
                        return (TokenInfoGlobal.type=T_LOWER);
                    }
                }
                else if(c == '>')
                {
                    c = fgetc(FileGlobal);
                    if(c == '=')
                        return (TokenInfoGlobal.type=T_G_EQ);
                    else
                    {
                       if ( c == EOF)
                         ungetc(c, FileGlobal);
		       else if ((ungetc(c, FileGlobal)) == EOF)
                          return (TokenInfoGlobal.type=T_INTER_ERR);
                        return (TokenInfoGlobal.type=T_GREATER);
                    }
                }
                else if(c == '[')
                    return (TokenInfoGlobal.type=T_L_SQ_BRACKET);
                else if(c == ']')
                    return (TokenInfoGlobal.type=T_R_SQ_BRACKET);
                else if(c == ',')
                    return (TokenInfoGlobal.type=T_COMMA);
                else if(c == ':')
                    return (TokenInfoGlobal.type=T_COLON);
                // Pokud jsme narazili na PUNCT znak, ktery nezname, tak UNKN
                return (TokenInfoGlobal.type=T_LEX_ERR);
                break;

            // Nacteni stringu do bufferu
            case ST_STRING:
                // Pokud mame prazdny retezec, nacetli jsme znova uvozovky, takze si to overime
                if(c != '"')
                {
                    if(c == EOF || c == EOL)
                        return (TokenInfoGlobal.type=T_LEX_ERR);
                    if((add_char(i, c)) == LEX_INTER)
                        return (TokenInfoGlobal.type=T_INTER_ERR);
                    ++i;
                }
                else
                {
                    if((i >= 2 && TokenInfoGlobal.string[i-1] == '\\' && TokenInfoGlobal.string[i-2] != '\\')
                       || (i == 1 && TokenInfoGlobal.string[0] == '\\'))
                    {
                        if((add_char(i, '"')) == LEX_INTER)
                            return (TokenInfoGlobal.type=T_INTER_ERR);
                        ++i;
                        break;
                    }
                    if((add_char(i, '\0')) == LEX_INTER)
                        return (TokenInfoGlobal.type=T_INTER_ERR);

                    if (CheckEscapeSequences(TokenInfoGlobal.string))
                      return (TokenInfoGlobal.type=T_LEX_ERR);
                    type = T_STR;
                }
                break;
            // Nacteme a zpracujeme cisilko
            case ST_NUMBER:
                if(isdigit(c))
                {
                    if((add_char(i, c)) == LEX_INTER)
                        return (TokenInfoGlobal.type=T_INTER_ERR);
                }
                else
                {
                    switch(c)
                    {
                        case '.':
                            // Pokud uz tecka nebo exponent byl, chyba
                            if(check || check2)
                               return (TokenInfoGlobal.type=T_LEX_ERR);
                            else
                            {
                                if((add_char(i, c)) == LEX_INTER)
                                    return (TokenInfoGlobal.type=T_INTER_ERR);
                                ++i;
                                if(!isdigit((c = fgetc(FileGlobal))))
                                    return (TokenInfoGlobal.type=T_LEX_ERR);
                                else
                                  if((add_char(i, c)) == LEX_INTER)
                                    return (TokenInfoGlobal.type=T_INTER_ERR);
                                // Check, ktery rika, ze uz v cisle tecka je.
                                ++check;
                            }
                            break;
                        case 'e':
                            // Pokud uz exponent byl, tak chyba
                            if(check2)
                                return (TokenInfoGlobal.type=T_LEX_ERR);
                            else
                            {
                                if((add_char(i, 'e')) == LEX_INTER)
                                    return (TokenInfoGlobal.type=T_INTER_ERR);
                                ++i;
                                if(isdigit((c = fgetc(FileGlobal))))
                                {
                                    if((add_char( i, c)) == LEX_INTER)
                                        return (TokenInfoGlobal.type=T_INTER_ERR);
                                }
                                else if (c == '+' || c == '-')
                                {
                                    if((add_char( i, c)) == LEX_INTER)
                                        return (TokenInfoGlobal.type=T_INTER_ERR);
                                    ++i;
                                    if(isdigit((c = fgetc(FileGlobal))))
                                    {
                                        if((add_char( i, c)) == LEX_INTER)
                                            return (TokenInfoGlobal.type=T_INTER_ERR);
                                    }
                                    else
                                        return (TokenInfoGlobal.type=T_LEX_ERR);
                                }
                                else
                                    return (TokenInfoGlobal.type=T_LEX_ERR);
                                ++check2;
                            }
                            break;
                        default:
                            // Pokud by nahodou nebyla za cisly ani tecka ani 'e', tak neni validni
                            if ( c == EOF)
                              ungetc(c, FileGlobal);
		            else if ((ungetc(c, FileGlobal)) == EOF)
                              return (TokenInfoGlobal.type=T_INTER_ERR);
                            if(check == 0 && check2 == 0)
                                return (TokenInfoGlobal.type=T_LEX_ERR);
                            if((add_char( i, '\0')) == LEX_INTER)
                                return (TokenInfoGlobal.type=T_INTER_ERR);
                            type = T_NUM;
                            --i;
                    }
                }
                ++i;
                break;
            // Zpracujeme promennou
            case ST_VARIABLE:
                if(i != 0)
                    return (TokenInfoGlobal.type=T_LEX_ERR);
                if((add_char(i, '_')) == LEX_INTER)
                        return (TokenInfoGlobal.type=T_INTER_ERR);
                ++i;
                state = ST_KEYORVAR;

            // Rozhodnuti, jestli mame klicove slovo nebo promennou
            case ST_KEYORVAR:
                if(isalpha(c) || isdigit(c) || c == '_')
                {
                    if((add_char(i, c)) == LEX_INTER)
                        return (TokenInfoGlobal.type=T_INTER_ERR);
                    ++i;
                }
                else
                {
		    ungetc(c, FileGlobal);
		    if((add_char( i, '\0')) == LEX_INTER)
                        return (TokenInfoGlobal.type=T_INTER_ERR);

		    check = key_word(TokenInfoGlobal.string);
                    if(check != -1 && check != -2) // Pokud je to pouzivane klicove slovo
                        type = check;
		    else if (check == -2)   // Pokud je to rezervovane slovo
                        return (TokenInfoGlobal.type=T_SEM_ERR);
		    else
		       type = T_ID;

                }
                break;

            // Defaultne nacteme neznamy token
            default:
                type = T_LEX_ERR;
                break;
        }
    }
    TokenInfoGlobal.line = line;
    TokenInfoGlobal.len = i;
    TokenInfoGlobal.type = type;
    return type;
}

/// Obalovaci funkce pro cteni tokenu
ecodes_e GetNextToken()
{

   // Setrvani v chybovem stavu nebo "koncovem" stavu
   if (TokenInfoGlobal.error != E_OK || TokenInfoGlobal.type >= T_START)
         return TokenInfoGlobal.error;

  TTokenType type = GetNextTokenTrue();


  // Pokud je neni indikovan error, je vsechno OK
  if (type < T_START)
      return (TokenInfoGlobal.error = E_OK);
  else if (type == T_START || type == T_INTER_ERR) // Interni chyba
       return (TokenInfoGlobal.error = E_INTER);
  else if (type == T_LEX_ERR)   //  Lexikalni chyba
        return (TokenInfoGlobal.error = E_LEXEM);
  else if (type == T_SEM_ERR)   //  Syntakticka chyba
        return (TokenInfoGlobal.error = E_SYNTAX);
  else     // Konec souboru
        return (TokenInfoGlobal.error = E_OK);

  return type;
}

