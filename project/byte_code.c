/**
 * @file bytecode.c
 * @brief Implementace byte code.
 */
/*
 * *******************************************************************
 * Date:         24.11. 2012
 * Author(s):    Petr Huf
 * Project:      Interpret IFJ12
 * Team:         Petr Jirout,         xjirou07
 *               Petr Huf,            xhufpe00
 *               Matej Vanatko,       xvanat01
 *               Gabriel Brandersky,  xbrand04
 * Description:
 * Implementace byte codu
 ********************************************************************
 */
#include <stdio.h>
#include <string.h>

#include "global_types.h"
#include "byte_code.h"
#include "ial.h"
#include "parser.h"
#include "scanner.h"
#include "str.h"
#include "interpreter.h"
#include "inst_list.h"


ecodes_e GenerateByteCode(void)
{
  
  unsigned int maxIndex = conTable->actIndex;
  int type;
  
  /* 
   * Obsah tabulky konstant se tiskne ve formatu:
   * 
   * >>CT<<
   * type;value
   * type;value
   * type;value
   * >>END<<
   */
  
  printf(">>CT<<\n");

  //Vytiskneme obsah tabulky konstant
  for (unsigned int i = 0; i < maxIndex; ++i) {
    type = conTable->items[i].type;
    
    printf("%d;",type);  // tisk type
    
    // Tisk hodnot
    if (type == ST_DEFAULT || type == ST_NIL || type == ST_ARRAY)
      printf("-");
    if (type == ST_STRING)
      printf("%s",conTable->items[i].value.string);
    else if (type == ST_LOGIC || type == ST_NUMBER)
      printf("%g",conTable->items[i].value.number);
    else if (type == ST_FUNC)
      printf("%u",conTable->items[i].value.instAddr);
    
    putchar('\n');
  }
  
  printf(">>END<<\n");
  
  
  // Tisk instrukcni pasky
  /*
   * Format tisku:
   * 
   * >>INSTR<<
   * type;src1;src2;dest
   * type;src1;src2;dest
   * type;src1;src2;dest
   * >>END<<
   */
  printf(">>INSTR<<\n");


  // Tisk instrukci
  for (unsigned int i=0; i < instListGlobal->actIndex; ++i) {
    printf("%u;%u;%u;%u\n",instListGlobal->instArray[i].instType,instListGlobal->instArray[i].src1,
                           instListGlobal->instArray[i].src2,instListGlobal->instArray[i].dest);
 
  }
  
  printf(">>END<<");
  
  return E_OK;
}

ecodes_e DoByteCode()
{
  char line[100];
  int type;
  FILE *fr=FileGlobal;
  symTable_t *table;
  stData_t data;
  unsigned int constHelpCounter=0;
  char constName[100];
  int c;
  string read;

  if (strInit(&read)) // inicializace retezce kam budeme ukladat nekonecne dlouhy retezec
    return E_INTER;
  
  table = SymTableInit();  // Neni zde ale potreba, nutna pouze pro volani funkce SymTableInsert
  conTable = ConTableInit();
  instListGlobal = InstListInit();

  // Test formatu souboru (jen zacatek)
  if (fgets(line,99,fr) == NULL || strcmp(line,">>CT<<\n"))
    return E_INTER;
  
  // Nacitani tabulky konstant
  while (fscanf(fr,"%d;",&type)) {
    
    // Vygenerovani unikatniho jmena pro konstantu
    sprintf(constName,"%u",constHelpCounter++);
    data.tempName = constName;
    
    data.type = type;
    
    // Hodnotu nacteme podle typu
    if (type == ST_STRING) {
      
      strClear(&read);  // Vycistime stavajici retezec
      
      while ((c=fgetc(fr)) != '\n')  // cteme nekonecne dlouhy retezec
        if (strAddChar(&read,c))
          return E_INTER;
     
      ungetc(c,fr);  // Vratime znak konce radku do "souboru"
      data.value.string=read.str;
    }
    else if (type == ST_NUMBER || type == ST_LOGIC) {  // Musime nacist cislo
      if (fscanf(fr,"%lf",&data.value.number) == 0)
        return E_INTER;
    }
      
    // Docteme konec radku
    while ((c=fgetc(fr)) != '\n')
      ;

    // Vlozime konstantu do tabulky konstant
    SymTableInsert(table,conTable,data);
   
  }
  
  // Dalsi kontrola formatu souboru
  if (fgets(line,99,fr) == NULL || strcmp(line,">>END<<\n") || 
      fgets(line,99,fr) == NULL || strcmp(line,">>INSTR<<\n"))
    return E_INTER;
  
  SymTableDestroy(table);  // Uz ji nepotrebujeme
   
  
   
  // Nyni budeme nacitat instrukce
  instListGlobal = InstListInit();
  unsigned int src1,src2,dest;
  int instType;
  inst_t instr;
  
  // Nacitani a generovani instrukci
  while (fscanf(fr,"%d;%u;%u;%u",&instType,&src1,&src2,&dest) == 4) {
    instr.instType = instType;
    instr.src1 = src1;
    instr.src2 = src2;
    instr.dest = dest;
    
    InstAdd(instListGlobal,instr);
    
    // Docteme konec radku
    while ((c=fgetc(fr)) != '\n')
      ;
  }

  // Konec instrukci?
  if (fgets(line,99,fr) == NULL)  
    return E_INTER;
  
  line[7]='\0';
  if (strcmp(line,">>END<<"))
    return E_INTER;
     
  int retCode;
  
  // A uz zbyva jen byte code interpretovat
  retCode = Interpreter(conTable, instListGlobal);

  return retCode;
}


