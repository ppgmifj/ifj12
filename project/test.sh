#! /bin/bash

clear
# Pre istotu znova prelozime
make clean > "/dev/null" 2> "/dev/null"
make > "/dev/null"

# nazov nasho spustitelneho suboru
program=ifj12

# test vykoname pre vsetky subory v ciselne oznacenych slozkach
# ciselne oznacenie UDAVA navratovu hodnotu, ktore maju subory
# v danej slozke vracat

# presunieme sa k testom
cd ../tests/

for i in $( ls ); do
    # pre vsetky adresare, ktore maju celociselny nazov     
    if [[ -d "$i" && -z "${i##[0-9]*}" ]]
       then

       # spustime nas program pre vsetky subory v tomto adreseri
       for f in $( find "$i/" -type f  ); do
           if [[ "$f" == "0/0-test-18.fal" ]] 
             then 
                ../project/"$program" "../tests/$f"
                retCode=`echo $?`
                #  ak testy NEDOPADLI spravne vypis chybu           
                if [ "$retCode" != "$i" ]
                    then
                    echo "CHYBA!!!!! $f: PROGRAM VRATIL: $retCode"
                fi
           fi
       done
    fi
done