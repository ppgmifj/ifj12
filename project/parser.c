/**
 * @file parser.c
 * @brief Syntakticky analyzator
 */
/*
 * *******************************************************************
 * Date:         25.10. 2012
 * Author(s):    Gabriel Brandersky
 * Project:      Interpret IFJ12
 * Team:         Petr Jirout,         xjirou07
 *               Petr Huf,            xhufpe00
 *               Matej Vanatko,       xvanat01
 *               Gabriel Brandersky,  xbrand04
 * Description:
 * Pri cinosti syntaktickemy analyzatoru (SA) pomaha lexikalny analyzator,
 * ktory mu poskytuje tokeny. SA pomocou nich simuluje konstrukciu
 * derivacneho stromu, ak sa strom podarilo vytvorit zdrojovy subor
 * je syntakticky spravny.
 * SA pracuje metodou rekurzivneho zostupu (metoda zhora-dole) zalozenej
 * na LL gramatike
 * *******************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "global_types.h"
#include "interpreter.h"
#include "builtin.h"
#include "scanner.h"
#include "ial.h"
#include "parser.h"
#include "precedens.h"
#include "inst_list.h"
 
#define SAFE_CALL(functionName) if (TokenInfoGlobal.error != E_OK) return TokenInfoGlobal.error; \
                                else if ( (result = functionName) != E_OK) return result 

#define REQUIRED_TOKEN(token) if (TokenInfoGlobal.type != token) return E_SYNTAX

// pocet ukazatelov na TS
#define SYS_TABLE_COUNT 12

// pocet indexov, ktore si musime pamatat
#define ARRAY_LENGTH 12

// indexy do tabulky symbolov
#define FUNC_INDEX 0
#define CALL_INDEX 1
#define PARAM_INDEX 2
#define MAIN_INDEX 3

// pocet vestavenych funkci na pociatocne vlozenie do TS
#define BUILD_IN_MAX 7

// toto je makro pre rozsirenie array = indexovanie pola


#define IS_ARRAY do { if (tmpNode->data.type == ST_ARRAY) {\
    instr.src2 = tmpNode->data.stackIndex + 1;\
    if (TokenInfoGlobal.type == T_L_SQ_BRACKET) {\
        GetNextToken();\
        if (TokenInfoGlobal.type == T_NUM) {\
            data.type = ST_NUMBER;\
            sscanf(TokenInfoGlobal.string,"%lf",&data.value.number);\
            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);\
            instr.instType = I_LOAD;\
            instr.src1 = tmpNode->data.conTableIndex;\
            instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;\
            InstAdd(instListGlobal,instr); \
        }\
        else if (TokenInfoGlobal.type == T_ID) {\
            tmpNode = SymTableSearch(sysTableArray[GlobalIndex], TokenInfoGlobal.string);\
        }\
        else return E_SYNTAX;\
        pom = 1;\
        instr.instType = I_COPY_ARR; \
        instr.src1 = tmpNode->data.stackIndex;\
        instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;\
        InstAdd(instListGlobal,instr); \
        \
        \
        GetNextToken();\
        \
        REQUIRED_TOKEN(T_R_SQ_BRACKET);\
        GetNextToken();                    \
        \
    }\
    else {\
    } \
    } } while(0)\

int GlobalMaxIndex;
int GlobalActualMax;
// globalna premena na indexovanie tabuliek symbolov
int GlobalIndex;

unsigned int constCount;

// globalna premena, ktoru je potrebne dealokovat na konci
int *rememberIndexes = NULL;

ecodes_e BodyFuncRule();

char *BUILD_IN_FUNCTIONS[] = {
    "print","numeric","input","typeOf","len","find","sort"
};


#ifdef DEBUG_PARSER


/*************DEBUG**************************************************
 *******************************************************************/
// ------------------------------------------------------------------

static void PrintBT(symTable_t* table, btNode_t* root);
//wrapper na tisk tabulky


static void PrintST(symTable_t* table)
{
    printf("\n  -----   SYMBOL TABLE   -----\n\n");

    if (table->root != NULL)
        PrintBT(table, table->root);

    printf("\n  ------------------------------\n\n");
}

char *varType[] = {"ST_DEFAULT","ST_STRING","ST_NIL","ST_LOGIC","ST_NUMBER","ST_FUNC","ST_ARRAY","UNKNOW"};

//rekurzivne vytiskne vsechny prvky daneho stromu, od korene, zleva doprava
static void PrintBT(symTable_t* table, btNode_t* root)
{
    if (root == NULL)
        return;
    int type;
    printf("NAME:           %s\n", table->nameChain + root->data.nameIndex);
    type=root->data.type;
    if (type < 0 || type > 5)
        type = 6;
    printf("Type:           %s\n",varType[type]);
    if (type == ST_DEFAULT || type == ST_ARRAY)
        printf("stackIndex:     %u",root->data.stackIndex);
    else
        printf("conTableIndex:  %u",root->data.conTableIndex);
    putchar('\n');putchar('\n');
    PrintBT(table, root->left);
    PrintBT(table, root->right);
}

static void PrintCT(conTable_t *table)
{
    printf("\n  -----   CONSTANT TABLE   -----\n\n");

    for (unsigned int i=0;i<table->actIndex;++i) {
        printf("Ind: %u\t",i);
        int type=table->items[i].type;
        if (type < 0 || type > 5)
            type = 6;
        printf("Type: %-15s",varType[type]);
        if (type == ST_STRING)
            printf("Value: %s\n",table->items[i].value.string);
        else if (type == ST_NUMBER)
            printf("Value: %lf\n",table->items[i].value.number);
        else if (type == ST_NIL)
            printf("Value: NIL        .... in conTable not defined\n");
        else if (type == ST_LOGIC) {
            if (table->items[i].value.number > 0)
                printf("Value: TRUE  (%lf)\n",table->items[i].value.number);
            else
                printf("Value: FALSE (%lf)\n",table->items[i].value.number);
        }
        else if(type == ST_FUNC) {
          printf("Adresa:  (%d)\n",table->items[i].value.instAddr);
        }
        putchar('\n');
    }
    printf("\n  --------------------------------\n\n");
}



// **********************************************************************
// Debugovacia funkcia PrintInstr()
char *instrR[] =
{
  "ADD","SUB","MUL","DIV","POW","ISEQ","ISNOTEQ","LESS","GREATER","LESSOREQ",
  "GREATEROREQ","MOV","LOAD", "CALL", "JMPIFFALSE", "PARAM", "I_ENDPARAM",
  "ENDCALL", "LABEL", "FUNCBEG", "FUNCEND", "JMP", "CREATESTACK", "RETURN",
  "SUBSTRING", "SUBSTRINGRES", "PARAM_COUNT", "NOP", "COPY", "COPY_INC", 
  "COPY_ARR", "MODIFY_ARR",
};
int notSrc1[] = { I_ENDCALL, I_FUNCBEG, I_LABEL, I_FUNCEND, I_JMP,I_CREATESTACK, I_ENDPARAM, I_NOP, -1 };
int notSrc2[] = { I_MOV, I_LOAD, I_PARAM, I_CALL, I_ENDCALL, I_FUNCBEG, I_FUNCEND,
I_JMPIFFALSE, I_LABEL, I_JMP, I_CREATESTACK, I_RETURN, I_SUBSTRINGRES, I_ENDPARAM,
I_PARAMCOUNT, I_NOP, I_COPY, I_COPY_INC, -1 };
int notDes[] = { I_PARAM, I_FUNCBEG, I_FUNCEND, I_LABEL, I_CREATESTACK,
I_PARAMCOUNT, I_RETURN, I_ENDPARAM, I_ENDCALL, I_NOP, -1 };

static void PrintInstr()
{
  // Tisk instrukci
  int count = 0;
  int i = 0;
  int in = 0;
  int instr = 0;
  for (unsigned int j=0;j<instListGlobal->actIndex;++j) {
    instr = instListGlobal->instArray[j].instType;
    printf("%4d.   %-13s  ",count++, instrR[instr]);

    // ma src1?
    i = 0;
    in = 0;
    while( notSrc1[i] != -1) {
        if (instr == notSrc1[i])
            in = 1;
        i++;
    }

    if (! in)
        printf("%-10u",instListGlobal->instArray[j].src1);
    else
        printf("%-10s","X");

    // ma src2?
    i = 0;
    in = 0;
    while( notSrc2[i] != -1) {
        if (instr == notSrc2[i])
            in = 1;
        i++;
    }
    if (! in)
        printf("%-10u",instListGlobal->instArray[j].src2);
    else
        printf("%-10s","X");

    // ma dest?
    i = 0;
    in = 0;
    while( notDes[i] != -1) {
        if (instr == notDes[i])
            in = 1;
        i++;
    }

    if (! in)
        printf("%-10u",instListGlobal->instArray[j].dest);
    else
        printf("%-10s","X");

    printf("  \n");
  }
  putchar('\n');
}
// -------------------------------------------------------------
// **********************************************************************

char *TOKEN_NAMES[] = {
    /* matika */
    [T_PLUS] = "+",
    [T_MINUS] = "-",
    [T_MUL] = "*",
    [T_DIV] = "/",
    [T_EXP] = "**",
    [T_EQ] = "==",
    [T_NOT_EQ] = "!=",
    [T_LOWER] = "<",
    [T_GREATER] = ">",
    [T_L_EQ] = "<=",
    [T_G_EQ] = ">=",
    [T_L_BRACKET] = "(",
    [T_R_BRACKET] = ")",

    /* klicova slova */
    [T_EOL] = "eol",
    [T_FUNC] = "function",
    [T_ELSE] = "else",
    [T_IF] = "if",
    [T_NIL] = "nil",
    [T_RETURN] = "return",
    [T_TRUE] = "true",
    [T_FALSE] = "false",
    [T_WHILE] = "while",
    [T_LOOP] = "loop",
    [T_FOR] = "for",
    [T_IN] = "in",
    [T_END] = "end",

    [T_L_SQ_BRACKET] = "[",
    [T_R_SQ_BRACKET] = "]",

    /* ine */
    [T_COMMA] = ",",
    [T_ASSIGN] = "=",
    [T_COLON] = ":",

    /* typy */
    [T_NUM] = "numeric",
    [T_STR] = "retezec",
    [T_ID] = "id",

    /* chyby */
    [T_EOF] = "EOF",
    [T_LEX_ERR] = "lexikalni chyba",
    [T_INTER_ERR] = "interni chyba"
};


/**
 * Debugovacia funkcia na vypisanie tokenu
 */
void printToken(int token)
{
   if (token < 0 || token > T_INTER_ERR)
        token = T_INTER_ERR;

    printf("%s\n", TOKEN_NAMES[token]);
}
/*************</DEBUG>**************************************************
 *******************************************************************/
#endif



/**
 * Pociatocna alokacia pomocnych indexov
 */
ecodes_e allocateStackIndexes(int **rememberIndexes) {

    int *ptr = NULL;

    ptr = malloc(ARRAY_LENGTH * sizeof(int));

    if (ptr == NULL)
        return E_INTER;

    *rememberIndexes = ptr;

    return E_OK;
}

/**
 * Realokacia pomocnych indexov
 */
ecodes_e reallocateStackIndexes(int **rememberIndexes, int *maxLength)
{   
    // na dvojnasobok
    (*maxLength) <<= 1;
    int *ptr = realloc(*rememberIndexes, (*maxLength) * sizeof(int));

    if (ptr == NULL)
        return E_INTER;

    *rememberIndexes = ptr;
    
    return E_OK;
}


/**
 * Dynamicke zvacsenie poctu tabuliek symbolov.
 *
 * @return pripadnu chybu reallocu
 */
int ReallocateSymTables(symTable_t*** sysTableArray)
{
    // dvojnasobime pole
    GlobalMaxIndex <<= 1;
    symTable_t**  tmpArrayTables = realloc
                  (*sysTableArray,GlobalMaxIndex * sizeof(symTable_t*));

    if (tmpArrayTables == NULL)  // realloc sa nepodaril
        return E_INTER;

    // inicializujeme nove TS
    for(int i = (GlobalMaxIndex >> 1); i < GlobalMaxIndex; i++) {
       tmpArrayTables[i] = SymTableInit();
    }

    *sysTableArray = tmpArrayTables;

    return E_OK;
}

/**
 * Pravidlo pre  neterminal PARAM_N pri volani funkce.
 *
 * Reprezentuje lubovolny pocet dalsich parametrov
 *
 * @return vysledok kontroly podstromu (ecodes_e)
 */
ecodes_e ParamCallNextRule(symTable_t** sysTableArray, const int buildInFunc)
{
    int result = E_OK;
    int repeatParam = 1;


    while(repeatParam) {
        btNode_t* tmpNode = NULL;
        stData_t data;
        data.type = ST_DEFAULT;
        data.tempName = TokenInfoGlobal.string;
        char str[30] = "";
        repeatParam = 0;
        inst_t instr;

        // simulace PARAM_N -> , id PARAM_N
        if (TokenInfoGlobal.type == T_COMMA) {
            repeatParam = 1;
            GetNextToken();

            switch(TokenInfoGlobal.type) {
                case T_ID:
                    // Semanticka kontrola
                    // *************************************************************
                    // premena musi existovat
                    tmpNode = SymTableSearch(sysTableArray[GlobalIndex], TokenInfoGlobal.string);

                    if (tmpNode == NULL)  // nedefinovana premena
                        return E_VAR_SEM;
                    // *************************************************************
                    GetNextToken();

                    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    unsigned int pom = 0;
                    // prekopirovanie pola alebo indexovanie
                    IS_ARRAY;
                    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

                    // Generovanie instrukcie - parameter funkcie (premena)
                    // -------------------------------------------------------------
                    instr.instType = I_PARAM;
                    if (pom)
                        instr.src1 = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
                    else
                       instr.src1 = tmpNode->data.stackIndex;
                    InstAdd(instListGlobal,instr);
                    // -------------------------------------------------------------
                    break;

                case T_STR:
                    data.type = ST_STRING;
                    data.value.string=TokenInfoGlobal.string;
                    break;

                case T_TRUE:
                    data.type = ST_LOGIC;
                    data.value.number = 1.0;
                    break;

                case T_FALSE:
                    data.type = ST_LOGIC;
                    data.value.number = -1.0;
                    break;

                case T_NUM:
                    data.type = ST_NUMBER;
                    sscanf(TokenInfoGlobal.string,"%lf",&data.value.number);
                    break;

                case T_NIL:
                    data.type = ST_NIL;
                    break;

                default:
                    return E_SYNTAX;
            }

            // ulozenie konstanty
            switch(TokenInfoGlobal.type) {
                case T_STR:
                case T_TRUE:
                case T_FALSE:
                case T_NUM:
                case T_NIL:
                    sprintf(str,"%%const%u",constCount++);
                    data.tempName=str;

                    tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

                    if (buildInFunc) {
                        // Generovanie instrukcie - parameter funkcie (konstanta)
                        // -------------------------------------------------------------
                        // najprv nahrame konstantu na zasobnik
                        instr.instType = I_LOAD;
                        instr.src1 = tmpNode->data.conTableIndex;
                        instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
                        InstAdd(instListGlobal,instr);

                        instr.instType = I_PARAM;
                        instr.src1 = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
                        InstAdd(instListGlobal,instr);
                        // -------------------------------------------------------------
                    }

                GetNextToken();
                default:
                    break;
            }



        }
        // simulace PARAM_N -> eps
        else if (TokenInfoGlobal.type == T_R_BRACKET)
            ;
        else
            return E_SYNTAX;
    }

    return result;
}

/**
 * Pravidlo pre  neterminal PARAM pri volani funkce.
 *
 * Reprezentuje parametre pri definicii funkcie
 *
 * @return vysledok kontroly podstromu (ecodes_e)
 */
ecodes_e ParamCallRule(symTable_t** sysTableArray, const int buildInFunc)
{
    inst_t instr;
    int result = E_OK;
    btNode_t* tmpNode = NULL;
    stData_t data;
    data.type = ST_DEFAULT;
    char str[30] = "";


    switch(TokenInfoGlobal.type) {
        // simulace PARAM -> id PARAM_N
        case T_ID:
            // Semanticka kontrola
            // *************************************************************
            // premena musi existovat
            tmpNode = SymTableSearch(sysTableArray[GlobalIndex], TokenInfoGlobal.string);

            if (tmpNode == NULL){
                // ked je to typeOf
                if (buildInFunc == 2) {

                    // nedefinovana premena alebo id funkcie
                    tmpNode = SymTableSearch(sysTableArray[0], TokenInfoGlobal.string);

                    if (tmpNode == NULL) {
                        // funkcia, ktoru v typeOf testuje este nebola definovana
                        data.tempName = TokenInfoGlobal.string;
                        data.type = ST_NUMBER;
                        data.value.number = 0;

                        tmpNode = SymTableInsert(sysTableArray[CALL_INDEX], conTable, data);
                    }

                    // je to funkcia, pridame ako konstantu
                    data.type = ST_FUNC;
                    sprintf(str,"%%const%u",constCount++);
                    data.tempName=str;
                    tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

                    instr.instType = I_LOAD;
                    instr.src1 = tmpNode->data.conTableIndex;
                    instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
                    InstAdd(instListGlobal,instr);

                    tmpNode->data.stackIndex = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
                }
                else
                    return E_VAR_SEM;


            }


            // *************************************************************
            GetNextToken();

            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            unsigned int pom = 0;

            // prekopirovanie pola alebo indexovanie
            IS_ARRAY;
            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                      
            // Generovanie instrukcie - parameter funkcie (premena)
            // -------------------------------------------------------------
            instr.instType = I_PARAM;
            if (pom)
                instr.src1 = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
            else
                instr.src1 = tmpNode->data.stackIndex;
            InstAdd(instListGlobal,instr);
            // -------------------------------------------------------------



            SAFE_CALL( ParamCallNextRule(sysTableArray, 1) );
            break;

        case T_STR:
            data.type = ST_STRING;
            data.value.string=TokenInfoGlobal.string;
            break;

        case T_TRUE:
            data.type = ST_LOGIC;
            data.value.number = 1.0;
            break;

        case T_FALSE:
            data.type = ST_LOGIC;
            data.value.number = -1.0;
            break;

        case T_NUM:
            data.type = ST_NUMBER;
            sscanf(TokenInfoGlobal.string,"%lf",&data.value.number);
            break;

        case T_NIL:
            data.type = ST_NIL;
            break;

        // simulace PARAM -> eps
        case T_R_BRACKET:
            break;

        default:
            return E_SYNTAX;
    }

    // ulozenie konstanty
    switch(TokenInfoGlobal.type) {
        case T_STR:
        case T_TRUE:
        case T_FALSE:
        case T_NUM:
        case T_NIL:
            sprintf(str,"%%const%u",constCount++);
            data.tempName=str;

            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

            if (buildInFunc) {
                // Generovanie instrukcie - parameter funkcie (konstanta)
                // -------------------------------------------------------------
                // najprv nahrame konstantu na zasobnik
                instr.instType = I_LOAD;
                instr.src1 = tmpNode->data.conTableIndex;
                instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
                InstAdd(instListGlobal,instr);

                instr.instType = I_PARAM;
                instr.src1 = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
                InstAdd(instListGlobal,instr);
                // -------------------------------------------------------------
            }


            data.type = ST_DEFAULT;
            GetNextToken();
            SAFE_CALL( ParamCallNextRule(sysTableArray, 1) );

        default:
            break;
    }

   return result;
}

/**
 * Pravidlo pre  neterminal PARAM_N pri definicii funkce
 *
 * Reprezentuje lubovolny pocet dalsich parametrov
 *
 * @return vysledok kontroly podstromu (ecodes_e)
 */
ecodes_e ParamDefNextRule(symTable_t** sysTableArray, unsigned int *paramCount)
{
    int result = E_OK;
    int repeatParam = 1;

    while(repeatParam) {
        repeatParam = 0;

        // simulace PARAM_N -> , id PARAM_N
        if (TokenInfoGlobal.type == T_COMMA) {
            repeatParam = 1;
            GetNextToken();

            REQUIRED_TOKEN(T_ID);
            (*paramCount) += 1;

            // kontrola rovnako pomenovanych parametrov
            btNode_t* tmpNode = SymTableSearch(sysTableArray[GlobalIndex], TokenInfoGlobal.string);

            if (tmpNode != NULL) // su to rovnake nazvy parametrov
                return E_OTHER_SEM;

            
            stData_t data;
            // vlozenie do TS - parameter
            data.type = ST_DEFAULT;    
            
            data.tempName = TokenInfoGlobal.string;

            SymTableInsert(sysTableArray[GlobalIndex], conTable, data);
            GetNextToken();
        }
        // simulace PARAM_N -> eps
        else if (TokenInfoGlobal.type == T_R_BRACKET)
            ;
        else
            return E_SYNTAX;
    }

    return result;
}

/**
 * Pravidlo pre  neterminal PARAM pri definicii funkce
 *
 * Reprezentuje parametre pri definicii funkcie
 *
 * @return vysledok kontroly podstromu (ecodes_e)
 */
ecodes_e ParamDefRule(symTable_t** sysTableArray, unsigned int *paramCount)
{
    int result = E_OK;

    // simulace PARAM -> id PARAM_N
    if (TokenInfoGlobal.type == T_ID ) {
        (*paramCount) += 1;
        // Semanticka kontrola
        // *************************************************************
        // kontrola rovnako pomenovanych parametrov
        btNode_t* tmpNode = SymTableSearch(sysTableArray[GlobalIndex], TokenInfoGlobal.string);

        if (tmpNode != NULL) // su to rovnake nazvy parametrov
           return E_OTHER_SEM;
        // *************************************************************

        // vlozenie do TS - parameter
        stData_t data;
        data.type = ST_DEFAULT;
        data.tempName = TokenInfoGlobal.string;

        SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

        GetNextToken();

        SAFE_CALL( ParamDefNextRule(sysTableArray, paramCount) );
    }
    // simulace PARAM -> eps
    else if (TokenInfoGlobal.type == T_R_BRACKET)
        ;
    else
       return E_SYNTAX;

    return result;
}

/**
 * Pravidlo pre FUNCDEF neterminal.
 *
 * Reprezentuje definiciu funkcie.
 *
 * @return vysledok kontroly podstromu (ecodes_e)
 */
ecodes_e FuncDefRule(symTable_t*** sysTableArray, TStack *stack)
{
    inst_t instr;
    int result = E_OK;
    unsigned int jmpItem = 0;
    stData_t data;
    char str[30] = "";
    btNode_t* tmpNode = NULL;
    btNode_t* paramNode = NULL;
    unsigned int paramCount = 0;

    // nova TS
    GlobalActualMax++;
    GlobalIndex = GlobalActualMax;
    if (GlobalActualMax == GlobalMaxIndex) {

       if (ReallocateSymTables(sysTableArray) != E_OK)
         return E_INTER;

    }

    // simulujeme FUNCDEF -> function id ( PARAM ) eol BODY_FUNC end
    REQUIRED_TOKEN(T_FUNC);
    GetNextToken();

    REQUIRED_TOKEN(T_ID);

    // Semanticka kontrola - funkcia s nazvom premenej
    // *************************************************************
    for (int i = MAIN_INDEX; i < GlobalIndex; i++ ) {
        tmpNode = SymTableSearch((*sysTableArray)[i], TokenInfoGlobal.string);

        if (tmpNode != NULL)
            return E_OTHER_SEM;
    }

    // kontrola, ci uz funkcia nebola deklarovana
    tmpNode = SymTableSearch((*sysTableArray)[0], TokenInfoGlobal.string);

    if (tmpNode != NULL) // je to viacnasobna deklarace
        return E_OTHER_SEM;

    // vlozenie do TS - funkce
    data.type = ST_FUNC;
    data.tempName = TokenInfoGlobal.string;

    SymTableInsert((*sysTableArray)[0], conTable, data);
    // *************************************************************


    // Generovanie instrukcie - definice
    // -------------------------------------------------------------
    // oznacenie zaciatku
    instr.instType = I_FUNCBEG;
    InstAdd(instListGlobal,instr);
    // DELETED
    jmpItem = instListGlobal->actIndex - 1;

    // nahrame adresu do TS
    // 1. funkcia uz bola volana = je v TS, prepiseme instr
    data.type = ST_FUNC;
    data.tempName = TokenInfoGlobal.string;
    // deleted
    data.value.instAddr = jmpItem;
    tmpNode = SymTableInsert((*sysTableArray)[CALL_INDEX], conTable, data);


    char tmpIdName[TokenInfoGlobal.len];
    strcpy(tmpIdName, TokenInfoGlobal.string);
    // -------------------------------------------------------------
    GetNextToken();

    REQUIRED_TOKEN(T_L_BRACKET);
    GetNextToken();

    SAFE_CALL(ParamDefRule((*sysTableArray), &paramCount));


    // Generovanie instrukcie - definice
    // -------------------------------------------------------------
    data.tempName = tmpIdName;
    data.type = ST_NUMBER;
    data.value.number = paramCount;
    paramNode = SymTableInsert((*sysTableArray)[PARAM_INDEX], conTable, data);
    // -------------------------------------------------------------

    REQUIRED_TOKEN(T_R_BRACKET);
    GetNextToken();

    REQUIRED_TOKEN(T_EOL);
    GetNextToken();

    SAFE_CALL( BodyFuncRule((*sysTableArray), stack) );

    REQUIRED_TOKEN(T_END);
    GetNextToken();

    // Generovanie instrukcie
    // -------------------------------------------------------------
    sprintf(str,"%%const%u",constCount++);
    data.tempName = str;
    data.type = ST_NIL;
    tmpNode = SymTableInsert((*sysTableArray)[PARAM_INDEX], conTable, data);

    instr.instType = I_LOAD;
    instr.src1 = tmpNode->data.conTableIndex;
    instr.dest = ((*sysTableArray)[GlobalIndex])->stackIndexCounter++;
    InstAdd(instListGlobal,instr);

    // prazdny return
    instr.instType = I_RETURN;
    instr.src1 = ((*sysTableArray)[GlobalIndex])->stackIndexCounter - 1;
    InstAdd(instListGlobal,instr);


    // oznacenie konca
    instr.instType = I_FUNCEND;
    InstAdd(instListGlobal,instr);
    // -------------------------------------------------------------

    GlobalIndex = MAIN_INDEX;

    return result;
}

/**
 * Pravidlo pre EXPR_NEXT neterminal.
 *
 * Reprezentuje lubovolny pocet po sebe iducich vyrazov
 * odelenych ciarkov v deklaraci pola.
 *
 * @return vysledok kontroly podstromu (ecodes_e)
 */
ecodes_e ExprNextRule(symTable_t** sysTableArray, TStack *stack, unsigned int *arrayIndex, unsigned int *conArray)
{
    int result = E_OK;
    int repeatExpr = 1;
    int lengthOfArray = 0;
    inst_t instr;
    stData_t data;
    char str[30]= "";

    static int maxLength = ARRAY_LENGTH;
    if (rememberIndexes == NULL)
        if (allocateStackIndexes(&rememberIndexes) != E_OK)
            return E_INTER;

    btNode_t* tmpNode = NULL;

    // generovanie instrukcie - array declaraction
    // -------------------------------------------------------------
    // vyhladame si vysledok prveho vyrazu
    tmpNode = SymTableSearch(sysTableArray[GlobalIndex], "%RESULT");

    // presunieme si ho na volne miesto
    instr.instType = I_MOV;
    instr.src1 = tmpNode->data.stackIndex;
    instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
    InstAdd(instListGlobal,instr);

    rememberIndexes[lengthOfArray] = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
    // -------------------------------------------------------------

    while(repeatExpr) {
        repeatExpr = 0;

        // simulace EXPR_NEXT -> , EXPR EXPR_NEXT
        if (TokenInfoGlobal.type == T_COMMA) {
            repeatExpr = 1;
            GetNextToken();

            SAFE_CALL( DoPrecedens(sysTableArray[GlobalIndex],sysTableArray[0],&constCount, stack) );
            // generovanie instrukcie - array declaraction 2
            // -------------------------------------------------------------
            ++lengthOfArray;

            // mozno je treba realokovat
            if (maxLength <= lengthOfArray) {
                if (reallocateStackIndexes(&rememberIndexes, &maxLength) != E_OK)
                    return E_INTER;
            }

            // vyhladame si vysledok dalsieho vyrzu
            tmpNode = SymTableSearch(sysTableArray[GlobalIndex], "%RESULT");

            // presunieme si ho na volne miesto
            instr.instType = I_MOV;
            instr.src1 = tmpNode->data.stackIndex;
            instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
            InstAdd(instListGlobal,instr);

            rememberIndexes[lengthOfArray] = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
            // -------------------------------------------------------------
        }
        // simulace EXPR_N -> eps
        else if (TokenInfoGlobal.type == T_R_SQ_BRACKET) {
          // generovanie instrukcie - array declaraction 3
          // -------------------------------------------------------------
          // vytvarame nove pole, na novom indexe 
          // bud redeklarujeme alebo deklarujeme
          
          // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
          // mod
          data.type = ST_ARRAY;
          sprintf(str,"%%const%u",constCount++);
          data.tempName = str;
          data.value.number = lengthOfArray;
          tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);
          (*arrayIndex) = tmpNode->data.stackIndex;
          (*conArray) = tmpNode->data.conTableIndex;
          // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
          
          for(int i = 0; i <= lengthOfArray; i++) {
            // ostatne prvky
            instr.instType = I_MOV;
            instr.src1 = rememberIndexes[i];
            instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
            InstAdd(instListGlobal,instr);
          }
          
          // pridame konstantu s dlzkou pola
          data.type = ST_ARRAY;
          sprintf(str,"%%const%u",constCount++);
          data.tempName = str;
          data.value.number = lengthOfArray;
          tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

          // presunieme I_MOV
          instr.instType = I_LOAD;
          instr.src1 = tmpNode->data.conTableIndex;
          
          tmpNode = SymTableSearch(sysTableArray[GlobalIndex], "%RESULT");
          instr.dest = tmpNode->data.stackIndex;
          InstAdd(instListGlobal,instr);
          
          //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

          // -------------------------------------------------------------
        }
        else
            return E_SYNTAX;
    }

    return result;
}

/**
 * Pravidlo pre ID_SUB neterminal.
 *
 * Reprezentuje nepovine casti pri vybere podretazca
 *
 * @return vysledok kontroly podstromu (ecodes_e)
 */
ecodes_e IdSubRule(symTable_t** sysTableArray, int tmpIndex)
{
    int result = E_OK;
    btNode_t* tmpNode;
    stData_t data;
    inst_t instr;
    char str[30] = "";

    switch (TokenInfoGlobal.type)
    {
        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // ID_SUB -> id
        /*case  T_ID:
            tmpNode = SymTableSearch(sysTableArray[GlobalIndex], TokenInfoGlobal.string);

            if (tmpNode == NULL) // nedefinovana premena
                return E_VAR_SEM;

            // generovanie instrukcie - vyber podretazca 1
            // -------------------------------------------------------------

            instr.instType = I_SUBSTRING;
            instr.src1 = tmpIndex;
            instr.src2 = tmpNode->data.stackIndex;
            instr.dest = END_INDEX_VALID;
            InstAdd(instListGlobal,instr);
            // -------------------------------------------------------------
          // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

        case  T_NUM:

            // ulozenie konstanty
            sprintf(str,"%%const%u",constCount++);
            data.tempName = str;
            data.type = ST_NUMBER;
            sscanf(TokenInfoGlobal.string,"%lf",&data.value.number);
            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

            instr.instType = I_LOAD;
            instr.src1 = tmpNode->data.conTableIndex;
            instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
            InstAdd(instListGlobal,instr);

            instr.instType = I_SUBSTRING;
            instr.src1 = tmpIndex;
            instr.src2 = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
            instr.dest = END_INDEX_VALID;
            InstAdd(instListGlobal,instr);
            // -------------------------------------------------------------


            GetNextToken();
            break;

        // ID_SUB -> eps
        case T_R_SQ_BRACKET:
            // -------------------------------------------------------------
            instr.instType = I_SUBSTRING;
            instr.src1 = tmpIndex;
            instr.dest = END_INDEX_EMPTY;
            InstAdd(instListGlobal,instr);
            // -------------------------------------------------------------



            break;

        default: return E_SYNTAX;
    }

    return result;
}

/**
 * Pravidlo pre STAT_N4 neterminal.
 *
 * Tato cast rozhodne o konstrukci, bud je to vyber podretazca
 * alebo indexovanie (pouzitie) pola
 *
 * @return vysledok kontroly podstromu (ecodes_e)
 */
ecodes_e StatN4Rule(symTable_t** sysTableArray, int tmpIndex)
{
    int result = E_OK;

    switch (TokenInfoGlobal.type)
    {
        // STAT_N4 -> : ID_SUB ]
        // vyber podretazca
        case T_COLON:
            GetNextToken();

            SAFE_CALL( IdSubRule(sysTableArray, tmpIndex) );

            REQUIRED_TOKEN(T_R_SQ_BRACKET);
            GetNextToken();
            break;

        default: return E_SYNTAX;
    }

    return result;
}

/**
 * Pravidlo pre STAT_N3 neterminal.
 *
 * Tato cast je len medzikrok, pri rozhodovani o aku
 * konstrukciu ide.
 *
 * @return vysledok kontroly podstromu (ecodes_e)
 */
ecodes_e StatN3Rule(symTable_t** sysTableArray)
{
    int result = E_OK;
    btNode_t* tmpNode;
    stData_t data;
    inst_t instr;
    char str[30] = "";

    unsigned int tmpIndex = 0;

    switch(TokenInfoGlobal.type)
    {
        // STAT_N3 -> id  STAT_N4
        // podretazec alebo indexacia pola
        // *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        /*case T_ID:
            // kontrola deklarace premenej
            tmpNode = SymTableSearch(sysTableArray[GlobalIndex], TokenInfoGlobal.string);

            if (tmpNode == NULL)  // nedefinovana premena
                return E_VAR_SEM;

            tmpIndex = tmpNode->data.stackIndex;
            GetNextToken();
            SAFE_CALL( StatN4Rule(sysTableArray, tmpIndex) );

            break;
          // *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

        case T_NUM:
            // generovanie instrukcie - vyber podretazca 1
            // -------------------------------------------------------------
            // ulozenie konstanty
            sprintf(str,"%%const%u",constCount++);
            data.tempName = str;
            data.type = ST_NUMBER;
            sscanf(TokenInfoGlobal.string,"%lf",&data.value.number);
            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

            instr.instType = I_LOAD;
            instr.src1 = tmpNode->data.conTableIndex;
            instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
            InstAdd(instListGlobal,instr);
            tmpIndex = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
            // -------------------------------------------------------------


            GetNextToken();
            SAFE_CALL( StatN4Rule(sysTableArray, tmpIndex) );


            break;

        // STAT_N3 -> : ID_SUB ]
        case T_COLON:
            GetNextToken();

            // generovanie instrukcie - vyber podretazca 1
            // -------------------------------------------------------------
            // ulozenie konstanty
            data.tempName = "%ZERO";
            data.type = ST_NUMBER;
            data.value.number = 0;
            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

            instr.instType = I_LOAD;
            instr.src1 = tmpNode->data.conTableIndex;
            instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
            InstAdd(instListGlobal,instr);
            tmpIndex = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
            // -------------------------------------------------------------

            SAFE_CALL(IdSubRule(sysTableArray, tmpIndex));


            REQUIRED_TOKEN(T_R_SQ_BRACKET);
            GetNextToken();
            break;

        default: return E_SYNTAX;

    }

    return result;
}

/**
 * Pravidlo pre STAT_N2 neterminal.
 *
 * Tato cast je len medzikrok, pri rozhodovani o aku
 * konstrukciu ide.
 *
 * @return vysledok kontroly podstromu (ecodes_e)
 */
ecodes_e StatN2Rule(symTable_t** sysTableArray, char* tmpIdName)
{
    int result = E_OK;
    char str[30] = "";
    btNode_t* tmpNode = NULL;
    tmpNode = SymTableSearch(sysTableArray[GlobalIndex], tmpIdName);
    stData_t data;
    data.type = ST_DEFAULT;
    inst_t instr;
    int buildInFunc = 0;
    unsigned int tmpIndex = 0;

    switch (TokenInfoGlobal.type) {
        // STAT_N2 -> eps
        // priradenie id = id
        case T_EOL:
            // kontrola, ci uz premena bola deklarovana

            if (tmpNode == NULL)
                return E_VAR_SEM;

            break;

        // STAT_N2 -> ( PARAM )
        case T_L_BRACKET:


            buildInFunc = 0;
            // zistime, ci to je vestavena funkcia
            for (int i = 0; i < BUILD_IN_MAX; i++) {
                if (strcmp(tmpIdName, BUILD_IN_FUNCTIONS[i]) == 0) {
                    buildInFunc = 1;

                    // osetrenie typeOf
                    if (i == 3)
                        buildInFunc = 2;

                    // generovanie instrukcie - vestavena funkcie
                    // -------------------------------------------------------------
                    instr.instType = I_CALL;
                    instr.src1 = i;

                    // vlozime pomocnu premenu
                    sprintf(str,"%%RESULT");
                    data.tempName = str;
                    data.type = ST_DEFAULT;
                    tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);


                    instr.dest = tmpNode->data.stackIndex;  // Ulozime do mezivysledku;

                    InstAdd(instListGlobal,instr);
                    // -------------------------------------------------------------
                }
            }

            // ak to neni vestavena, tak je to uzivatelska
            if (! buildInFunc) {
                // generovanie instrukcie - uzivatelska funkcia
                // -------------------------------------------------------------
                // vytvori sa novy zasobnik
                instr.instType = I_CREATESTACK;
                InstAdd(instListGlobal,instr);

                // vyhladame pocet parametrov
                tmpNode = SymTableSearch(sysTableArray[PARAM_INDEX], tmpIdName);

                // ak nebola definovana, este nevieme pocet parametrov
                if (tmpNode == NULL) {
                    data.tempName = tmpIdName;
                    data.type = ST_NUMBER;
                    data.value.number = -1;
                    tmpNode = SymTableInsert(sysTableArray[PARAM_INDEX], conTable, data);
                }

                // najprv nahrame konstantu na zasobnik
                instr.instType = I_LOAD;
                instr.src1 = tmpNode->data.conTableIndex;
                instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
                InstAdd(instListGlobal,instr);

                // pocet parametrov
                instr.instType = I_PARAMCOUNT;
                instr.src1 = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
                InstAdd(instListGlobal,instr);

                // ak bola uz definovana, tak v TS ma aj adresu, ktoru nesmieme prepisat
                tmpNode = SymTableSearch(sysTableArray[CALL_INDEX], tmpIdName);

                // ak sa tam nenachadza pridame ju, aby sme ziskali index
                // ktory si zapamatame v tmpIndex
                // do tabulky konstant
                if (tmpNode == NULL) {
                    data.tempName = tmpIdName;
                    data.type = ST_FUNC;
                    // deleted
                    data.value.instAddr = 0;
                    tmpNode = SymTableInsert(sysTableArray[CALL_INDEX], conTable, data);
                }


                // -------------------------------------------------------------
            }
            GetNextToken();

            if (buildInFunc == 2) {
                SAFE_CALL( ParamCallRule(sysTableArray, 2) );
            }
            else {
                SAFE_CALL( ParamCallRule(sysTableArray, 1) );
            }


            REQUIRED_TOKEN(T_R_BRACKET);

            if (buildInFunc) {
                // Generovanie instrukcie - koniec volania vest. funkcie
                // -------------------------------------------------------------
                instr.instType = I_ENDCALL;
                InstAdd(instListGlobal,instr);
                // -------------------------------------------------------------
            }
            else {

                // Generovanie instrukcie - uzivatelska funkce
                // -------------------------------------------------------------
                instr.instType = I_ENDPARAM;
                InstAdd(instListGlobal,instr);

                // teraz mozeme adresu nahrat s pomocou I_LOAD
                instr.instType = I_LOAD;
                instr.src1 = tmpNode->data.conTableIndex;
                instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
                InstAdd(instListGlobal,instr);
                // adresa kam budeme skakat, lezi na
                tmpIndex = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;

                // skocime
                instr.instType = I_JMP;
                instr.dest = tmpIndex;
                InstAdd(instListGlobal,instr);

                data.type = ST_DEFAULT;
                data.tempName = "%RESULT";
                tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

                // vysledok z uzivatelskej funkcie
                instr.instType = I_MOV;
                instr.src1 = VSTACK_RET;
                instr.dest = tmpNode->data.stackIndex ;
                InstAdd(instListGlobal,instr);
                // -------------------------------------------------------------
            }

            GetNextToken();
            break;

        default: return E_SYNTAX;
    }

    return result;
}

/**
 * Pravidlo pre STAT_N1 neterminal.
 *
 * Tato cast je len medzikrok, pri rozhodovani o aku
 * konstrukciu ide.
 * Moze to vyber podretazca, volanie funkcie, atd.
 *
 * @return vysledok kontroly podstromu (ecodes_e)
 */
ecodes_e StatN1Rule(symTable_t** sysTableArray, TStack *stack, unsigned int *arrayIndex, unsigned int *conArray)
{
    int result = E_OK;

    // osetrenie vyrazu
    int ch = 0;
    stData_t data;
    btNode_t* tmpNode;
    inst_t instr;
    data.tempName = TokenInfoGlobal.string;
    data.type = ST_DEFAULT;
    char str[30]="";

    // nevieme, co sme nacitali: funkciu, premenu -> zapamatame si
    char tmpIdName[TokenInfoGlobal.len];
    strcpy(tmpIdName, TokenInfoGlobal.string);

    switch (TokenInfoGlobal.type) {

        // simulace STAT_N1 -> id STAT_N2
        case T_ID:

            // osetrenie vyrazu id = id (operator) ...
            ch = getc(FileGlobal);

            if (ch == ' ') // preskocime medzery
                while( (ch = getc(FileGlobal)) == ' ')
                    ;

            ungetc(ch, FileGlobal);
            if (ch == '[') {
                tmpNode = SymTableSearch(sysTableArray[GlobalIndex], TokenInfoGlobal.string);
                // nedefinovana premena
                if (tmpNode == NULL)
                    return E_VAR_SEM;


                if (tmpNode->data.type == ST_ARRAY) {
                    SAFE_CALL( DoPrecedens(sysTableArray[GlobalIndex],sysTableArray[0],&constCount, stack) );
                }
                else {
                    // sem sa mi teraz dostane aj indexacia pola, aj vyber podretazca
                    GetNextToken();
                    GetNextToken();
                    
                    SAFE_CALL(StatN3Rule(sysTableArray));
                    

                    // Generovanie kodu - vybera podretazca
                    // ----------------------------------------------------------------
                    // vyhladame stackIndex premenej
                    instr.instType = I_SUBSTRINGRES;
                    instr.src1 = tmpNode->data.stackIndex;

                    data.type = ST_DEFAULT;
                    data.tempName = "%RESULT";
                    tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

                    instr.dest = tmpNode->data.stackIndex;
                    InstAdd(instListGlobal,instr);
                    // -----------------------------------------------------------
                }
            }
            // volanie funkcie
            else if (ch == '(') {
                GetNextToken();
                SAFE_CALL( StatN2Rule(sysTableArray, tmpIdName) );
            }
            else {
                
                SAFE_CALL( DoPrecedens(sysTableArray[GlobalIndex],sysTableArray[0],&constCount, stack) );
                
            }
                

            break;

        // simulace STAT_N1 -> "string"[]
        case T_STR:


            // osetrenie vyrazu id = "string" (operator) ...
            ch = getc(FileGlobal);

            if (ch == ' ') // preskocime medzery
                while( (ch = getc(FileGlobal)) == ' ')
                    ;


            ungetc(ch, FileGlobal);

            // skontrolujeme, ci to neni volanie funkcie
            if (ch != '[' ) {
                SAFE_CALL( DoPrecedens(sysTableArray[GlobalIndex],sysTableArray[0],&constCount, stack) );
            }
            else {
                sprintf(str,"%%const%u",constCount++);
                data.type = ST_STRING;
                data.value.string=TokenInfoGlobal.string;
                data.tempName = str;

                tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

                GetNextToken();

                // vyraz zacinajuci string
                if (TokenInfoGlobal.type != T_L_SQ_BRACKET) {
                    SAFE_CALL( DoPrecedens(sysTableArray[GlobalIndex],sysTableArray[0],&constCount, stack) );
                }
                    

                GetNextToken();


                SAFE_CALL(StatN3Rule(sysTableArray));
                

                // Generovanie instrukcie - vyber podretazca 2
                // -------------------------------------------------------------
                // najprv nahrame retazec na zasobnik
                instr.instType = I_LOAD;
                instr.src1 = tmpNode->data.conTableIndex;
                instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
                InstAdd(instListGlobal,instr);

                instr.instType = I_SUBSTRINGRES;
                instr.src1 = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;


                data.type = ST_DEFAULT;
                data.tempName = "%RESULT";
                tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

                instr.dest = tmpNode->data.stackIndex;
                InstAdd(instListGlobal,instr);
                // -------------------------------------------------------------

            }

            break;

        // STAT_N1 -> EXPR
        case T_MINUS:
        case T_L_BRACKET:
        case T_TRUE:
        case T_NUM:
        case T_FALSE:
        case T_NIL:
            SAFE_CALL( DoPrecedens(sysTableArray[GlobalIndex],sysTableArray[0],&constCount, stack) );
            break;

        // STAT_N1 -> [ EXPR EXPR_NEXT ]
        case T_L_SQ_BRACKET:
            GetNextToken();

            SAFE_CALL( DoPrecedens(sysTableArray[GlobalIndex],sysTableArray[0],&constCount, stack) );

            SAFE_CALL( ExprNextRule(sysTableArray, stack, arrayIndex, conArray) );

            REQUIRED_TOKEN(T_R_SQ_BRACKET);
            GetNextToken();

            break;

        default: return E_SYNTAX;
    }



    return result;
}

/**
 * Pravidlo pre STAT neterminal.
 *
 * Reprezentuje akykolvek prikaz: if-else, while, for, loop, ..
 *
 * @return vysledok kontroly podstromu (ecodes_e)
 */
ecodes_e StatRule(symTable_t** sysTableArray, TStack *stack)
{
    int result = E_OK;
    unsigned int tmpIndex[2] = {0, 0};
    // DELETED
    unsigned int jmpItem;
    char str[30] = "";
    stData_t data;
    data.tempName = TokenInfoGlobal.string;
    data.type = ST_DEFAULT;
    btNode_t* tmpNode = NULL;
    inst_t instr;
    char tmpIdName[TokenInfoGlobal.len];
    unsigned int arrayIndex = 0;
    unsigned int conArray = 0;

    switch(TokenInfoGlobal.type)
    {
        // simulace STAT -> if EXPR eol BODY_FUNC else eol BODY_FUNC end
        case T_IF:
            GetNextToken();

            SAFE_CALL( DoPrecedens(sysTableArray[GlobalIndex],sysTableArray[0],&constCount, stack) );

            // Generovanie instrukcie - podmieneny prikaz
            // -------------------------------------------------------------
            // vytvorime else label, kam budeme skakat label
            // tj. nahranie indexu(adresy) do zasobniku
            data.type = ST_FUNC;
            sprintf(str,"%%const%u",constCount++);
            data.tempName = str;
            data.value.instAddr = -1;
            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);
            jmpItem = tmpNode->data.conTableIndex;

            // nahrame ju do zasobniku hodnot
            instr.instType = I_LOAD;
            instr.src1 = jmpItem;
            instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
            InstAdd(instListGlobal,instr);
            // index else
            tmpIndex[0] = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;

            // podmieneny skok na else
            instr.instType = I_JMPIFFALSE;
            // podla vysledku podmienky - vyhladame v TS
            tmpNode = SymTableSearch(sysTableArray[GlobalIndex], "%RESULT");
            instr.src1 = tmpNode->data.stackIndex;
            instr.dest = tmpIndex[0];
            InstAdd(instListGlobal,instr);
            // -------------------------------------------------------------

            REQUIRED_TOKEN(T_EOL);
            GetNextToken();

            SAFE_CALL( BodyFuncRule(sysTableArray, stack) );

            // Generovanie instrukcie - podmieneny prikaz
            // -------------------------------------------------------------
            // nahrame index skoku do TS a potom do instrukcnej pasky
            data.type = ST_FUNC;
            sprintf(str,"%%const%u",constCount++);
            data.tempName = str;
            // zatial nevieme, kam mame skakat
            data.value.instAddr = -1;
            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

            // nahrame ho do zasobniku hodnot
            instr.instType = I_LOAD;
            instr.src1 = tmpNode->data.conTableIndex;
            instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
            InstAdd(instListGlobal,instr);
            // retazec endIf
            tmpIndex[1] = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;

            // skok na endIf - preskocime else
            instr.instType = I_JMP;
            instr.dest = tmpIndex[1];
            InstAdd(instListGlobal,instr);

            // else label prikaz
            instr.instType = I_LABEL;
            instr.src1 = tmpIndex[0];
            InstAdd(instListGlobal,instr);
            conTable->items[jmpItem].value.instAddr = instListGlobal->actIndex - 1;
            // -------------------------------------------------------------

            REQUIRED_TOKEN(T_ELSE);
            GetNextToken();

            REQUIRED_TOKEN(T_EOL);
            GetNextToken();

            SAFE_CALL( BodyFuncRule(sysTableArray, stack) );

            REQUIRED_TOKEN(T_END);
            GetNextToken();

            
            // Generovanie instrukcie - podmieneny prikaz
            // -------------------------------------------------------------
            // endIf label prikaz
            instr.instType = I_LABEL;
            instr.src1 = tmpIndex[1];
            InstAdd(instListGlobal,instr);

            conTable->items[tmpNode->data.conTableIndex].value.instAddr = instListGlobal->actIndex - 1;
            // -------------------------------------------------------------
            
            REQUIRED_TOKEN(T_EOL);
            break;

        // simulace STAT -> while EXPR eol BODY_FUNC end
        case T_WHILE:
            // Generovanie instrukcie - while prikaz
            // -------------------------------------------------------------
            // whileBegin label prikaz
            instr.instType = I_LABEL;
            InstAdd(instListGlobal,instr);

            // index(adresu) zaciatku si musime ulozit ako konstantu
            data.type = ST_FUNC;
            sprintf(str,"%%const%u",constCount++);
            data.tempName = str;
            data.value.instAddr = instListGlobal->actIndex - 1;
            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

            GetNextToken();
            SAFE_CALL( DoPrecedens(sysTableArray[GlobalIndex],sysTableArray[0],&constCount, stack) );

            // nahrame ho do zasobniku hodnot
            instr.instType = I_LOAD;
            instr.src1 = tmpNode->data.conTableIndex;
            instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
            InstAdd(instListGlobal,instr);
            // adresa, index, label whileEnd
            jmpItem = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;


            // vytvorime whileEnd label, kam budeme skakat podla podmienky
            data.type = ST_FUNC;
            sprintf(str,"%%const%u",constCount++);
            data.tempName=str;
            data.value.instAddr = - 1;
            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

            // nahrame ho do zasobniku hodnot
            instr.instType = I_LOAD;
            instr.src1 = tmpNode->data.conTableIndex;
            instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
            InstAdd(instListGlobal,instr);
            // retazec whileEnd
            tmpIndex[1] = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;

            // index(adresa)
            tmpIndex[0] = tmpNode->data.conTableIndex;

            // podmieneny skok na whileEnd
            instr.instType = I_JMPIFFALSE;
            // podla vysledku podmienky - vyhladame v TS
            tmpNode = SymTableSearch(sysTableArray[GlobalIndex], "%RESULT");
            instr.src1 = tmpNode->data.stackIndex;
            instr.dest = tmpIndex[1];
            InstAdd(instListGlobal,instr);
            // -------------------------------------------------------------


            REQUIRED_TOKEN(T_EOL);
            GetNextToken();

            SAFE_CALL( BodyFuncRule(sysTableArray, stack) );

            // Generovanie instrukcie - while prikaz
            // -------------------------------------------------------------
            // skocime na zaciatok
            instr.instType = I_JMP;
            instr.dest = jmpItem;
            InstAdd(instListGlobal,instr);


            // whileEnd label prikaz
            instr.instType = I_LABEL;
            InstAdd(instListGlobal,instr);
            // doplnime adresu na koniec
            conTable->items[tmpIndex[0]].value.instAddr = instListGlobal->actIndex - 1;

            // -------------------------------------------------------------

            REQUIRED_TOKEN(T_END);
            GetNextToken();
            
            REQUIRED_TOKEN(T_EOL);
            break;

        // simulujeme STAT -> return EXPR
        case T_RETURN:
            GetNextToken();

            
            SAFE_CALL( DoPrecedens(sysTableArray[GlobalIndex],sysTableArray[0],&constCount, stack) );
            
            // Generovanie instrukcie - return
            // -------------------------------------------------------------
            instr.instType = I_RETURN;
            // vyhladame vysledok a ulozime do return
            tmpNode = SymTableSearch(sysTableArray[GlobalIndex], "%RESULT");
            instr.src1 = tmpNode->data.stackIndex;
            InstAdd(instListGlobal,instr);
            // -------------------------------------------------------------

            REQUIRED_TOKEN(T_EOL);
            
            break;

        // STAT -> id = STAT_N1
        case T_ID:

            // signal pouzitiy na neskore pridanie do TS
            tmpIndex[0] = 0;
            // Semanticka kontrola - premena s nazvom funkcie
            // *************************************************************

            tmpNode = SymTableSearch(sysTableArray[0], TokenInfoGlobal.string);

            if (tmpNode != NULL) {

                GetNextToken();

                if (TokenInfoGlobal.type == T_L_BRACKET)
                    return E_SYNTAX;

                return E_OTHER_SEM;
            }
            // *************************************************************


            // pozrieme sa, ci to je premena deklarovana a potom, ci je to pole
            tmpNode = SymTableSearch(sysTableArray[GlobalIndex], TokenInfoGlobal.string);

            unsigned int pom = 0;
            if (tmpNode == NULL) {
              // ak premena nebola deklarovana "pohoda" pridame ju tam
              // resp. dame signal na pridanie :)
              tmpIndex[0] = 1;
              strcpy(tmpIdName, TokenInfoGlobal.string);
              data.tempName = tmpIdName;

              GetNextToken();

            }
            else {
              // musime skontrolovat, ci to neni indexacia pola
              // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
              GetNextToken();
              if (tmpNode->data.type == ST_ARRAY) {
                instr.src2 = tmpNode->data.stackIndex + 1;  
                if (TokenInfoGlobal.type == T_L_SQ_BRACKET) {
                  GetNextToken();

                  if (TokenInfoGlobal.type == T_NUM) {
                      data.type = ST_NUMBER;
                      sscanf(TokenInfoGlobal.string,"%lf",&data.value.number);
                      tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);
                      instr.instType = I_LOAD;
                      instr.src1 = tmpNode->data.conTableIndex;
                      instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
                      InstAdd(instListGlobal,instr);
                  }
                  else if (TokenInfoGlobal.type == T_ID) {
                      tmpNode = SymTableSearch(sysTableArray[GlobalIndex], TokenInfoGlobal.string);
                  }
                  else return E_SYNTAX;
                      
                  pom = 1;
                  instr.instType = I_MODIFY_ARR; 
                  instr.src1 = tmpNode->data.stackIndex;
                  
                  
                  GetNextToken();
                  
                  REQUIRED_TOKEN(T_R_SQ_BRACKET);
                  GetNextToken();                    
                }
              }
              // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            }

            REQUIRED_TOKEN(T_ASSIGN);
            GetNextToken();

            arrayIndex = 0;
            conArray = 0;
            SAFE_CALL( StatN1Rule(sysTableArray, stack, &arrayIndex, &conArray) );
            
            if (arrayIndex && pom)
                return E_SYNTAX;
            
            // "signal" na neskore pridanie
            if (tmpIndex[0]) {
                // ak bolo deklarovane pole
                if (arrayIndex) 
                    data.type = ST_ARRAY;
                
                tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);
                
            }
            
            if (arrayIndex) {
                tmpNode->data.stackIndex = arrayIndex;
                tmpNode->data.conTableIndex = conArray;
            }
            
            // Generovanie instrukcie - priradenie vysledku
            // funkcia, vyber podretazca, deklarace pola
            // -------------------------------------------------------------
            if (pom) {
                tmpNode = SymTableSearch(sysTableArray[GlobalIndex], "%RESULT");
                instr.dest = tmpNode->data.stackIndex; 
                InstAdd(instListGlobal,instr);
            }
            else {
                instr.instType = I_MOV;
                instr.dest = tmpNode->data.stackIndex;
                // vysledok funkcie
                tmpNode = SymTableSearch(sysTableArray[GlobalIndex], "%RESULT");
                if (tmpNode != NULL) {
                    instr.src1 = tmpNode->data.stackIndex;
                    
                    InstAdd(instListGlobal,instr);
                }
            }
            
            // -------------------------------------------------------------
            REQUIRED_TOKEN(T_EOL);
            
            break;

        // STAT -> loop eol BODY_FUNC end EXPR_ARB
        case T_LOOP:
            GetNextToken();

            // Generovanie instrukcie - LOOP
            // -------------------------------------------------------------
            // oznacime si zaciatok
            instr.instType = I_LABEL;
            InstAdd(instListGlobal,instr);

            // pridame adresu
            data.type = ST_FUNC;
            sprintf(str,"%%const%u",constCount++);
            data.tempName = str;
            data.value.instAddr = instListGlobal->actIndex - 1;
            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

            // nahrame adresu
            instr.instType = I_LOAD;
            instr.src1 = tmpNode->data.conTableIndex;
            instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
            InstAdd(instListGlobal,instr);

            tmpIndex[0] = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
            //--------------------------------------------------------------

            REQUIRED_TOKEN(T_EOL);
            GetNextToken();

            SAFE_CALL(BodyFuncRule(sysTableArray, stack) );

            REQUIRED_TOKEN(T_END);
            GetNextToken();

            // volanie EXPR_ARB - ak ma eol spravny, inak zavola
            // precedencni analyzu

            if (TokenInfoGlobal.type == T_EOL) {
                GetNextToken();

                data.type = ST_LOGIC;
                sprintf(str,"%%const%u",constCount++);
                data.tempName = str;
                data.value.number = -1.0;

                tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

                instr.instType = I_LOAD;
                instr.src1 = tmpNode->data.conTableIndex;
                instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
                InstAdd(instListGlobal,instr);

                instr.src1 = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
            }
            else {
                SAFE_CALL(DoPrecedens(sysTableArray[GlobalIndex],sysTableArray[0],&constCount, stack) );
                tmpNode = SymTableSearch(sysTableArray[GlobalIndex], "%RESULT");
                instr.src1 = tmpNode->data.stackIndex;
            }

            // Generovanie instrukcie - LOOP
            // -------------------------------------------------------------
            // skok naspat
            instr.instType = I_JMPIFFALSE;
            // podla vysledku podmienky - vyhladame v TS
            instr.dest = tmpIndex[0];
            InstAdd(instListGlobal,instr);
            // -------------------------------------------------------------
            REQUIRED_TOKEN(T_EOL);
            break;

        // STAT -> for id in id eol BODY_FUNC end
        case T_FOR:
            GetNextToken();
            // Generovanie FORDO = pomocne premene 
            //------------------------------------------------------------------
            // nahrame 0 pre porovnavanie 
            data.type = ST_NUMBER;    
            sprintf(str,"%%const%u",constCount++);
            data.tempName = str;
            data.value.number = 0;
            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);

            instr.instType = I_LOAD;
            instr.src1 = tmpNode->data.conTableIndex;
            instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
            InstAdd(instListGlobal,instr);
            
            tmpIndex[0] = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
            
            // nahrame 1 pre odcitovanie
            data.type = ST_NUMBER;    
            sprintf(str,"%%const%u",constCount++);
            data.tempName = str;
            data.value.number = 1;
            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);
            
            instr.instType = I_LOAD;
            instr.src1 = tmpNode->data.conTableIndex;
            instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
            InstAdd(instListGlobal,instr);
            
            tmpIndex[1] = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
            // ------------------------------------------------------------------
            
            REQUIRED_TOKEN(T_ID);
            // vlozenie do TS - premena
            data.type = ST_DEFAULT;
            data.tempName = TokenInfoGlobal.string;
            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);
            pom = tmpNode->data.stackIndex;
            
            GetNextToken();

            REQUIRED_TOKEN(T_IN);
            GetNextToken();

            REQUIRED_TOKEN(T_ID);
            // kontrola, ci uz premena bola deklarovana
            btNode_t* tmpNode = NULL;
            tmpNode = SymTableSearch(sysTableArray[GlobalIndex], TokenInfoGlobal.string);

            if (tmpNode == NULL)
                return E_VAR_SEM;

            if (tmpNode->data.type != ST_ARRAY)
                return E_OTHER_SEM;
            //------------------------------------------------------------------
                
            // Generovanie instrukcie - FORDO
            // presunieme si velkost pola do pomocnej premenej
            
            jmpItem = tmpNode->data.stackIndex;
            
            // pridame velkost pola
            data.type = ST_NUMBER;
            sprintf(str,"%%const%u",constCount++);
            data.tempName = str;
            //printf("dlzka pola %d",tmpNode->data.conTableIndex);
            //printf("%d",conTable->items[tmpNode->data.conTableIndex].value.number);
            data.value.number = conTable->items[tmpNode->data.conTableIndex].value.number;
            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);
            
            instr.instType = I_LOAD;
            instr.src1 = tmpNode->data.conTableIndex;
            instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
            InstAdd(instListGlobal,instr);
           
            arrayIndex = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
                            
            // pridame adresu
            data.type = ST_FUNC;
            sprintf(str,"%%const%u",constCount++);
            data.tempName = str;
            // zatial nepozname kam
            data.value.instAddr = instListGlobal->actIndex + 1;
            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);
            
            instr.instType = I_LOAD;
            instr.src1 = tmpNode->data.conTableIndex;
            instr.dest = (sysTableArray[GlobalIndex])->stackIndexCounter++;
            InstAdd(instListGlobal,instr);
            
            conArray = (sysTableArray[GlobalIndex])->stackIndexCounter - 1;
            // oznacime si zaciatok
            instr.instType = I_LABEL;
            InstAdd(instListGlobal,instr);
            
            instr.instType = I_COPY_INC;
            instr.src1 =  jmpItem + 1;
            instr.dest = pom;
            InstAdd(instListGlobal,instr);
            // ------------------------------------------------------------------
    

            GetNextToken();

            REQUIRED_TOKEN(T_EOL);
            GetNextToken();

            SAFE_CALL(BodyFuncRule(sysTableArray, stack) );

            // --------------------------------------------------------
            // koniec vyhodnotenie podmienky a pripadny skok na zaciatok
            instr.instType = I_SUB;
            instr.src1 = arrayIndex;
            instr.src2 = tmpIndex[1];
            instr.dest = arrayIndex;
            InstAdd(instListGlobal,instr);
            
            data.type = ST_DEFAULT;
            data.tempName = "%RESULT";
            tmpNode = SymTableInsert(sysTableArray[GlobalIndex], conTable, data);
            instr.instType = I_LESS;
            instr.src1 = arrayIndex;
            instr.src2 = tmpIndex[0];
            instr.dest = tmpNode->data.stackIndex;
            InstAdd(instListGlobal,instr);
            
            instr.instType = I_JMPIFFALSE;
            // podla vysledku podmienky - vyhladame v TS
            
            instr.src1 = tmpNode->data.stackIndex;
            instr.dest = conArray;
            InstAdd(instListGlobal,instr);
            // --------------------------------------------------------
            
            REQUIRED_TOKEN(T_END);
            GetNextToken();
            
            REQUIRED_TOKEN(T_EOL);
            break;

        default: return E_SYNTAX;
    }

    return result;


}

/**
 * Pravidlo pre BODY_FUNC neterminal.
 *
 * Reprezentuje telo funkci, cyklov, if, atd.
 *
 * @return vysledok kontroly podstromu (ecodes_e)
 */
ecodes_e BodyFuncRule(symTable_t** sysTableArray, TStack *stack)
{
    int result = E_OK;
    // cyklenie nad BODY_FUNC - odstranenie nefektivnej rekurzie
    int repeatBodyFunc = 1;

    while(repeatBodyFunc) {
        repeatBodyFunc = 0;

        // odstranenie prazdnych riadkov BODY_FUNC -> eol BODY_FUNC
        while (TokenInfoGlobal.type == T_EOL)
            GetNextToken();

        switch (TokenInfoGlobal.type)
        {
            // BODY_FUNC -> eps.
            case T_ELSE:
            case T_END:
                ;
                break;

            // BODY_FUNC -> STAT eol BODY_FUNC
            case T_ID:
            case T_IF:
            case T_WHILE:
            case T_RETURN:
            case T_LOOP:
            case T_FOR:
                SAFE_CALL( StatRule(sysTableArray, stack) );

                repeatBodyFunc = 1;
                break;


            default: return E_SYNTAX;
        }
    }

    return result;
}

/**
 * Pravidlo pre BODY neterminal.
 *
 * Reprezentuje hlavne telo programu.
 *
 * @return vysledok kontroly podstromu (ecodes_e)
 */
ecodes_e BodyRule(symTable_t*** sysTableArray, TStack *stack )
{
    int result = E_OK;
    // cyklenie nad BODY - odstranenie nefektivnej rekurzie
    int repeatBody = 1;

    while(repeatBody) {
        repeatBody = 0;

        // odstranime prazdne riadky BODY -> eol BODY
        while(TokenInfoGlobal.type == T_EOL)
            GetNextToken();

        switch (TokenInfoGlobal.type)
        {
            // simulace BODY -> FUNCDEF eol BODY
            case T_FUNC:

                SAFE_CALL(FuncDefRule(sysTableArray, stack));

                if (TokenInfoGlobal.type != T_EOL && TokenInfoGlobal.type != T_EOF)
                    return E_SYNTAX;
                GetNextToken();

                repeatBody = 1;
                break;

            // simulace BODY -> STAT eol BODY
            case T_IF:
            case T_WHILE:
            case T_RETURN:
            case T_ID:
            case T_LOOP:
            case T_FOR:

                SAFE_CALL( StatRule(*sysTableArray, stack) );
                repeatBody = 1;


                break;

            case T_EOF:
                break;

            default:return E_SYNTAX;
        }
    }

    return result;
}


//rekurzivne vytiskne vsechny prvky daneho stromu, od korene, zleva doprava
ecodes_e CheckCalledFunctions(symTable_t** sysTableArray, btNode_t* root)
{
    int result = E_OK;

    symTable_t* table = sysTableArray[CALL_INDEX];
    if (root == NULL)
        return E_OK;

    // kontrola prazneho retazca
    if (table->chainIndex == 0)
        return E_OK;

    btNode_t* tmpNode;
    tmpNode = SymTableSearch(sysTableArray[FUNC_INDEX], table->nameChain + root->data.nameIndex);

    if (tmpNode == NULL) {
        // je to volanie z typeOf
        if (table->root->data.type == ST_NUMBER)
            return E_VAR_SEM;
        else
            return E_FUNC_SEM;
    }

    result = CheckCalledFunctions(sysTableArray, root->left);
    if (result != E_OK)
        return result;

    return CheckCalledFunctions(sysTableArray, root->right);
}


/**
 * Vykona syntakticku kontrolu - Pociatocny neterminal.
 *
 * Metodou rekurzivneho zostupu simuluje tvorbu derivacneho stromu
 * Ak sa strom podari odsimulovat, kod je spravny. Zaroven priebezne
 * generuje trojadresny kod (3AK).
 *
 * @return vysledok syntaktickej analyzy (ecodes_e)
 */
ecodes_e Parse()
{
    int result = E_OK;
    constCount = 0;

    // zasobnik pre precedencnu analyzu
    TStack stack;
    if (StackInit(&stack) != E_OK)
        return E_INTER;

    // inicializujeme globalnu tabulku konstant
    conTable = ConTableInit();

    // inicializujeme globalnu instrukcnu pasku
    instListGlobal = InstListInit();

    // vytvorime si zopar (SYS_TABLE_COUNT) tabuliek symbolov
    GlobalMaxIndex = SYS_TABLE_COUNT;
    symTable_t** sysTableArray = malloc(SYS_TABLE_COUNT * sizeof(symTable_t*));

    // malloc sa nepodaril
    if (sysTableArray == NULL)
        return E_INTER;

    // inicializujeme TS
    for (int i = 0; i < GlobalMaxIndex; i++)
        sysTableArray[i] = SymTableInit();



    // vlozime vestavene funkcie do prvej TS (tabulka funkci - TF)
    stData_t data;
    data.type = ST_DEFAULT;

    for(int i = 0; i < BUILD_IN_MAX; i++) {
        data.tempName = BUILD_IN_FUNCTIONS[i];
        SymTableInsert(sysTableArray[0], conTable, data);
    }

    // tabulka symbolov pre main
    GlobalIndex = MAIN_INDEX;
    GlobalActualMax = GlobalIndex;

    // pouzijeme globalnu strukturu TToken reprezentujucu token
    // pri volani GetNextToken sa meni OBSAH!
    TokenInfoGlobal.type = 0;
    GetNextToken();

    // ak funkcia skoncila s chybou, nepokracuje sa dalej!
    result = BodyRule(&sysTableArray, &stack);


    // kontrola nedefinovanovanych funkci
    if (result == E_OK && TokenInfoGlobal.error == E_OK)
        result = CheckCalledFunctions(sysTableArray,sysTableArray[CALL_INDEX]->root);



    // TEST
#ifdef DEBUG_PARSER
   for(int i = 0; i < 5; i++) {
      PrintST(sysTableArray[i]);
    }
    PrintCT(conTable);

//   printf("-----------------------BEFORE_OPTIMALIZATION-------------------\n");
//     PrintInstr();
//   printf("-----------------------AFTER_OPTIMALIZATION-------------------\n");
//     OptimizeInstList(instListGlobal);
     PrintInstr();

#endif
    if (rememberIndexes != NULL)
        free(rememberIndexes);
    // zasobnik pre precedencni analyzu
    StackFree(&stack);

    // dealokujeme, co sme vytvoril
    for(int i = 0; i < GlobalMaxIndex; i++)
        SymTableDestroy(sysTableArray[i]);

    // dealokacia ukazatelov na tabulky symbolov
    free(sysTableArray);

    // chyby z lexikalneho analyzatoru
    if (TokenInfoGlobal.error != E_OK)
        return TokenInfoGlobal.error;

    // chyby zo syntaktickeho analyzatoru
    if (result != E_OK)
        return result;

    // posledny znak MUSI byt EOF
    REQUIRED_TOKEN(T_EOF);

    return result;
}
