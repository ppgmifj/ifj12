/*
 * *******************************************************************
 * Date:         14.10. 2012
 * Author(s):    Petr Huf
 * Project:      Interpret IFJ12
 * Team:         Petr Jirout,         xjirou07
 *               Petr Huf,            xhufpe00
 *               Matej Vanatko,       xvanat01
 *               Gabriel Brandersky,  xbrand04
 * Description:
 * Program provadi precendcni analyzu vyrazu.
 * Je vyuzivan syntaktickym analyzatorem.
 ********************************************************************
 */

#ifndef ___PRECEDENS___
#define ___PRECEDENS___

/// Polozky zasobniku, indexace do precedencni tabulky
typedef enum {
  TokPlus,          ///  +         0
  TokMinus,         ///  -         1
  TokMul,           ///  *         2
  TokDiv,           ///  /         3
  TokExp,           ///  **        4
  TokEq,            ///  ==        5
  TokNonEq,         ///  !=        6
  TokLess,          ///  <         7
  TokGreater,       ///  >         8
  TokLessEq,        ///  <=        9
  TokGreaterEq,     ///  >=        10
  TokLeftBr,        ///  (         11
  TokRightBr,       ///  )         12
  TokUnMinus,       ///  - (unarni)13 
  TokLeftArr,       ///  [         14
  TokRightArr,      ///  ]         15
  TokId,            ///  id        16
  TokDoll,          ///  $         17
  TokEXPR,          ///  E - aplikace pravidla, nonterminal         18
  TokLeft           ///  Znak '<', ktery znaci zacatek handle       19
} MyTokens;

typedef struct {
  MyTokens token;
  btNode_t* node; 
  unsigned int stackIndex;
}TStackItem;

/// Data zasobniku
typedef struct tStack {
  TStackItem *items; /// Ukazatel na zasobnik (pole)
  int actSize;       /// Aktualni velikost zasobniku
  int maxSize;       /// Velikost alokovana pro zasobnik
} TStack;




/// Promenna pro vysledek vyrazu
#define ExprResult "%RESULT"

extern ecodes_e DoPrecedens(symTable_t *table,symTable_t *functions,unsigned int *constCount, TStack *stack);

extern ecodes_e StackInit(TStack *st);
extern void StackFree(TStack *st);
#endif