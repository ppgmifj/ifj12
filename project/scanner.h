#ifndef SCANNER_H_INCLUDED
#define SCANNER_H_INCLUDED
/*
 ********************************************************************
 * File:         scanner.c
 * Date:         17.10. 2012
 * Author(s):    Matej Vanatko
 * Project:      Interpret IFJ12
 * Team:         Petr Jirout,         xjirou07
 *               Petr Huf,            xhufpe00
 *               Matej Vanatko,       xvanat01
 *               Gabriel Brandersky,  xbrand04
 ********************************************************************
 */


/**
 * Vyctovy typ pro terminaly
 */
typedef enum terminals
{
    /* matika */
    T_PLUS,          /* 0  */      ///< +
    T_MINUS,         /* 1  */      ///< -
    T_MUL,           /* 2  */      ///< *
    T_DIV,           /* 3  */      ///< /
    T_EXP,           /* 4  */      ///< **
    T_EQ,            /* 5  */      ///< ==
    T_NOT_EQ,        /* 6  */      ///< !=
    T_LOWER,         /* 7  */      ///< <
    T_GREATER,       /* 8  */      ///< >
    T_L_EQ,          /* 9  */      ///< <=
    T_G_EQ,          /* 10 */      ///< >=
    T_L_BRACKET,     /* 11 */      ///< (
    T_R_BRACKET,     /* 12 */      ///< )

    /* klicova slova */
    T_EOL,           /* 13 */      ///< '\n'
    T_FUNC,          /* 14 */      ///< function
    T_ELSE,          /* 15 */      ///< else
    T_IF,            /* 16 */      ///< if
    T_NIL,           /* 17 */      ///< nil
    T_RETURN,        /* 18 */      ///< return
    T_TRUE,          /* 19 */      ///< true
    T_FALSE,         /* 20 */      ///< false
    T_WHILE,         /* 21 */      ///< while
    T_LOOP,          /* 22 */      ///< loop
    T_FOR,           /* 23 */      ///< for
    T_IN,            /* 24 */      ///< in
    T_END,           /* 25 */      ///< end

    T_L_SQ_BRACKET,  /* 26 */      ///< [
    T_R_SQ_BRACKET,  /* 27 */      ///< ]

    /* ine */
    T_COMMA,         /* 28 */       ///< ,
    T_ASSIGN,        /* 29 */       ///< =
    T_COLON,         /* 30 */       ///< :

    /* typy */
    T_NUM,           /* 31 */       ///< cisilko
    T_STR,           /* 32 */       ///< retezec
    T_ID,            /* 33 */       ///< identifikator
    
    /* chyby */
    T_START,         /* 34 */
    T_EOF,           /* 35 */       ///< konec souboru
    T_LEX_ERR,       /* 36 */       ///< lexikalni chyba
    T_SEM_ERR,       /* 37 */       ///< semanticka chyba
    T_INTER_ERR,     /* 38 */       ///< interni chyba programu

}TTokenType;

typedef struct token{
    int len;           /// delka retezce
    int line;          /// radek, na kterem byl retezec nacten
    TTokenType type;   /// typ tokenu
    ecodes_e error;    /// indikator znacici chybu
    char *string;      /// vlastni retezec
}TToken;

TToken TokenInfoGlobal;  // Globalni promena obsahujici nacteny token
FILE *FileGlobal;        // Globalni promena obsahujici cteny soubor                
extern ecodes_e GetNextToken();   // Funkce nacitajici token
extern int CheckEscapeSequences(const char *str); // Funkce kontroluje escape sekvence v retezci
#endif

