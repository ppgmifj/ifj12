/*
 * *******************************************************************
 * Date:         19.10. 2012
 * Author(s):    Gabriel Brandersky
 * Project:      Interpret IFJ12
 * Team:         Petr Jirout,         xjirou07
 *               Petr Huf,            xhufpe00
 *               Matej Vanatko,       xvanat01
 *               Gabriel Brandersky,  xbrand04
 * Description:
 * Pri cinosti syntaktickemy analyzatoru (SA) pomaha lexikalny analyzator, 
 * ktory mu poskytuje tokeny. SA pomocou nich simuluje konstrukciu 
 * derivacneho stromu, ak sa strom podarilo vytvorit zdrojovy subor
 * je syntakticky spravny.
 * SA pracuje metodou rekurzivneho zostupu (metoda zhora-dole) zalozenej
 * na LL gramatike
 * *******************************************************************
 */

#ifndef PARSER_H_INCLUDED
#define PARSER_H_INCLUDED

conTable_t* conTable;
ecodes_e Parse();

#endif // PARSER_H_INCLUDED

