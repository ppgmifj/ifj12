/**
 * @file precedens.c
 * @brief Precedencni analyza vyrazu.
 */
/*
 * *******************************************************************
 * Date:         14.10. 2012
 * Author(s):    Petr Huf
 * Project:      Interpret IFJ12
 * Team:         Petr Jirout,         xjirou07
 *               Petr Huf,            xhufpe00
 *               Matej Vanatko,       xvanat01
 *               Gabriel Brandersky,  xbrand04
 * Description:
 * Program provadi precendcni analyzu vyrazu.
 * Je vyuzivan syntaktickym analyzatorem.
 ********************************************************************
 */
#define PR_DEBUG 0

#include <stdio.h>
#include <stdlib.h>

#include "global_types.h"
#include "ial.h"
#include "inst_list.h" 
#include "scanner.h"
#include "precedens.h"
#include "parser.h"


typedef enum {
  PR_OK,
  PR_MEMORY_ERROR
}ERRORS;

/// Zakladni velikost zasobniku pro precedencni analyzu
#define STACK_SIZE 200

/// Jmeno pomocnych promennych
#define ZERO "%ZERO"
#define NIL "%NIL"
#define TRUE "%TRUE"
#define FALSE "%FALSE"


/**
 * Precedencni tabulka
 * E - znaci error
 */
int pdTable[19][19] = {
  //          +    -    *    /    **  ==   !=    <    >   <=   >=    (    )    -    [    ]  id    $
  /* +  */  {'>', '>', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '>', '<', '<', 'E', '<', '>'},
  /* -  */  {'>', '>', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '>', '<', '<', 'E', '<', '>'},
  /* *  */  {'>', '>', '>', '>', '<', '>', '>', '>', '>', '>', '>', '<', '>', '<', '<', 'E', '<', '>'},
  /* /  */  {'>', '>', '>', '>', '<', '>', '>', '>', '>', '>', '>', '<', '>', '<', '<', 'E', '<', '>'},
  /* ** */  {'>', '>', '>', '>', '>', '>', '>', '>', '>', '>', '>', '<', '>', '<', '<', 'E', '<', '>'},
  /* == */  {'<', '<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '>', '<', '<', 'E', '<', '>'},
  /* != */  {'<', '<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '>', '<', '<', 'E', '<', '>'},
  /* <  */  {'<', '<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '>', '<', '<', 'E', '<', '>'},
  /* >  */  {'<', '<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '>', '<', '<', 'E', '<', '>'},
  /* <= */  {'<', '<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '>', '<', '<', 'E', '<', '>'},
  /* >= */  {'<', '<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '>', '<', '<', 'E', '<', '>'},
  /* (  */  {'<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', 'E', '<', 'E'},
  /* )  */  {'>', '>', '>', '>', '>', '>', '>', '>', '>', '>', '>', 'E', '>', 'E', 'E', 'E', 'E', '>'},
  /* -  */  {'>', '>', '>', '>', '>', '>', '>', '>', '>', '>', '>', '<', '>', '<', '<', 'E', '<', '>'}, 
  /* [  */  {'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', '<', '<', 'E'},
  /* ]  */  {'>', '>', '>', '>', '>', '>', '>', '>', '>', '>', '>', 'E', '>', 'E', 'E', 'E', 'E', '>'},
  /* id */  {'>', '>', '>', '>', '>', '>', '>', '>', '>', '>', '>', 'E', '>', 'E', '>', '>', 'E', '>'},
  /* $  */  {'<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', 'E', '<', '<', 'E', '<', 'E'}
};


ecodes_e DoPrecedens(symTable_t *table,symTable_t *functions,unsigned int *constCount, TStack *stack);
static TStackItem CheckEnter(symTable_t* table, TStack *stack,unsigned int *constCount,symTable_t* tableF,int *copy);
ecodes_e StackInit(TStack *st);
void StackFree(TStack *st);
static ERRORS StackPush(TStack *st,TStackItem *item);
static TStackItem StackPop(TStack *st);
static TStackItem StackTop(TStack *st);
static TStackItem StackTrueTop(TStack *st);


// Ladici podprogramy ---------------------------------------------

#if PR_DEBUG


/* Tato cast zarizuje tisk TS a CT a instrukctniho list
 * Pouziti:
 * 
 * PrintST(symTable_t* table)
 * PrintCT(conTable_t *table)
 * PrintInstr()            // Instrukcni seznam je globalni promenna
 * 
 */


char *tokCh[] = {
  "+","-","*","/","**","==","!=","<",">","<=",">=","(",")","un-","[","]","id","$","E","-<-"
};


// ------------------------------------------------------------------


char *instrR[] = 
{
  "ADD","SUB","MUL","DIV","POW","ISEQ","ISNOTEQ","LESS","GREATER","LESSOREQ",
  "GREATEROREQ","MOV","LOAD"
};

void StackPrint(TStack *st)
{
  printf("   ");
  for (int i=0;i<st->actSize;++i)
    printf("%s  ",tokCh[st->items[i].token]);
  putchar('\n');
}


static void PrintBT(symTable_t* table, btNode_t* root);
//wrapper na tisk tabulky
static void PrintST(symTable_t* table)
{
  printf("\n  -----   SYMBOL TABLE   -----\n\n");
  
    if (table->root != NULL)
        PrintBT(table, table->root);
    
  printf("\n  ------------------------------\n\n");
}

char *varType[] = {"ST_DEFAULT","ST_STRING","ST_NIL","ST_LOGIC","ST_NUMBER","ST_FUNC","ST_ARRAY","UNKNOW"};

//rekurzivne vytiskne vsechny prvky daneho stromu, od korene, zleva doprava
static void PrintBT(symTable_t* table, btNode_t* root)
{
    if (root == NULL)
        return;
    int type;
    printf("NAME:           %s\n", table->nameChain + root->data.nameIndex);
    type=root->data.type;
    if (type < 0 || type > 6)
      type = 7;
    printf("Type:           %s\n",varType[type]);
    if (type == ST_DEFAULT)
      printf("stackIndex:     %u",root->data.stackIndex);
    else {
      printf("stackIndex:     %u\n",root->data.stackIndex);
      printf("conTableIndex:  %u",root->data.conTableIndex);
    }
    putchar('\n');putchar('\n');
    PrintBT(table, root->left);
    PrintBT(table, root->right);
}

static void PrintCT(conTable_t *table)
{
  printf("\n  -----   CONSTANT TABLE   -----\n\n");
  
  for (unsigned int i=0;i<table->actIndex;++i) {
    printf("conTableIndex:    %u\n",i);
    int type=table->items[i].type;
    if (type < 0 || type > 4)
      type = 5;
    printf("Type:             %s\n",varType[type]);
    if (type == ST_STRING)
      printf("Value:            %s\n",table->items[i].value.string);
    else if (type == ST_NUMBER)
      printf("Value:            %lf\n",table->items[i].value.number);
    else if (type == ST_NIL)
      printf("Value:            NIL        .... in conTable not defined\n");
    else if (type == ST_LOGIC) {
      if (table->items[i].value.number > 0)
        printf("Value:            TRUE  (%lf)\n",table->items[i].value.number);
      else
        printf("Value:            FALSE (%lf)\n",table->items[i].value.number);
    }
    putchar('\n');
  }
  printf("\n  --------------------------------\n\n");
}


static void PrintInstr() 
{
   // Tisk instrukci
 
  for (unsigned int i=0;i<instListGlobal->actIndex;++i) {

  printf("%5s  ",instrR[instListGlobal->instArray[i].instType]);

  
  printf("%3u  ",instListGlobal->instArray[i].src1);
  
  if (instListGlobal->instArray[i].instType != I_MOV && instListGlobal->instArray[i].instType != I_LOAD) 
    printf("%3u  ",instListGlobal->instArray[i].src2);
  else 
    printf("     ");
  
  printf("%3u  ",instListGlobal->instArray[i].dest);
  printf("  \n");

  }
  putchar('\n');
}
// -------------------------------------------------------------




int main()
{
  FileGlobal=fopen("../in.txt","r");

  // NAPLNENA TABULKA SYMBOLU ---------------------------------------
  symTable_t *table,*tableF;
  table=SymTableInit();
  tableF=SymTableInit();
  stData_t data;
  TStack stack;
  StackInit(&stack);
  btNode_t *node;
  
  instListGlobal=InstListInit();
  conTable = ConTableInit();
 
  data.type=ST_DEFAULT;
 
  data.tempName="id1";
  data.value.number=12;
  SymTableInsert(table,conTable,data);
  
  data.tempName="id2"; 
  data.value.number=17;
  SymTableInsert(table,conTable,data);
  
  data.value.number=29;
  data.tempName="id3";
  SymTableInsert(table,conTable,data);
  data.tempName="id4";
  SymTableInsert(table,conTable,data);
  data.tempName="id5";
  SymTableInsert(table,conTable,data);
  
 
   data.type = ST_ARRAY;
   data.tempName = "pole";
   node = SymTableInsert(table,conTable,data);
   node->data.conTableIndex = 2;
   table->stackIndexCounter++;
   table->stackIndexCounter++;
   
  data.tempName="func";
  data.type=ST_DEFAULT;
  SymTableInsert(tableF,conTable,data);
  unsigned int z=0;
  
  GetNextToken();
  int x=DoPrecedens(table,tableF,&z,&stack);
//  fseek(FileGlobal,0,SEEK_SET);
  
 // GetNextToken();
  //x=DoPrecedens(table,tableF,&z,&stack);
  
  
  PrintST(table);
  PrintCT(conTable);
  PrintInstr();

  
  SymTableDestroy(table);
  SymTableDestroy(tableF);
  ConTableDestroy(conTable);
  StackFree(&stack);
  
  free((TokenInfoGlobal.string));
  InstListDestroy(instListGlobal);
  
  printf("---------   END    %d   ---------\n\n",x);
  
  fclose(FileGlobal);

}


#endif

// -------------------------------------------------------------------

/**
 * Funkce provadi precedencni analyzu.
 * 
 * Funkce je volana pri zpracovani vyrazu.
 * Cte vstupni tokeny a postupne vyraz zpracovava.
 * Zaroven je generovan tri adresny kod
 * 
 * @return vysledek precedencni analyzy
 */
ecodes_e DoPrecedens(symTable_t *table,symTable_t *functions,unsigned int *constCount, TStack *stack)
{

  TStackItem stackTerminal,tokensInStack[4],loadedToken,temp;
  stData_t data;
  btNode_t *node;
  inst_t instr;
  int zeroLoaded=0,action;
  unsigned int zeroStackIndex=0;
  unsigned int resultStackIndex;
  unsigned int lastTempResult=0;
  int copy = 1;


  // Vlozeni promenne pro vysledek do tabulky symbolu
  data.tempName = ExprResult;
  data.type = ST_DEFAULT;
  node = SymTableInsert(table,conTable,data);
  resultStackIndex = node->data.stackIndex;
       
  stackTerminal.token = TokDoll;     // Terminal nejblize vrcholu je dolar

  // Osetrime aktualni vstupni token
  if (TokenInfoGlobal.error != E_OK)
    return TokenInfoGlobal.error;
  
  loadedToken=CheckEnter(table,stack,constCount,functions,&copy);
  if (loadedToken.token == TokEXPR) {
    if (loadedToken.node == NULL) {
      if (loadedToken.stackIndex == 1)  // Pokud je to nedefinovana promenna
        return E_VAR_SEM;
      else
        return E_SYNTAX;
    }
    else 
      loadedToken.stackIndex = loadedToken.node->data.stackIndex; // Dale se budeme odkazovat uz jen pres stackIndex
     lastTempResult = loadedToken.stackIndex;
  }
    
  do {
    
    /* Maly trik, pokud je na vstupu EXPR, zaindexujeme se tokenem ID.
     * To proto, ze nemame pravidlo ID -> EXPR. My totiz vzdycky jak nacteme ID, tak to hned dame
     * jako EXPR. Tohle bysme totiz redukovali vzdycky a je to zbytecny delat, kdyz vim jak to dopadne. 
     */
    if (loadedToken.token == TokEXPR)
      loadedToken.token = TokId;
    
    action=pdTable[stackTerminal.token][loadedToken.token];
    
    if (loadedToken.token == TokId)
      loadedToken.token = TokEXPR;
  
    /*
     * Konec triku
     */
    
    switch (action) {
      
      case '<': if (StackPush(stack,&loadedToken) != PR_OK)
                  return E_INTER;
                GetNextToken();
                break;
      
      case '>': /******************************************************* 
                 * Aplikace pravidel
                 *******************************************************/  
              
                /*
                 * Pravidla se dvema operandy
                 ********************************/
                
                tokensInStack[3] = StackPop(stack);  
                if ((int)tokensInStack[3].token == -1)
                  return E_SYNTAX;
                
                tokensInStack[2] = StackPop(stack);
                if ((int)tokensInStack[2].token == -1)
                   return E_SYNTAX;

                
                // Pravidlo -EXPR, realizujeme ji jako:  0 - EXPR
                if (tokensInStack[3].token == TokEXPR && tokensInStack[2].token == TokUnMinus) { 
                  
                  // Konstantu nula, jsme jeste nepouzili
                  if (zeroLoaded == 0) {
                    
                    // Vlozime ji do TS/CT
                    data.tempName = ZERO;
                    data.type = ST_NUMBER;
                    data.value.number = 0;
                    node = SymTableInsert(table,conTable,data);
                    
                    instr.src1 = node->data.conTableIndex;
                    instr.dest = node->data.stackIndex;
                    instr.instType = I_LOAD;
                    InstAdd(instListGlobal,instr);   
                    
                    zeroStackIndex = instr.dest;   // Ulozime si pozici, kde nulu mame ulozenou
                    zeroLoaded = 1;
                  }
                  
                  instr.src1 = zeroStackIndex;
                  instr.src2 = tokensInStack[3].stackIndex;
                  instr.dest = table->stackIndexCounter++;    // Mezivysledek ulozime na prvni volnou pozici v ZH
                  instr.instType = I_SUB;
                  
                  InstAdd(instListGlobal,instr);  // Pridame instrukci do seznamu  
                  
                  // dame do zasobniku vysledek operace
                  tokensInStack[3].stackIndex = instr.dest;
                  StackPush(stack,&tokensInStack[3]);  // Pusheme vysledek operace
                  
                  lastTempResult = instr.dest;      // ulozime si posledni vysledek
                  
                  copy = 0;             // Nebudeme pouzivat instrukci I_COPY
                  break;
                }
                          
                
                
                /* 
                 * Pravidla se 3 operandy
                 *********************************/
                
                tokensInStack[1]=StackPop(stack);                 
                if ((int)tokensInStack[1].token == -1)
                    return E_SYNTAX;
        
                
                // Pravidlo (EXPR)
                if (tokensInStack[3].token == TokRightBr) {  
                  
                  // Overeni syntaxe pravidla
                  if (tokensInStack[2].token != TokEXPR || tokensInStack[1].token != TokLeftBr)
                    return E_SYNTAX; 

                  StackPush(stack,&tokensInStack[2]);  // Push <EXPR>
                  
                  lastTempResult = tokensInStack[2].stackIndex;      // ulozime si posledni vysledek
                  break;
                }
                
                  
                // Pravidla:    EXPR operator EXPR
                if (tokensInStack[3].token == TokEXPR) {
                    
                  // Overeni syntaxe pravidla
                  if (!((int)tokensInStack[2].token >= TokPlus && tokensInStack[2].token <= TokGreaterEq
                      && tokensInStack[1].token == TokEXPR))
                    return E_SYNTAX;
                   
                  instr.src1 = tokensInStack[1].stackIndex;
                  instr.src2 = tokensInStack[3].stackIndex;
                  instr.dest = table->stackIndexCounter++;  // Mezivysledek ulozime na prvni volnou pozici v ZH
                  instr.instType = tokensInStack[2].token;
                  
                  InstAdd(instListGlobal,instr);  // Pridame instrukci do seznamu  
                  
                  tokensInStack[3].stackIndex = instr.dest;
                  StackPush(stack,&tokensInStack[3]);  // Pusheme vysledek operace
                  
                  lastTempResult = instr.dest;      // ulozime si posledni vysledek
                  
                  copy = 0;             // Nebudeme pouzivat instrukci I_COPY
                  break;
                }

                /*
                 * Pravidlo se 4 operandy
                 *******************************/
                
                // Pravidlo    EXPR [ EXPR ]             ==> pristup do pole
                if (tokensInStack[3].token == TokRightArr) { 
                  tokensInStack[0] = StackPop(stack);          // EXPR
                       
                  // Overeni spravne syntaxe pravidla
                  if (tokensInStack[2].token != TokEXPR || tokensInStack[1].token != TokLeftArr || tokensInStack[0].token != TokEXPR)
                    return E_SYNTAX;     
                  
                  // prvni EXPR musi byt typ ST_ARRAY
                  if (tokensInStack[0].node->data.type != ST_ARRAY)
                    return E_SYNTAX;
                  
                  // Vygeneruje instrukci naa pouziti pole
                  instr.instType = I_COPY_ARR;
                  instr.src1 = tokensInStack[2].node->data.stackIndex;
                  instr.src2 = tokensInStack[0].node->data.stackIndex + 1;
                  instr.dest = (table->stackIndexCounter)++;
                  InstAdd(instListGlobal,instr);
                  
                  // Redukce pravidla
                  temp.token = TokEXPR;
                  temp.stackIndex = instr.dest;

                  lastTempResult = temp.stackIndex;      // ulozime si posledni vysledek
                  StackPush(stack,&temp);  // Push <EXPR>
                  
                  copy = 0;             // Nebudeme pouzivat instrukci I_COPY
                  break;
                }
                
                // Pravidlo nenalezeno
                return E_SYNTAX;
                
      case 'E' : 
      default  : return E_SYNTAX;
        
      }
   
    stackTerminal=StackTop(stack);     // Terminal nejblize vrcholu zasobniku
    
    // Osetrime aktualni vstupni token
    if (TokenInfoGlobal.error != E_OK)
      return TokenInfoGlobal.error;

    loadedToken=CheckEnter(table,stack,constCount,functions,&copy);
    if (loadedToken.token == TokEXPR) {
      if (loadedToken.node == NULL) {
        if (loadedToken.stackIndex == 1)  // Pokud je to nedefinovana promenna
          return E_VAR_SEM;
        else
          return E_SYNTAX;
      }
      else
        loadedToken.stackIndex = loadedToken.node->data.stackIndex; // Dale se budeme odkazovat uz jen pres stackIndex
     }
 
  } while (!(stackTerminal.token == TokDoll && loadedToken.token == TokDoll)); 
          
  // Presuneme posledni mezivysledek do promenne RESULT
  instr.src1 = lastTempResult;
  instr.dest = resultStackIndex;
  if (copy == 0)
    instr.instType = I_MOV;
  else
    instr.instType = I_COPY;
  InstAdd(instListGlobal,instr);  // Pridame instrukci do seznamu  
  
  // Vyprazdnime zasobnik, nechame tam jen token dollar
  stack->actSize=1;

  return E_OK;
}

/** Funkce osetri vstupni token
 * 
 * Funkce ostetri vstupni token a urci typ dle vyctoveho typu MyTokens.
 * 
 * @param *table odkaz na tabulku symbolu
 * @return polozku zasobniku
 */
static TStackItem CheckEnter(symTable_t* table, TStack *stack,unsigned int *constCount,symTable_t* tableF,int *copy)
{
  TStackItem item;
  char str[30];
  stData_t data;
  inst_t instr;
  
  // Na vstupu je id
  if (TokenInfoGlobal.type >= T_NUM && TokenInfoGlobal.type <= T_ID) {
    item.token = TokEXPR;           // Plati pro T_ID, T_NUM i T_STRING
    
    // Osetrime postupne T_ID, T_STRING, T_NUM
    if (TokenInfoGlobal.type == T_ID) {
      item.node = SymTableSearch(tableF,TokenInfoGlobal.string);       // Doufam, ze na vstupu neni id funkce
      if (item.node != NULL) {
        item.node = NULL;
        item.stackIndex=2;  // Syntakticky problem
        return item;
      }
      item.node = SymTableSearch(table,TokenInfoGlobal.string);    // Vyhledame promennou v tabulce symbolu
      item.stackIndex=1;   // Znaci monzny problem nedefinovane promenne
      
      return item;
    } 
    
    if (TokenInfoGlobal.type == T_NUM) {            // Na vstupuju je cislo
      data.type = ST_NUMBER;
      sscanf(TokenInfoGlobal.string,"%lf",&data.value.number);
    } 
    else {
      data.type = ST_STRING;                         // Na vstupu je retezec
      data.value.string = TokenInfoGlobal.string;
    }
    // Vlozime konstantu do tabulky symbolu
    sprintf(str,"%%const%u",(*constCount)++);
    data.tempName = str;
    item.node = SymTableInsert(table,conTable,data);
      
    // Vygenerujeme instrukci pro nahrani konstanty do zasobniku hodnot
    instr.src1 = item.node->data.conTableIndex;
    instr.dest = item.node->data.stackIndex;
    instr.instType = I_LOAD;
    InstAdd(instListGlobal,instr);
    
    item.stackIndex=1;
    
    *copy = 0;
    return item;           
  }                                        // T_NUM nebo T_ID nebo T_STRING je zpracovano
 
  
  // Zpracujeme NIL, TRUE nebo FALSE, pokud to neco z toho je
  if (TokenInfoGlobal.type == T_NIL || TokenInfoGlobal.type == T_TRUE || TokenInfoGlobal.type == T_FALSE) {
    
    // Oznacime konstantu podle typu
    if (TokenInfoGlobal.type == T_NIL) {
      data.type = ST_NIL;
      data.tempName = NIL;
    }
    else if (TokenInfoGlobal.type == T_TRUE) {
      data.type = ST_LOGIC;
      data.tempName = TRUE;
      data.value.number = 1.0;
    }
    else if (TokenInfoGlobal.type == T_FALSE) {
      data.type = ST_LOGIC;
      data.tempName = FALSE;
      data.value.number = -1.0;
    }

    // Vyhledani, pripadne vlozeni konstanty do TS/CT
    item.node = SymTableInsert(table,conTable,data); 
    // Pokud konstanta jeste nebyla nahrana to zasobniku hodnot, nahrajeme ji tam

    instr.src1 = item.node->data.conTableIndex;
    instr.dest = item.node->data.stackIndex;
    instr.instType = I_LOAD;
    InstAdd(instListGlobal,instr);
      
    item.node->data.loaded = 1;        // Oznacime konstantu za nahranou
    item.stackIndex=1;
    item.token = TokEXPR;
    
    *copy = 0;
    return item;
  }

  // na vstupu je neco jineho nez operatory nebo zavorky nebo id - znaci konec vyrazu
  if (!((int)TokenInfoGlobal.type >= T_PLUS && TokenInfoGlobal.type <= T_R_BRACKET))
    item.token=TokDoll;
  else {
    if (TokenInfoGlobal.type != T_MINUS)
      item.token=TokenInfoGlobal.type;
    else { // Je to unarni minus nebo binarni?
      item=StackTrueTop(stack);
      if (item.token == TokEXPR || item.token == TokId || item.token == TokRightBr || item.token == TokRightArr) 
        item.token=TokMinus;
      else 
        item.token=TokUnMinus;       // + - je to unarni minus
    }
  }
  
  if (TokenInfoGlobal.type == T_L_SQ_BRACKET)
    item.token = TokLeftArr;
  else if (TokenInfoGlobal.type == T_R_SQ_BRACKET) {
    item=StackTop(stack); 
    
    // Musime rozlisit, zda pouze ukoncujem indexaci pole nebo inicializaci pole
    if (item.token == TokLeftArr)
      item.token = TokRightArr;
    else
      item.token = TokDoll;
  }
  
  return item;
}


/**
 * Funkce inicializuje zasobnik pro precedencni analyzu.
 * 
 * Alokuje pamet pro zasobnik a inicializuje hodnoty zasobniku
 * na defaultni hodnoty
 * 
 * @param *st odkaz na zasobnik
 * @return chybovy stav operace
 */
ecodes_e StackInit(TStack *st) 
{
  // Alokace pameti pro zasobnik
  st->items=(TStackItem *)malloc(STACK_SIZE*sizeof(TStackItem));
  if (st->items == NULL)
    return E_INTER;
  
  // Inicializace polozek zasobniku
  st->maxSize=STACK_SIZE;
  st->actSize=1;
  
  // Na zacatku vlozime dolar na zasobnik
  TStackItem item;
  item.token = TokDoll;
  st->items[0]=item;

  return E_OK;
}

/**
 * Funkce uvolni pamet zasobniku.
 * 
 * Funkce uvolni pamet zasobniku.
 * 
 * @param *st odkaz na zasobnik
 */
void StackFree(TStack *st) 
{
  free((void *)st->items);
}

/**
 * Funkce vlozi polozku na zasobnik
 * 
 * Funkce vlozi polozku v parametru na zasobnik.
 * V pripade plneho zasobniku zasobnik rozsiri o novou pamet.
 * 
 * @param *st odkaz na zasobnik
 * @param item vkladana polozka na zasobnik
 * @return chybovy stav operace
 */
static ERRORS StackPush(TStack *st,TStackItem *item)
{
  // Pokud je zasobnik plny, bude potreba prideli vetsi prostor
  if (st->actSize == st->maxSize) {
    st->maxSize += STACK_SIZE;   // Nova velikost zasobniku
    
    // Prealokovani pameti
    TStackItem *temp=(TStackItem *)realloc((void *)st->items,st->maxSize); 
    if (temp == NULL)
      return PR_MEMORY_ERROR; 
    
    // Aktualizujeme ukazatel
    st->items=temp;
  }
  
  // Pridani polozky na zasobnik
  st->items[st->actSize++]=*item;
  
  return PR_OK;
}

/**
 * Funkce vyjme polozku ze zasobniku
 * 
 * Funkce vlozi polozku v parametru na zasobnik.
 * 
 * @param *st odkaz na zasobnik
 * @return polozku vyjmutou ze zasobniku
 */
static TStackItem StackPop(TStack *st)
{
  if (st->actSize == 0) {
    TStackItem item;
    item.token=-1;
    item.node=NULL;
    return item;
  }
  st->actSize--;
  return st->items[st->actSize];
}

/**
 * Funkce vraci terminal nejblize vrcholu zasobniku.
 * 
 * Funkce vraci terminal nejblize vrcholu zasobniku.
 * 
 * @param *st odkaz na zasobnik
 * @return polozku zasobniku
 */
static TStackItem StackTop(TStack *st)
{
  if (st->items[st->actSize-1].token < TokEXPR)
    return st->items[st->actSize-1];
  else
    return st->items[st->actSize-2];
}


/**
 * Funkce vraci prvek na vrcholu zasobniku.
 * 
 * Funkce vraci prvek na vrcholu zasobniku.
 * 
 * @param *st odkaz na zasobnik
 * @return polozku zasobniku
 */
static TStackItem StackTrueTop(TStack *st)
{

  return st->items[st->actSize-1];
}



